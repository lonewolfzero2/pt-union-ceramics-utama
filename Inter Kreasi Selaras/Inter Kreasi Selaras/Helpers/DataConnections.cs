﻿using System.Collections.Generic;
using System.Linq;
using PT_Union_Ceramics_Utama.Controller.Helpers;

namespace PT_Union_Ceramics_Utama.Helpers
{
    public class DataConnections
    {
        /*
        public static object GetBahanBaku
        {
            get
            {
                return from bahanBaku in DatabaseHandler.Instance.DataClasses.BahanBakus
                       select new
                            {
                                bahanBaku.kdBahanBaku, 
                                bahanBaku.namaBahanBaku, 
                                bahanBaku.ukuran
                            };
            }
        }

        public static object GetPelanggan
        {
            get
            {
                return from pelanggan in DatabaseHandler.Instance.DataClasses.Pelanggans
                       select new
                            {
                                pelanggan.kdPelanggan,
                                pelanggan.tanggalPendaftaran,
                                pelanggan.nama,
                                pelanggan.noTelepon,
                                pelanggan.noFax,
                                pelanggan.jenisKelamin,
                                pelanggan.email,
                                pelanggan.kota,
                                pelanggan.alamat
                            };
            }
        }

        public static object GetPemasoks
        {
            get
            {
                return from pemasok in DatabaseHandler.Instance.DataClasses.Pemasoks
                    select new
                        {
                            pemasok.kdPemasok,
                            pemasok.tanggalPendaftaran,
                            pemasok.namaPerusahaan,
                            pemasok.pic,
                            pemasok.noTelepon,
                            pemasok.noFax,
                            pemasok.email,
                            pemasok.kota,
                            pemasok.alamat
                        };
            }
        }

        public static string GetLastPemasok
        {
            get
            {
                return (from pemasok in DatabaseHandler.Instance.DataClasses.Pemasoks
                        orderby pemasok.kdPemasok descending
                        select pemasok.kdPemasok).Take(1).ToList().GenerateNewCode();
            }
        }

        public static object GetSuratPermintaanPemasokans
        {
            get
            {
                return
                    from suratPermintaanPemasokan in DatabaseHandler.Instance.DataClasses.SuratPermintaanPemasokans
                        select new
                            {
                                suratPermintaanPemasokan.kdSuratPermintaanPemasokan, 
                                suratPermintaanPemasokan.tanggalPermintaanPemasokan, 
                                suratPermintaanPemasokan.kdPemasok, 
                                suratPermintaanPemasokan.kdPegawai
                            };
            }
        }

        public static object GetSuratPemesananProduks
        {
            get
            {
                return
                    from suratPemesananProduk in DatabaseHandler.Instance.DataClasses.SuratPemesananProduks
                        select new
                            {
                                suratPemesananProduk.kdSuratPemesananProduk,
                                suratPemesananProduk.tanggalSuratPemesananProduk,
                                suratPemesananProduk.kdPegawai,
                                suratPemesananProduk.kdPelanggan
                            };
            }
        }

        public static object GetProduks
        {
            get
            {
                return from produk in DatabaseHandler.Instance.DataClasses.Produks
                       select new
                           {
                               produk.kdProduk, 
                               produk.namaProduk, 
                               produk.harga, 
                               produk.jumlah, 
                               produk.merek
                           };
            }
        }

        public static string GetLastProductCode
        {
            get
            {
                return (from produk in DatabaseHandler.Instance.DataClasses.Produks
                        orderby produk.kdProduk descending
                        select produk.kdProduk).Take(1).ToList().GenerateNewCode();
            }
        }

        public static object GetSuratJalans
        {
            get
            {
                return from suratJalan in DatabaseHandler.Instance.DataClasses.SuratJalans
                       select new
                           {
                               suratJalan.kdSuratJalan, 
                               suratJalan.kdSuratPemesananProduk, 
                               suratJalan.tanggalJalan
                           };
            }
        }

        public static object GetSuppliers
        {
            get
            {
                return from pemasok in DatabaseHandler.Instance.DataClasses.Pemasoks
                       select new
                            {
                                pemasok.kdPemasok,
                                pemasok.tanggalPendaftaran,
                                pemasok.namaPerusahaan,
                                pemasok.pic,
                                pemasok.noTelepon,
                                pemasok.noFax,
                                pemasok.email,
                                pemasok.kota,
                                pemasok.alamat
                            };
            }
        }


        public static Pemasok GetPemasokByCode(string code)
        {
            return (from q in DatabaseHandler.Instance.DataClasses.Pemasoks where q.kdPemasok == code select q).First();
        }

        public static Produk GetProductByCode(string code)
        {
            return (from q in DatabaseHandler.Instance.DataClasses.Produks where q.kdProduk == code select q).First();
        }

        public static List<Produk> GetProductsByName(string name)
        {
            return (from q in DatabaseHandler.Instance.DataClasses.Produks where q.namaProduk.Contains(name) select q).ToList();
        }

        public static string GetLastTagihanCode()
        {
            return (from q in DatabaseHandler.Instance.DataClasses.Tagihans orderby q.kdTagihan descending select q.kdTagihan).Take(1).ToList().GenerateNewCode();
        }

        public static object GetUser(string employeeCode, string password, string position)
        {
            return (from q in DatabaseHandler.Instance.DataClasses.Pegawais
                    where q.kdPegawai == employeeCode && q.password == password && q.jabatan == position
                    select new {q.kdPegawai, q.nama, q.jabatan}).First();
        }

        public static bool IsUserExists(string employeeCode, string password, string position)
        {
            var users = (from q in DatabaseHandler.Instance.DataClasses.Pegawais
                         where q.kdPegawai == employeeCode && q.password == password && q.jabatan == position
                         select new { q.kdPegawai, q.nama, q.jabatan });
            return users.Any();
        }

        public static string GetLastSuratPermintaanPemasokanCode()
        {
            return (from q in DatabaseHandler.Instance.DataClasses.SuratPenerimaanPemasokans
                 orderby q.kdSuratPenerimaanPemasokan descending
                 select q.kdSuratPenerimaanPemasokan).Take(1).ToList().GenerateNewCode();
        }

        public static string GetLastPembayaranPenjualanProduk()
        {
            return (from q in DatabaseHandler.Instance.DataClasses.PembayaranPenjualanProduks
                    orderby q.kdPembayaran descending
                    select q.kdPembayaran).Take(1).ToList().GenerateNewCode();
        }
         */
    }
}
