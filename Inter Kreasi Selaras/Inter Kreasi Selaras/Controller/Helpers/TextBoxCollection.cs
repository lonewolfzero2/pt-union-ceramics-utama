﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using PT_Union_Ceramics_Utama.Controller.Helpers.Enums;

namespace PT_Union_Ceramics_Utama.Controller.Helpers
{
    public class TextBoxCollection : List<TextBox>
    {
        private readonly DataGridView _dataGridView;

        public TextBoxCollection(DataGridView dataGridView, Control groupBox)
        {
            _dataGridView = dataGridView;
            AddRange(groupBox.GetAllChildrenOfType<TextBox>());
        }

        private new void Clear()
        {
            base.Clear();
        }

        public void ClearValues(DmlModes dmlMode)
        {
            if (dmlMode != DmlModes.Mengubah)
                this.ElementAt(0).Text = "";

            for (var i = 1; i < Count; i++)
                this.ElementAt(i).Text = "";
        }

        public void UpdateTextBoxValue(int rowIndex)
        {
            for (var i = 0; i < _dataGridView.Rows[rowIndex].Cells.Count; i++)
                this.ElementAt(i).Text = _dataGridView.Rows[rowIndex].Cells[i].Value.ToString();
        }

        public bool IsEmpty
        {
            get { return this.Any(textBox => string.IsNullOrEmpty(textBox.Text)); }
        }
    }

    
}
