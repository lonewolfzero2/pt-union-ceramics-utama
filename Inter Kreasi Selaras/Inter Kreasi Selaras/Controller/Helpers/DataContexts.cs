﻿using System.Data.Linq;
using System.Linq;
using PT_Union_Ceramics_Utama;

namespace PT_Union_Ceramics_Utama.Controller.Helpers
{
    public static class DataContexts
    {
        public static readonly DataClassesDataContext DataContext = DatabaseHandler.Instance.DataClassesDataContext;

        // FormPencarianPemasokanBarang
        public static object PurchaseOrders
        {
            get
            {
                return (from q in DataContext.PurchaseOrders
                        select new {q.idPO, q.tanggalPO, q.idSupplier, q.idPegawai}).ToList();
            }
        }

        // FormPencarianPemesanan
        public static object SuratPemesananProduks
        {
            get
            {
                return (from q in DataContext.SuratPemesananProduks
                        select new {q.idOrder, q.tanggalOrder, q.idPegawai, q.idAgen}).ToList();
            }
        }

        // FormPencarianPelanggan
        public static object Agens
        {
            get
            {
                return (from a in DataContext.Agens
                        select new {a.kdPelanggan, a.tanggalPendaftaran, a.nama, a.noTelepon, a.noFax, a.jenisKelamin, a.email, a.kota, a.alamat}).ToList();
            }
        }

        // FormPencarianPemasok
        public static object Suppliers
        {
            get
            {
                return (from a in DataContext.Suppliers
                        select new {a.idSupplier, a.namaPerusahaan, a.pic, a.noTelepon, a.noFax, a.email, a.kota, a.alamat}).ToList();
            }
        }

        // FormPencarianProduk
        public static object Produks
        {
            get
            {
                return (from a in DataContext.Produks
                        select new {a.idProduk, a.namaProduk, a.harga, a.jumlah}).ToList();
            }
        }

        // FormPencarianSuratJalan
        public static object SuratJalans
        {
            get
            {
                return (from q in DataContext.SuratJalans
                        select new {q.idSuratJalan, q.idOrder, q.tanggalJalan}).ToList();
            }
        }

        // FormPencarianTagihan
        public static object Tagihans
        {
            get
            {
                return (from q in DataContext.Tagihans
                        from w in DataContext.SuratJalans
                        where w.idSuratJalan == q.idSuratJalan
                        select new {q.idTagihan, w.idSuratJalan, q.grandTotal}).ToList();
            }
        }
    }
}





