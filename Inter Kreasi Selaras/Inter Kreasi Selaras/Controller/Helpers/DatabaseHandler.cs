﻿using System.Data.SqlClient;
using PT_Union_Ceramics_Utama.Properties;

namespace PT_Union_Ceramics_Utama.Controller.Helpers
{
    public class DatabaseHandler
    {
        private static readonly DatabaseHandler _instance = new DatabaseHandler();
        public static DatabaseHandler Instance
        {
            get { return _instance; }
        }
        private DatabaseHandler() {}


        private DataClassesDataContext _dataClassesDataContext;
        public DataClassesDataContext DataClassesDataContext
        {
            get
            {
                if (_dataClassesDataContext == null)
                    _dataClassesDataContext = new DataClassesDataContext(DatabasePath);
                return _dataClassesDataContext;
            }
        }
        
        private Settings _dbSettingProperty;
        private Settings _dbSetting
        {
            get
            {
                if (_dbSettingProperty == null)
                    _dbSettingProperty = new Settings();
                return _dbSettingProperty;
            }
        }

        private SqlConnection _sqlConnection;
        public SqlConnection SqlConnection
        {
            get
            {
                if (_sqlConnection == null)
                    _sqlConnection = new SqlConnection(_dbSetting.PT_Union_Ceramics_UtamaConnectionPath);
                return _sqlConnection;
            }
        }

        public string DatabasePath
        {
            get { return _dbSetting.PT_Union_Ceramics_UtamaConnectionPath; }
        }
    }
}
