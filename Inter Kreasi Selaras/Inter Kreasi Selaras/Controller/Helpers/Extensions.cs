﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace PT_Union_Ceramics_Utama.Controller.Helpers
{
    public static class Extensions
    {
        public static string ToCode(this List<string> data)
        {
            var nol = "";
            var format = data.Last().Substring(0, 5);
            var incrementalNumber = int.Parse(data.Last().Substring(5, 3)) + 1;

            if (incrementalNumber < 10) 
                nol = "00";
            else if (incrementalNumber < 100) 
                nol = "0";

            var result = format + nol + incrementalNumber;
            return result;
        }

        public static bool IsEmail(this TextBox txtEmail)
        {
            if (txtEmail.Text.Trim() != "")
            {
                var rex = Regex.Match(txtEmail.Text.Trim(' '), "^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,3})$", RegexOptions.IgnoreCase);
                if (!rex.Success)
                {
                    txtEmail.Focus();
                    return false;
                }
                return true;
            }
            return true;
        }

        public static IEnumerable<T> GetAllChildrenOfType<T>(this Control control)
        {
            var controls = control.Controls.Cast<Control>();
            var enumerable = controls as Control[] ?? controls.ToArray();
            var temp =  enumerable.OfType<T>().Concat<T>(enumerable.SelectMany(GetAllChildrenOfType<T>));
            var rs  = temp as T[] ?? temp.ToArray();
            return rs.OrderBy(r => (r as Control).TabIndex);
        }

        public static int ParseToIntOrDefault(this string value, int defaultValue=0)
        {
            int temp;
            if (int.TryParse(value, out temp))
                return temp;
            return defaultValue;
        }

        /// <summary>
        /// Gets all childern of type T recursively
        /// </summary>
        /// <typeparam name="T">Type of child to be searched</typeparam>
        /// <param name="controlCollection"></param>
        /// <returns></returns>
        public static IEnumerable<T> GetAllChildrenOfType<T>(this Control.ControlCollection controlCollection)
        {
            var result = new List<T>();
            result.AddRange(controlCollection.OfType<T>());
            var controls = controlCollection.OfType<Control>();
            foreach (var control in controls)
            {
                result.AddRange(control.Controls.GetAllChildrenOfType<T>());
            }
            return result;
        }
    }
}
