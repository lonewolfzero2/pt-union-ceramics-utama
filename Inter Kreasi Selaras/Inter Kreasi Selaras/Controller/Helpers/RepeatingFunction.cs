﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace PT_Union_Ceramics_Utama.Controller.Helpers
{
    static class RepeatingFunction
    {
        // Unused
        public static void EnableButton(List<Button> buttonCollection, bool status) 
        {
            for (var i = 0; i < buttonCollection.Count; i++)
            {
                buttonCollection.ElementAt(i).Enabled = status;
            }
        }
        
        // Unused
        public static string GenerateLabelTanggal(string sekarang)
        {
            Regex regex = new Regex("/");
            string[] hasilSplit= regex.Split(sekarang);
            string hari = hasilSplit[1];
            string bulanAngka = hasilSplit[0];
            string tahun = hasilSplit[2];
            string bulanKata= "";
            switch (bulanAngka)
            {
                case "1": bulanKata = "Januari";
                    break;
                case "2": bulanKata = "Februari";
                    break;
                case "3": bulanKata = "Maret";
                    break;
                case "4": bulanKata = "April";
                    break;
                case "5": bulanKata = "Mei";
                    break;
                case "6": bulanKata = "Juni";
                    break;
                case "7": bulanKata = "Juli";
                    break;
                case "8": bulanKata = "Agustus";
                    break;
                case "9": bulanKata = "September";
                    break;
                case "10": bulanKata = "Oktober";
                    break;
                case "11": bulanKata = "November";
                    break;
                case "12": bulanKata = "Desember";
                    break;
            }

            string hasilSekarang = (hasilSplit[1]+"/"+bulanKata+"/"+hasilSplit[2]);
            return hasilSekarang;
        }

        // Unused
        public static string GenerateLabelHari(string hariIni)
        {
            string hasilHariIni = "";
            switch (hariIni)
            {
                case "Sunday": hasilHariIni = "Minggu";
                    break;
                case "Monday": hasilHariIni = "Senin";
                    break;
                case "Tuesday": hasilHariIni = "Selasa";
                    break;
                case "Wednesday": hasilHariIni = "Rabu";
                    break;
                case "Thursday": hasilHariIni = "Kamis";
                    break;
                case "Friday": hasilHariIni = "Jumat";
                    break;
                case "Saturday": hasilHariIni = "Sabtu";
                    break;
            }
            return hasilHariIni;
        }

        // Unused
        public static bool CekTanggal(ComboBox tanggal, ComboBox bulan)
        {
            bool result = true;
            MessageBox.Show(tanggal.SelectedItem.ToString());
            if ((bulan.SelectedIndex+1) % 2 != 0)
            {
                if (int.Parse(tanggal.SelectedItem.ToString())>31)
                {
                    MessageBox.Show(@"Tanggal tidak valid");
                    result = false;
                }
            }
            else
            {
                if (int.Parse(tanggal.SelectedItem.ToString()) > 30)
                {
                    MessageBox.Show(@"Tanggal tidak valid");
                    result = false;
                }
            }
            return result;
			
        }
    }
}
