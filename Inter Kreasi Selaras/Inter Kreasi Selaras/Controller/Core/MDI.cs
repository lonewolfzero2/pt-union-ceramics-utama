﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Data.SqlClient;
using PT_Union_Ceramics_Utama.Controller.Forms;
using PT_Union_Ceramics_Utama.Controller.Helpers;
using PT_Union_Ceramics_Utama.Controller.Report;

namespace PT_Union_Ceramics_Utama.Controller.Core
{
    public partial class MDI : Form
    {
        private string _kodePegawai = "";
        private bool show = false;
        public string KodePegawai
        {
            get { return _kodePegawai; }
            set { _kodePegawai = value; }
        }

        //private int childFormNumber = 0;
        private FormBahanBakuGudang _fbbg;
        private FormBahanBaku _fbb;
        private FormProduk _fproduk;
        private FormPelanggan _fpel;
        private FormSupplier _fsup;
        private FormPermintaanBahanBaku _fpasok;
        private FormPenerimaanBahanBaku _fterima;
        private FormPemesananProduk _fpesan;
        private FormSuratJalan _fkeluar;
        private FormTagihan _ftagihan;
        private FormKwitansi _fkwitansi;
        private FormLogin _flogin;
        private FormCetakLaporanPembelian _fclpb;
        private FormPenerimaanProduk _fpp;
        private FormPengeluaranBahanBakucs _fpbb;
        private FormCetakLaporanGudang _fclg;

        public MDI()
        {
            InitializeComponent();
        }
        
        public void SetUserLogin()
        {
            var conn = DatabaseHandler.Instance.SqlConnection;
            
            var jabatan = ActiveUser.Instance.Position;
            switch (jabatan)
            {
                case "Pembelian":
                    {
                        masterToolStripMenuItem.Visible = !show;
                        bahanBakuToolStripMenuItem.Visible = !show;
                        pemasokToolStripMenuItem2.Visible = !show;
                        supplyToolStripMenuItem.Visible = !show;
                        permintaanBahanBakuToolStripMenuItem.Visible = !show;
                        laporanToolStripMenuItem.Visible = !show;
                        stokGudangToolStripMenuItem.Visible = !show;
                        
                        conn.Open();
                        var valid = "Select idbahanBaku,namaBahanBaku from BahanBaku where Berat < 150";
                        SqlDataAdapter data1 = new SqlDataAdapter(valid, conn);
                        DataTable t = new DataTable();
                        data1.Fill(t);

                        dataGridView1.DataSource = t;
                        //SqlCommand cmd = new SqlCommand(valid, conn);
                        //rdr1 = cmd.ExecuteReader();
                        //while (rdr1.Read())
                        //{
                        //    for (int i = 0; i < rdr1.FieldCount; i++)
                        //    {
                        //        jlh += rdr1.GetString(i) + ", ";
                        //    }
                        //}
                        //jlhProduct = jlh.ToString();
                        //conn.Close();

                        //richTextBox1.Text = string.Format("Bahan Baku {0} mencapai titik ROP", jlh);//jlhProduct + @" Item mencapai titik ROP");


                        //var valid = "Select Count (idBahanBaku) from BahanBaku where Berat < 150";
                        //var cmd = new SqlCommand(valid, conn);
                        //SqlDataReader rdr1 = cmd.ExecuteReader();
                        //while (rdr1.Read())
                        //{
                        //    jumlah = rdr1.GetInt32(0);
                        //}
                        //var jumlahProduct = jumlah.ToString();
                        //conn.Close();

                        //richTextBox1.Text = jumlahProduct + @" Bahan Baku mencapai titik ROP";
                    }
                    break;
                case "Penjualan":
                    {
                        masterToolStripMenuItem.Visible = !show;
                        pelangganToolStripMenuItem1.Visible = !show;
                        demandToolStripMenuItem.Visible = true;
                        produkToolStripMenuItem1.Visible = !show;
                        penjualanToolStripMenuItem1.Visible = !show;
                        pemesananProdukToolStripMenuItem.Visible = !show;
                        deliveryOrderMenuItem.Visible = !show;
                        invoiceToolStripMenuItem.Visible = !show;
                        laporanToolStripMenuItem.Visible = !show;// TODO: Re-Implement this
                        penjualanToolStripMenuItem1.Visible = !show;

                        conn.Open();
                        var valid = "Select idProduk, namaProduk from Produk where jumlah < 250";

                        SqlDataAdapter data1 = new SqlDataAdapter(valid, conn);
                        DataTable t = new DataTable();
                        data1.Fill(t);

                        dataGridView1.DataSource = t;

                    //    SqlCommand cmd = new SqlCommand(valid, conn);
                    //    rdr1 = cmd.ExecuteReader();
                    //    while (rdr1.Read())
                    //    {
                    //        for (int i = 0; i < rdr1.FieldCount; i++)
                    //        {
                    //            jlh += rdr1.GetString(i) + ", ";
                    //        }
                    //    }
                    //    jlhProduct = jlh.ToString();
                    //    conn.Close();

                    //    if (jlh == "")
                    //    {
                    //        richTextBox1.Text = string.Format("Tidak ada produk yang mencapai ROP", jlh);//jlhProduct + @" Item mencapai titik ROP");
                    //    }
                    //    else
                    //    {
                    //        richTextBox1.Text = string.Format("Produk {0} mencapai titik ROP", jlh);//jlhProduct + @" Item mencapai titik ROP");
                    //    }
                    }
                    break;
                case "Gudang":
                    {
                        masterToolStripMenuItem.Visible = !show;
                        bahanBakuToolStripMenuItem.Visible = !show;
                        produkToolStripMenuItem1.Visible = !show;
                        gudangToolStripMenuItem.Visible = !show;
                        penerimaanBahanBakuToolStripMenuItem.Visible = !show;
                        pengeluaranBahanBakuToolStripMenuItem.Visible = !show;
                        penerimaanProdukToolStripMenuItem.Visible = !show;
                        laporanToolStripMenuItem.Visible = !show;
                        gudangToolStripMenuItem1.Visible = !show;
                        penerimaanBahanBakuToolStripMenuItem.Visible = !show;
                        pengeluaranBahanBakuToolStripMenuItem.Visible = !show;
                        penerimaanProdukToolStripMenuItem.Visible = !show;
                        pengeluaranProdukToolStripMenuItem.Visible = !show;

                        conn.Open();
                        var valid = "select idProduk,namaProduk from Produk p where p.jumlah<250 union Select idBahanBaku,namaBahanBaku from BahanBaku bb where Berat<150";

                        SqlDataAdapter data1 = new SqlDataAdapter(valid, conn);
                        DataTable t = new DataTable();
                        data1.Fill(t);

                        dataGridView1.DataSource = t;
                        //SqlCommand cmd = new SqlCommand(valid, conn);
                        //rdr1 = cmd.ExecuteReader();
                        //while (rdr1.Read())
                        //{
                        //    for (int i = 0; i < rdr1.FieldCount; i++)
                        //    {
                        //        jlh += rdr1.GetString(i) + ", ";
                        //    }
                        //}
                        //jlhProduct = jlh.ToString();
                        //conn.Close();
                        

                        //conn.Open();
                        //var valid2 = "Select namaBahanBaku from BahanBaku where Berat < 150";
                        //SqlCommand cmd2 = new SqlCommand(valid2, conn);
                        //rdr2 = cmd2.ExecuteReader();
                        //while (rdr2.Read())
                        //{
                        //    for (int i = 0; i < rdr2.FieldCount; i++)
                        //    {
                        //        jlh2 += rdr2.GetString(i) + ", ";
                        //    }
                        //}
                        //jlhbahanbaku = jlh2.ToString();
                        //conn.Close();

                        //if (jlh == "")
                        //{
                        //    richTextBox1.Text = string.Format("Tidak ada produk yang mencapai ROP", jlh);//jlhProduct + @" Item mencapai titik ROP");
                        //}
                        //else if (jlh2 == "")
                        //{
                        //    richTextBox1.Text = string.Format("Tidak ada bahan baku yang mencapai ROP", jlh);//jlhProduct + @" Item mencapai titik ROP");
                        //}
                        //else
                        //{
                        //    richTextBox1.Text = string.Format("Produk {0} mencapai titik ROP, Bahan Baku {0} mencapai titik ROP", jlh, jlh2);//jlhProduct + @" Item mencapai titik ROP");
                        //}
                    }
                    break;
            }
            loginToolStripMenuItem1.Visible = show;
            logoutToolStripMenuItem1.Visible = !show;
            conn.Close();
        }

        private void logoutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ActiveUser.Instance.DoLogout();
            foreach (var form in Application.OpenForms.Cast<Form>().Where(f => f.IsMdiChild).ToArray()) // ToArray necessary to build a snapshot
            form.Close();

            masterToolStripMenuItem.Visible = show;
            bahanBakuToolStripMenuItem.Visible = show;
            pemasokToolStripMenuItem2.Visible = show;
            supplyToolStripMenuItem.Visible = show;
            permintaanBahanBakuToolStripMenuItem.Visible = show;
            stokGudangToolStripMenuItem.Visible = show;

            pelangganToolStripMenuItem1.Visible = show;
            demandToolStripMenuItem.Visible = show;
            produkToolStripMenuItem1.Visible = show;
            penjualanToolStripMenuItem1.Visible = show;
            pemesananProdukToolStripMenuItem.Visible = show;
            deliveryOrderMenuItem.Visible = show;
            invoiceToolStripMenuItem.Visible = show;
            laporanToolStripMenuItem.Visible = show;// TODO: Re-Implement this
            penjualanToolStripMenuItem1.Visible = show;

            gudangToolStripMenuItem.Visible = show;
            penerimaanBahanBakuToolStripMenuItem.Visible = show;
            pengeluaranBahanBakuToolStripMenuItem.Visible = show;
            penerimaanProdukToolStripMenuItem.Visible = show;

            gudangToolStripMenuItem1.Visible = show;
            penerimaanBahanBakuToolStripMenuItem.Visible = show;
            pengeluaranBahanBakuToolStripMenuItem.Visible = show;
            penerimaanProdukToolStripMenuItem.Visible = show;
            pengeluaranProdukToolStripMenuItem.Visible = show;

            logoutToolStripMenuItem1.Visible = show;
            loginToolStripMenuItem1.Visible = !show;
            MessageBox.Show("Sampai Jumpa Kembali");
            //richTextBox1.Text = "";
            dataGridView1.DataSource=null;

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void bahanBakuToolStripMenuItem1_Click(object sender, EventArgs e)
        {

            //foreach (var form in Application.OpenForms.Cast<Form>().Where(f => f.IsMdiChild).ToArray()) // ToArray necessary to build a snapshot
            //    form.Close();
            //if (_fcb == null || _fcb.IsDisposed)
            //{
            //    _fcb = new FormCetakBahanBaku(this);
            //    _fcb.Initiate();
            //}
        }

        private void produkToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            //foreach (var form in Application.OpenForms.Cast<Form>().Where(f => f.IsMdiChild).ToArray()) // ToArray necessary to build a snapshot
            //    form.Close();
            
            //FormCetakProduk fcb = new FormCetakProduk();
            //fcb.MdiParent = this;
            //fcb.Show();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
                            
        }

        private void bahanBakuToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            foreach (var form in Application.OpenForms.Cast<Form>().Where(f => f.IsMdiChild).ToArray()) // ToArray necessary to build a snapshot
                form.Close();

            _fbbg = new FormBahanBakuGudang(this);
            _fbbg.Initiate();
        }

        private void stokGudangToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            foreach (var form in Application.OpenForms.Cast<Form>().Where(f => f.IsMdiChild).ToArray()) // ToArray necessary to build a snapshot
                form.Close();

            _fclpb = new FormCetakLaporanPembelian();
            _fclpb.Show();
        }

        private void penerimaanBahanBakuToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            foreach (var form in Application.OpenForms.Cast<Form>().Where(f => f.IsMdiChild).ToArray()) // ToArray necessary to build a snapshot
                form.Close();
            if (_fterima == null || _fterima.IsDisposed)
            {
                _fterima = new FormPenerimaanBahanBaku(this);
                _fterima.Initiate();
            }
        }

        private void pelangganToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            foreach (var form in Application.OpenForms.Cast<Form>().Where(f => f.IsMdiChild).ToArray()) // ToArray necessary to build a sformapshot
                form.Close();
            if (_fpel == null || _fpel.IsDisposed)
            {
                _fpel = new FormPelanggan(this);
                _fpel.Initiate();
            }
        }

        private void pemasokToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            foreach (var form in Application.OpenForms.Cast<Form>().Where(f => f.IsMdiChild).ToArray()) // ToArray necessary to build a snapshot
                form.Close();
            if (_fsup == null || _fsup.IsDisposed)
            {
                _fsup = new FormSupplier(this);
                _fsup.Initiate();
            }
        }

        private void produkToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            foreach (var form in Application.OpenForms.Cast<Form>().Where(f => f.IsMdiChild).ToArray()) // ToArray necessary to build a snapshot
                form.Close();
            if (_fproduk == null || _fproduk.IsDisposed)
            {
                _fproduk = new FormProduk(this);
                _fproduk.Initiate();
            }
        }

        private void perToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var form in Application.OpenForms.Cast<Form>().Where(f => f.IsMdiChild).ToArray()) // ToArray necessary to build a snapshot
                form.Close();
            if (_fpasok == null || _fpasok.IsDisposed)
            {
                _fpasok = new FormPermintaanBahanBaku(this);
                _fpasok.Initiate();
            }
        }

        private void penerimaanBahanBakuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var form in Application.OpenForms.Cast<Form>().Where(f => f.IsMdiChild).ToArray()) // ToArray necessary to build a snapshot
                form.Close();
            if (_fterima == null || _fterima.IsDisposed)
            {
                _fterima = new FormPenerimaanBahanBaku(this);
                _fterima.Initiate();
            }
        }

        private void pemesananProdukToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var form in Application.OpenForms.Cast<Form>().Where(f => f.IsMdiChild).ToArray()) // ToArray necessary to build a snapshot
                form.Close();
            if (_fpesan == null || _fpesan.IsDisposed)
            {
                _fpesan = new FormPemesananProduk(this);
                _fpesan.Initiate();
            }
        }

        private void suratJalanToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            foreach (var form in Application.OpenForms.Cast<Form>().Where(f => f.IsMdiChild).ToArray()) // ToArray necessary to build a snapshot
                form.Close();
            if (_fkeluar == null || _fkeluar.IsDisposed)
            {
                _fkeluar = new FormSuratJalan(this);
                _fkeluar.Initiate();
            }
        }

        private void invoiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var form in Application.OpenForms.Cast<Form>().Where(f => f.IsMdiChild).ToArray()) // ToArray necessary to build a snapshot
                form.Close();
            if (_ftagihan == null || _ftagihan.IsDisposed)
            {
                _ftagihan = new FormTagihan(this);
                _ftagihan.Initiate();
            }
        }

        private void kwitansiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //foreach (var form in Application.OpenForms.Cast<Form>().Where(f => f.IsMdiChild).ToArray()) // ToArray necessary to build a snapshot
            //    form.Close();
            //if (_fkwitansi == null || _fkwitansi.IsDisposed)
            //{
            //    _fkwitansi = new FormKwitansi(this);
            //    _fkwitansi.Initiate();
            //}
        }

        private void penjualanToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            foreach (var form in Application.OpenForms.Cast<Form>().Where(f => f.IsMdiChild).ToArray()) // ToArray necessary to build a snapshot
                form.Close();

            FormCetakLaporanPemesanan lpsn = new FormCetakLaporanPemesanan();
            lpsn.Show();
        }

        private void loginToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            foreach (var form in Application.OpenForms.Cast<Form>().Where(f => f.IsMdiChild).ToArray()) // ToArray necessary to build a snapshot
                form.Close();
            if (_flogin == null || _flogin.IsDisposed)
            {
                _flogin = new FormLogin(this);
                _flogin.Initiate();
            }
        }

        private void bahanBakuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var form in Application.OpenForms.Cast<Form>().Where(f => f.IsMdiChild).ToArray()) // ToArray necessary to build a snapshot
                form.Close();
            if (_fbb == null || _fbb.IsDisposed)
            {
                _fbb = new FormBahanBaku(this);
                _fbb.Initiate();
            }
        }

        private void penerimaanProdukToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var form in Application.OpenForms.Cast<Form>().Where(f => f.IsMdiChild).ToArray()) // ToArray necessary to build a snapshot
                form.Close();
            if (_fpp == null || _fpp.IsDisposed)
            {
                _fpp = new FormPenerimaanProduk(this);
                _fpp.Initiate();
            }
        }

        private void pengeluaranBahanBakuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var form in Application.OpenForms.Cast<Form>().Where(f => f.IsMdiChild).ToArray()) // ToArray necessary to build a snapshot
                form.Close();
            if (_fpbb == null || _fpbb.IsDisposed)
            {
                _fpbb = new FormPengeluaranBahanBakucs(this);
                _fpbb.Initiate();
            }
        }

        private void pengeluaranProdukToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var form in Application.OpenForms.Cast<Form>().Where(f => f.IsMdiChild).ToArray()) // ToArray necessary to build a snapshot
                form.Close();
            if (_fclg == null || _fclg.IsDisposed)
            {
                _fclg = new FormCetakLaporanGudang(this,1);
                _fclg.Show();
            }
        }

        private void penerimaanBahanBakuToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            foreach (var form in Application.OpenForms.Cast<Form>().Where(f => f.IsMdiChild).ToArray()) // ToArray necessary to build a snapshot
                form.Close();
            if (_fclg == null || _fclg.IsDisposed)
            {
                _fclg = new FormCetakLaporanGudang(this, 2);
                _fclg.Show();
            }
        }

        private void penerimaanProdukToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            foreach (var form in Application.OpenForms.Cast<Form>().Where(f => f.IsMdiChild).ToArray()) // ToArray necessary to build a snapshot
                form.Close();
            if (_fclg == null || _fclg.IsDisposed)
            {
                _fclg = new FormCetakLaporanGudang(this, 3);
                _fclg.Show();
            }
        }

        private void pengeluaranProdukToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            foreach (var form in Application.OpenForms.Cast<Form>().Where(f => f.IsMdiChild).ToArray()) // ToArray necessary to build a snapshot
                form.Close();
            if (_fclg == null || _fclg.IsDisposed)
            {
                _fclg = new FormCetakLaporanGudang(this, 4);
                _fclg.Show();
            }
        }
    }
}
