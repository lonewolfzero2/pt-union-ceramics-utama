﻿using System.Windows.Forms;

namespace PT_Union_Ceramics_Utama.Controller.Core
{
    public class ActiveUser
    {
        // There should be only one copy of UserLogin exists
        private readonly static ActiveUser _instance = new ActiveUser();
        public static ActiveUser Instance { get { return _instance; } }
        private ActiveUser(){}


        public string Id;
        public string Name;
        public string Position;


        private bool _isLoggedIn;
        public void DoLogin(string id, string name, string position)
        {
            if (_isLoggedIn) return;
            _isLoggedIn = true;

            Id = id;
            Name = name;
            Position = position;

            MessageBox.Show(@"Login Sukses, " + Name, @"Konfirmasi Login", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public void DoLogout()
        {
            _isLoggedIn = false;
            Id = "";
            Name = "";
            Position = "";
        }
    }
}
