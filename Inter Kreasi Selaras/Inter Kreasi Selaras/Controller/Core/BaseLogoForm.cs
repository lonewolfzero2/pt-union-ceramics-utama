﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using PT_Union_Ceramics_Utama.Controller.Helpers;

namespace PT_Union_Ceramics_Utama.Controller.Core
{
    public partial class BaseLogoForm : BaseForm
    {
        public BaseLogoForm()
        {}

        protected BaseLogoForm(Form parent) : base (parent)
        {
            InitializeComponent();
        }

        public override void Initiate()
        {
            base.Initiate();
            
            // Update Logo Position
            _pictureMainLogo.Size = new Size(189, 105);
            var pictureLocation = _pictureMainLogo.Location;
            pictureLocation.X = (ClientSize.Width - _pictureMainLogo.Size.Width) / 2;
            _pictureMainLogo.Location = pictureLocation;
        }

        public void UpdateSelectedData(string header, DataGridViewRow value)
        {
            // Get all child controls recursively
            var controlCollection = Controls.GetAllChildrenOfType<Control>();

            foreach (var textbox in controlCollection.Where(textbox => textbox != null && textbox.Tag != null))
            {
                for (var i = 0; i < value.DataGridView.Columns.Count; i++)
                {
                    var column = value.DataGridView.Columns[i];
                    if (header+column.Name == textbox.Tag.ToString())
                        textbox.Text = value.Cells[i].Value.ToString();
                }
            }
        }
    }
}
