﻿using System;
using System.Windows.Forms;
using PT_Union_Ceramics_Utama.Controller.Helpers;

namespace PT_Union_Ceramics_Utama.Controller.Core
{
    public class BaseForm : Form
    {
        protected readonly DataClassesDataContext _dataContext;
        protected readonly BindingSource _bs = new BindingSource();
        private Label _labelTanggal;
        protected DataGridView _defaultDataGridView;

        public BaseForm() : this(null) { }

        protected BaseForm(Form parent)
        {
            InitializeComponent();
            _dataContext = DatabaseHandler.Instance.DataClassesDataContext;

            if (parent != null)
                MdiParent = parent;
        }

        private bool _hasInitiated;

        public virtual void Initiate()
        {
            if (_hasInitiated)
                throw new Exception(string.Format("Trying to initiate a form that has already been initiated"));

            OnInitiate();

            _hasInitiated = true;
            RefreshForm();
            EnablePanel(true);
            Show();
        }

        protected virtual void OnInitiate()
        {
            // Update Date Label
            _labelTanggal.Text = DateTime.Now.ToLongDateString();

            var newPoint = _labelTanggal.Location;
            newPoint.X = ClientSize.Width - _labelTanggal.Size.Width;
            _labelTanggal.Location = newPoint;
        }

        protected virtual void RefreshForm() { }

        protected virtual void EnablePanel(bool status) { }

        protected virtual void UpdateGridView(object data, DataGridView dataGridView = null)
        {
            _bs.DataSource = data;

            if (dataGridView == null)
                dataGridView = _defaultDataGridView;

            if (dataGridView != null)
            {
                dataGridView.DataSource = _bs;
                dataGridView.Refresh();
            }
            else
            {
                throw new Exception("Failed to find Default Data Grid View");
            }
        }

        private void InitializeComponent()
        {
            this._labelTanggal = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _labelTanggal
            // 
            this._labelTanggal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._labelTanggal.AutoSize = true;
            this._labelTanggal.Location = new System.Drawing.Point(0, 0);
            this._labelTanggal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this._labelTanggal.Name = "_labelTanggal";
            this._labelTanggal.Size = new System.Drawing.Size(68, 13);
            this._labelTanggal.TabIndex = 151;
            this._labelTanggal.Text = "labelTanggal";
            // 
            // BaseForm
            // 
            this.ClientSize = new System.Drawing.Size(282, 255);
            this.Controls.Add(this._labelTanggal);
            this.Name = "BaseForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }
    }
}
