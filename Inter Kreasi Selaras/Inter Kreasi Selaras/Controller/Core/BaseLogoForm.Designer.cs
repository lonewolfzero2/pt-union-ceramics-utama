﻿namespace PT_Union_Ceramics_Utama.Controller.Core
{
    partial class BaseLogoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._pictureMainLogo = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this._bs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._pictureMainLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // _pictureMainLogo
            // 
            this._pictureMainLogo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._pictureMainLogo.Image = global::PT_Union_Ceramics_Utama.Properties.Resources.logo2copy1;
            this._pictureMainLogo.Location = new System.Drawing.Point(2, 21);
            this._pictureMainLogo.Margin = new System.Windows.Forms.Padding(4);
            this._pictureMainLogo.Name = "_pictureMainLogo";
            this._pictureMainLogo.Size = new System.Drawing.Size(189, 105);
            this._pictureMainLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this._pictureMainLogo.TabIndex = 153;
            this._pictureMainLogo.TabStop = false;
            // 
            // BaseLogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this._pictureMainLogo);
            this.Name = "BaseLogoForm";
            this.Text = "BaseLogoForm";
            this.Controls.SetChildIndex(this._pictureMainLogo, 0);
            ((System.ComponentModel.ISupportInitialize)(this._bs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._pictureMainLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox _pictureMainLogo;
    }
}