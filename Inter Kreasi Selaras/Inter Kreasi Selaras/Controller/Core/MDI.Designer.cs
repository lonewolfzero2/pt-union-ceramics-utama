﻿namespace PT_Union_Ceramics_Utama.Controller.Core
{
    partial class MDI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loginToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.masterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bahanBakuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.produkToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pelangganToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pemasokToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.gudangToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.penerimaanBahanBakuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pengeluaranBahanBakuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.penerimaanProdukToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supplyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.permintaanBahanBakuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.demandToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pemesananProdukToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deliveryOrderMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.invoiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kwitansiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.laporanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stokGudangToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.penjualanToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.gudangToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.penerimaanBahanBakuToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pengeluaranProdukToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.penerimaanProdukToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pengeluaranProdukToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.loginToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.produkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pegawaiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pelangganToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pemasokToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.pemasokanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.penerimaanBarangToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.pemesananBarangToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.suratJalanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pembayaranToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.menuStrip.SuspendLayout();
            this.statusStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.masterToolStripMenuItem,
            this.gudangToolStripMenuItem,
            this.supplyToolStripMenuItem,
            this.demandToolStripMenuItem,
            this.laporanToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(706, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "MenuStrip";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loginToolStripMenuItem1,
            this.logoutToolStripMenuItem1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loginToolStripMenuItem1
            // 
            this.loginToolStripMenuItem1.Name = "loginToolStripMenuItem1";
            this.loginToolStripMenuItem1.Size = new System.Drawing.Size(112, 22);
            this.loginToolStripMenuItem1.Text = "Login";
            this.loginToolStripMenuItem1.Click += new System.EventHandler(this.loginToolStripMenuItem1_Click);
            // 
            // logoutToolStripMenuItem1
            // 
            this.logoutToolStripMenuItem1.Name = "logoutToolStripMenuItem1";
            this.logoutToolStripMenuItem1.Size = new System.Drawing.Size(112, 22);
            this.logoutToolStripMenuItem1.Text = "Logout";
            this.logoutToolStripMenuItem1.Visible = false;
            this.logoutToolStripMenuItem1.Click += new System.EventHandler(this.logoutToolStripMenuItem1_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // masterToolStripMenuItem
            // 
            this.masterToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bahanBakuToolStripMenuItem,
            this.produkToolStripMenuItem1,
            this.pelangganToolStripMenuItem1,
            this.pemasokToolStripMenuItem2});
            this.masterToolStripMenuItem.Name = "masterToolStripMenuItem";
            this.masterToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.masterToolStripMenuItem.Text = "Master";
            this.masterToolStripMenuItem.Visible = false;
            // 
            // bahanBakuToolStripMenuItem
            // 
            this.bahanBakuToolStripMenuItem.Name = "bahanBakuToolStripMenuItem";
            this.bahanBakuToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.bahanBakuToolStripMenuItem.Text = "Bahan Baku";
            this.bahanBakuToolStripMenuItem.Visible = false;
            this.bahanBakuToolStripMenuItem.Click += new System.EventHandler(this.bahanBakuToolStripMenuItem_Click);
            // 
            // produkToolStripMenuItem1
            // 
            this.produkToolStripMenuItem1.Name = "produkToolStripMenuItem1";
            this.produkToolStripMenuItem1.Size = new System.Drawing.Size(136, 22);
            this.produkToolStripMenuItem1.Text = "Produk";
            this.produkToolStripMenuItem1.Visible = false;
            this.produkToolStripMenuItem1.Click += new System.EventHandler(this.produkToolStripMenuItem1_Click);
            // 
            // pelangganToolStripMenuItem1
            // 
            this.pelangganToolStripMenuItem1.Name = "pelangganToolStripMenuItem1";
            this.pelangganToolStripMenuItem1.Size = new System.Drawing.Size(136, 22);
            this.pelangganToolStripMenuItem1.Text = "Agen";
            this.pelangganToolStripMenuItem1.Visible = false;
            this.pelangganToolStripMenuItem1.Click += new System.EventHandler(this.pelangganToolStripMenuItem1_Click);
            // 
            // pemasokToolStripMenuItem2
            // 
            this.pemasokToolStripMenuItem2.Name = "pemasokToolStripMenuItem2";
            this.pemasokToolStripMenuItem2.Size = new System.Drawing.Size(136, 22);
            this.pemasokToolStripMenuItem2.Text = "Supplier";
            this.pemasokToolStripMenuItem2.Visible = false;
            this.pemasokToolStripMenuItem2.Click += new System.EventHandler(this.pemasokToolStripMenuItem2_Click);
            // 
            // gudangToolStripMenuItem
            // 
            this.gudangToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.penerimaanBahanBakuToolStripMenuItem,
            this.pengeluaranBahanBakuToolStripMenuItem,
            this.penerimaanProdukToolStripMenuItem});
            this.gudangToolStripMenuItem.Name = "gudangToolStripMenuItem";
            this.gudangToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.gudangToolStripMenuItem.Text = "Gudang";
            this.gudangToolStripMenuItem.Visible = false;
            // 
            // penerimaanBahanBakuToolStripMenuItem
            // 
            this.penerimaanBahanBakuToolStripMenuItem.Name = "penerimaanBahanBakuToolStripMenuItem";
            this.penerimaanBahanBakuToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.penerimaanBahanBakuToolStripMenuItem.Text = "Penerimaan Bahan Baku";
            this.penerimaanBahanBakuToolStripMenuItem.Visible = false;
            this.penerimaanBahanBakuToolStripMenuItem.Click += new System.EventHandler(this.penerimaanBahanBakuToolStripMenuItem_Click_1);
            // 
            // pengeluaranBahanBakuToolStripMenuItem
            // 
            this.pengeluaranBahanBakuToolStripMenuItem.Name = "pengeluaranBahanBakuToolStripMenuItem";
            this.pengeluaranBahanBakuToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.pengeluaranBahanBakuToolStripMenuItem.Text = "Pengeluaran Bahan Baku";
            this.pengeluaranBahanBakuToolStripMenuItem.Visible = false;
            this.pengeluaranBahanBakuToolStripMenuItem.Click += new System.EventHandler(this.pengeluaranBahanBakuToolStripMenuItem_Click);
            // 
            // penerimaanProdukToolStripMenuItem
            // 
            this.penerimaanProdukToolStripMenuItem.Name = "penerimaanProdukToolStripMenuItem";
            this.penerimaanProdukToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.penerimaanProdukToolStripMenuItem.Text = "Penerimaan Produk";
            this.penerimaanProdukToolStripMenuItem.Visible = false;
            this.penerimaanProdukToolStripMenuItem.Click += new System.EventHandler(this.penerimaanProdukToolStripMenuItem_Click);
            // 
            // supplyToolStripMenuItem
            // 
            this.supplyToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.permintaanBahanBakuToolStripMenuItem});
            this.supplyToolStripMenuItem.Name = "supplyToolStripMenuItem";
            this.supplyToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.supplyToolStripMenuItem.Text = "Pembelian";
            this.supplyToolStripMenuItem.Visible = false;
            // 
            // permintaanBahanBakuToolStripMenuItem
            // 
            this.permintaanBahanBakuToolStripMenuItem.Name = "permintaanBahanBakuToolStripMenuItem";
            this.permintaanBahanBakuToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.permintaanBahanBakuToolStripMenuItem.Text = "Permintaan Bahan Baku";
            this.permintaanBahanBakuToolStripMenuItem.Visible = false;
            this.permintaanBahanBakuToolStripMenuItem.Click += new System.EventHandler(this.perToolStripMenuItem_Click);
            // 
            // demandToolStripMenuItem
            // 
            this.demandToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pemesananProdukToolStripMenuItem,
            this.deliveryOrderMenuItem,
            this.invoiceToolStripMenuItem,
            this.kwitansiToolStripMenuItem});
            this.demandToolStripMenuItem.Name = "demandToolStripMenuItem";
            this.demandToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.demandToolStripMenuItem.Text = "Penjualan";
            this.demandToolStripMenuItem.Visible = false;
            // 
            // pemesananProdukToolStripMenuItem
            // 
            this.pemesananProdukToolStripMenuItem.Name = "pemesananProdukToolStripMenuItem";
            this.pemesananProdukToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.pemesananProdukToolStripMenuItem.Text = "Pemesanan Produk";
            this.pemesananProdukToolStripMenuItem.Visible = false;
            this.pemesananProdukToolStripMenuItem.Click += new System.EventHandler(this.pemesananProdukToolStripMenuItem_Click);
            // 
            // deliveryOrderMenuItem
            // 
            this.deliveryOrderMenuItem.Name = "deliveryOrderMenuItem";
            this.deliveryOrderMenuItem.Size = new System.Drawing.Size(176, 22);
            this.deliveryOrderMenuItem.Text = "Surat Jalan";
            this.deliveryOrderMenuItem.Visible = false;
            this.deliveryOrderMenuItem.Click += new System.EventHandler(this.suratJalanToolStripMenuItem1_Click);
            // 
            // invoiceToolStripMenuItem
            // 
            this.invoiceToolStripMenuItem.Name = "invoiceToolStripMenuItem";
            this.invoiceToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.invoiceToolStripMenuItem.Text = "Tagihan";
            this.invoiceToolStripMenuItem.Visible = false;
            this.invoiceToolStripMenuItem.Click += new System.EventHandler(this.invoiceToolStripMenuItem_Click);
            // 
            // kwitansiToolStripMenuItem
            // 
            this.kwitansiToolStripMenuItem.Name = "kwitansiToolStripMenuItem";
            this.kwitansiToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.kwitansiToolStripMenuItem.Text = "Kwitansi";
            this.kwitansiToolStripMenuItem.Visible = false;
            this.kwitansiToolStripMenuItem.Click += new System.EventHandler(this.kwitansiToolStripMenuItem_Click);
            // 
            // laporanToolStripMenuItem
            // 
            this.laporanToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stokGudangToolStripMenuItem,
            this.penjualanToolStripMenuItem1,
            this.gudangToolStripMenuItem1});
            this.laporanToolStripMenuItem.Name = "laporanToolStripMenuItem";
            this.laporanToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.laporanToolStripMenuItem.Text = "Laporan";
            this.laporanToolStripMenuItem.Visible = false;
            // 
            // stokGudangToolStripMenuItem
            // 
            this.stokGudangToolStripMenuItem.Name = "stokGudangToolStripMenuItem";
            this.stokGudangToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.stokGudangToolStripMenuItem.Text = "Pembelian";
            this.stokGudangToolStripMenuItem.Visible = false;
            this.stokGudangToolStripMenuItem.Click += new System.EventHandler(this.stokGudangToolStripMenuItem_Click_1);
            // 
            // penjualanToolStripMenuItem1
            // 
            this.penjualanToolStripMenuItem1.Name = "penjualanToolStripMenuItem1";
            this.penjualanToolStripMenuItem1.Size = new System.Drawing.Size(130, 22);
            this.penjualanToolStripMenuItem1.Text = "Penjualan";
            this.penjualanToolStripMenuItem1.Visible = false;
            this.penjualanToolStripMenuItem1.Click += new System.EventHandler(this.penjualanToolStripMenuItem1_Click);
            // 
            // gudangToolStripMenuItem1
            // 
            this.gudangToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.penerimaanBahanBakuToolStripMenuItem1,
            this.pengeluaranProdukToolStripMenuItem,
            this.penerimaanProdukToolStripMenuItem1,
            this.pengeluaranProdukToolStripMenuItem1});
            this.gudangToolStripMenuItem1.Name = "gudangToolStripMenuItem1";
            this.gudangToolStripMenuItem1.Size = new System.Drawing.Size(130, 22);
            this.gudangToolStripMenuItem1.Text = "Gudang";
            this.gudangToolStripMenuItem1.Visible = false;
            // 
            // penerimaanBahanBakuToolStripMenuItem1
            // 
            this.penerimaanBahanBakuToolStripMenuItem1.Name = "penerimaanBahanBakuToolStripMenuItem1";
            this.penerimaanBahanBakuToolStripMenuItem1.Size = new System.Drawing.Size(202, 22);
            this.penerimaanBahanBakuToolStripMenuItem1.Text = "Penerimaan Bahan Baku";
            this.penerimaanBahanBakuToolStripMenuItem1.Click += new System.EventHandler(this.penerimaanBahanBakuToolStripMenuItem1_Click);
            // 
            // pengeluaranProdukToolStripMenuItem
            // 
            this.pengeluaranProdukToolStripMenuItem.Name = "pengeluaranProdukToolStripMenuItem";
            this.pengeluaranProdukToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.pengeluaranProdukToolStripMenuItem.Text = "Pengeluaran BahanBaku";
            this.pengeluaranProdukToolStripMenuItem.Click += new System.EventHandler(this.pengeluaranProdukToolStripMenuItem_Click);
            // 
            // penerimaanProdukToolStripMenuItem1
            // 
            this.penerimaanProdukToolStripMenuItem1.Name = "penerimaanProdukToolStripMenuItem1";
            this.penerimaanProdukToolStripMenuItem1.Size = new System.Drawing.Size(202, 22);
            this.penerimaanProdukToolStripMenuItem1.Text = "Penerimaan Produk";
            this.penerimaanProdukToolStripMenuItem1.Click += new System.EventHandler(this.penerimaanProdukToolStripMenuItem1_Click);
            // 
            // pengeluaranProdukToolStripMenuItem1
            // 
            this.pengeluaranProdukToolStripMenuItem1.Name = "pengeluaranProdukToolStripMenuItem1";
            this.pengeluaranProdukToolStripMenuItem1.Size = new System.Drawing.Size(202, 22);
            this.pengeluaranProdukToolStripMenuItem1.Text = "Pengeluaran Produk";
            this.pengeluaranProdukToolStripMenuItem1.Click += new System.EventHandler(this.pengeluaranProdukToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loginToolStripMenuItem,
            this.logoutToolStripMenuItem,
            this.exitToolStripMenuItem1});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(35, 20);
            this.toolStripMenuItem1.Text = "File";
            // 
            // loginToolStripMenuItem
            // 
            this.loginToolStripMenuItem.Name = "loginToolStripMenuItem";
            this.loginToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.loginToolStripMenuItem.Text = "Login";
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.logoutToolStripMenuItem.Text = "Logout";
            // 
            // exitToolStripMenuItem1
            // 
            this.exitToolStripMenuItem1.Name = "exitToolStripMenuItem1";
            this.exitToolStripMenuItem1.Size = new System.Drawing.Size(112, 22);
            this.exitToolStripMenuItem1.Text = "Exit";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.produkToolStripMenuItem,
            this.pegawaiToolStripMenuItem,
            this.pelangganToolStripMenuItem,
            this.pemasokToolStripMenuItem});
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(52, 20);
            this.toolStripMenuItem2.Text = "Master";
            // 
            // produkToolStripMenuItem
            // 
            this.produkToolStripMenuItem.Name = "produkToolStripMenuItem";
            this.produkToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.produkToolStripMenuItem.Text = "Produk";
            // 
            // pegawaiToolStripMenuItem
            // 
            this.pegawaiToolStripMenuItem.Name = "pegawaiToolStripMenuItem";
            this.pegawaiToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.pegawaiToolStripMenuItem.Text = "Pegawai";
            // 
            // pelangganToolStripMenuItem
            // 
            this.pelangganToolStripMenuItem.Name = "pelangganToolStripMenuItem";
            this.pelangganToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.pelangganToolStripMenuItem.Text = "Customer";
            // 
            // pemasokToolStripMenuItem
            // 
            this.pemasokToolStripMenuItem.Name = "pemasokToolStripMenuItem";
            this.pemasokToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.pemasokToolStripMenuItem.Text = "Supplier";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pemasokanToolStripMenuItem,
            this.penerimaanBarangToolStripMenuItem});
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(67, 20);
            this.toolStripMenuItem3.Text = "Pembelian";
            // 
            // pemasokanToolStripMenuItem
            // 
            this.pemasokanToolStripMenuItem.Name = "pemasokanToolStripMenuItem";
            this.pemasokanToolStripMenuItem.Size = new System.Drawing.Size(67, 22);
            // 
            // penerimaanBarangToolStripMenuItem
            // 
            this.penerimaanBarangToolStripMenuItem.Name = "penerimaanBarangToolStripMenuItem";
            this.penerimaanBarangToolStripMenuItem.Size = new System.Drawing.Size(67, 22);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pemesananBarangToolStripMenuItem,
            this.suratJalanToolStripMenuItem,
            this.pembayaranToolStripMenuItem});
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(66, 20);
            this.toolStripMenuItem4.Text = "Penjualan";
            // 
            // pemesananBarangToolStripMenuItem
            // 
            this.pemesananBarangToolStripMenuItem.Name = "pemesananBarangToolStripMenuItem";
            this.pemesananBarangToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            // 
            // suratJalanToolStripMenuItem
            // 
            this.suratJalanToolStripMenuItem.Name = "suratJalanToolStripMenuItem";
            this.suratJalanToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            // 
            // pembayaranToolStripMenuItem
            // 
            this.pembayaranToolStripMenuItem.Name = "pembayaranToolStripMenuItem";
            this.pembayaranToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.pembayaranToolStripMenuItem.Text = "Pembayaran";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(38, 17);
            this.toolStripStatusLabel.Text = "Status";
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip.Location = new System.Drawing.Point(0, 431);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(706, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "StatusStrip";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Enabled = false;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(71, 17);
            this.toolStripStatusLabel1.Text = "Copyright©";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(447, 27);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(247, 210);
            this.dataGridView1.TabIndex = 6;
            // 
            // MDI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::PT_Union_Ceramics_Utama.Properties.Resources.Background_copy1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(706, 453);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MDI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PT.Union Ceramics Utama";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion


        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem loginToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem produkToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pegawaiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pelangganToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pemasokToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem pemasokanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem penerimaanBarangToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pemesananBarangToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem suratJalanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pembayaranToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem masterToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem supplyToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem demandToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem laporanToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem bahanBakuToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem produkToolStripMenuItem1;
        public System.Windows.Forms.ToolStripMenuItem pelangganToolStripMenuItem1;
        public System.Windows.Forms.ToolStripMenuItem pemasokToolStripMenuItem2;
        public System.Windows.Forms.ToolStripMenuItem permintaanBahanBakuToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem pemesananProdukToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem deliveryOrderMenuItem;
        public System.Windows.Forms.ToolStripMenuItem invoiceToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem kwitansiToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem penjualanToolStripMenuItem1;
        public System.Windows.Forms.ToolStripMenuItem stokGudangToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem loginToolStripMenuItem1;
        public System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem1;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripMenuItem gudangToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem penerimaanBahanBakuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pengeluaranBahanBakuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem penerimaanProdukToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gudangToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem penerimaanBahanBakuToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem pengeluaranProdukToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem penerimaanProdukToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem pengeluaranProdukToolStripMenuItem1;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}



