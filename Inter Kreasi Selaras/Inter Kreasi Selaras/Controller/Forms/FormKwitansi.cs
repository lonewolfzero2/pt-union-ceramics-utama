﻿using System;
using System.Windows.Forms;
using PT_Union_Ceramics_Utama.Controller.Core;

namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    public partial class FormKwitansi : BaseLogoForm
    {
        FormPencarianTagihan _fpt;

        public string TanggalPemesanan;

        public FormKwitansi(Form parent) : base(parent)
        {
            InitializeComponent();
            SetKodeKwitansi();
            txtKodePegawai.Text = ActiveUser.Instance.Id;
            txtNamaPegawai.Text = ActiveUser.Instance.Name;
            SaveMode(true);
        }

        public override void Initiate()
        {
            _defaultDataGridView = dataGridView1;
            base.Initiate();
        }

        private void SetKodeKwitansi()
        {
            //var dataKodeKwitansi = (from q in _dataContext.PembayaranPenjualanProduks
            //                        orderby q.kdPembayaran descending
            //                        select q.kdPembayaran).Take(1).ToList();
            //txtKodeKwitansi.Text = dataKodeKwitansi.ToCode();
        }


        private void SaveMode(bool flag)
        {
            btnSave.Enabled = flag;
            btnPrint.Enabled = !flag;
            btnReset.Enabled = !flag;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (_fpt == null || _fpt.IsDisposed)
            {
                _fpt = new FormPencarianTagihan(this);
                _fpt.Initiate();
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            DateTime jatuhTempo = DateTime.Parse(txtTanggalJatuhTempo.Text);
            DateTime tanggalBayar = DateTime.Now;
            if (tanggalBayar < DateTime.Parse(TanggalPemesanan))
            {
                MessageBox.Show(@"Pemesanan ini dilakukan sebelum tanggal tersebut", @"Pembayaran", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else if (tanggalBayar <= jatuhTempo)
            {
                txtGrandTotal.Text = txtTotalTagihan.Text;
                panelButtonExecute.Enabled = true;
            }
            else
            {
                int telat = (tanggalBayar - jatuhTempo).Days;
                double bunga = telat;
                if (bunga >= 100)
                {
                    bunga = 100;
                }
                MessageBox.Show(@"Anda telat " + telat + @" hari, terkena bunga sebesar " + bunga + " %", @"Pembayaran", MessageBoxButtons.OK, MessageBoxIcon.Information);
                double tagihan = double.Parse(txtTotalTagihan.Text);
                txtGrandTotal.Text = ((tagihan) + (tagihan * bunga / 100)).ToString();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //if (string.IsNullOrEmpty(txtKodeTagihan.Text))
            //{
            //    MessageBox.Show(@"Lengkapi Data Tagihan", @"Penyimpanan Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
            //else
            //{
            //    var dataTagihan = (from q in _dataContext.PembayaranPenjualanProduks
            //                       where q.kdTagihan == txtKodeTagihan.Text
            //                       select q);
            //    if (!dataTagihan.Any())
            //    {
            //        var pembayaran = new PembayaranPenjualanProduk
            //            {
            //                kdPembayaran = txtKodeKwitansi.Text, 
            //                kdTagihan = txtKodeTagihan.Text, 
            //                kdPegawai = txtKodePegawai.Text, 
            //                tanggalBayar = DateTime.Now, 
            //                totalBayar = long.Parse(txtGrandTotal.Text)
            //            };

            //        _dataContext.PembayaranPenjualanProduks.InsertOnSubmit(pembayaran);
            //        _dataContext.SubmitChanges();
            //        MessageBox.Show(@"Transaksi berhasil", @"Penyimpanan Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //        SaveMode(false);
            //    }
            //    else
            //    {
            //        MessageBox.Show(@"No. Tagihan ini sudah pernah dibuat", @"Penyimpanan Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    }
            //}
        }

        private void txtTotalPembayaran_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {

            //var dataKodeKwitansi = (from q in _dataContext.PembayaranPenjualanProduks
            //                        orderby q.kdPembayaran descending
            //                        select q.kdPembayaran).Take(1).ToList();
            //txtKodeKwitansi.Text = dataKodeKwitansi.ToCode();

            //txtKodeTagihan.Text = "";
            //txtKodePelanggan.Text = "";
            //txtNamaPelanggan.Text = "";
            //txtTanggalJatuhTempo.Text = "";
            //UpdateGridView(null);

            //txtBunga.Text = "";
            //txtGrandTotal.Text = "";
            //SaveMode(true);

        }


        private void btnPrint_Click_1(object sender, EventArgs e)
        {
            //FormCetakKwitansi fck = new FormCetakKwitansi(txtKodeKwitansi.Text,txtKodeTagihan.Text, txtBunga.Text, txtGrandTotal.Text, txtTotalTagihan.Text);
            //fck.Show();
            //Dispose();
        }
    }
}
