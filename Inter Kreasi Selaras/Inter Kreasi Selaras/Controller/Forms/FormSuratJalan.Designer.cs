﻿namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    partial class FormSuratJalan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtAlamatPelanggan = new System.Windows.Forms.RichTextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtKodePegawai = new System.Windows.Forms.TextBox();
            this.btnPencarianPemesananProduk = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.txtKodePemesananProduk = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dataGridViewPemesanan = new System.Windows.Forms.DataGridView();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTanggalPemesanan = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNamaPelanggan = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtKodePelanggan = new System.Windows.Forms.TextBox();
            this.txtKodeSuratJalan = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panelButtonExecute = new System.Windows.Forms.Panel();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this._bs)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPemesanan)).BeginInit();
            this.panelButtonExecute.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtAlamatPelanggan
            // 
            this.txtAlamatPelanggan.Enabled = false;
            this.txtAlamatPelanggan.Location = new System.Drawing.Point(216, 192);
            this.txtAlamatPelanggan.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtAlamatPelanggan.Name = "txtAlamatPelanggan";
            this.txtAlamatPelanggan.ReadOnly = true;
            this.txtAlamatPelanggan.Size = new System.Drawing.Size(213, 61);
            this.txtAlamatPelanggan.TabIndex = 144;
            this.txtAlamatPelanggan.Text = "";
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.SystemColors.ControlLight;
            this.groupBox5.Controls.Add(this.dateTimePicker1);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Location = new System.Drawing.Point(19, 476);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox5.Size = new System.Drawing.Size(511, 85);
            this.groupBox5.TabIndex = 168;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Informasi Pengiriman";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(216, 36);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(260, 22);
            this.dateTimePicker1.TabIndex = 166;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(371, 39);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 17);
            this.label5.TabIndex = 165;
            this.label5.Text = "-";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(255, 41);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(13, 17);
            this.label10.TabIndex = 164;
            this.label10.Text = "-";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 39);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(95, 17);
            this.label11.TabIndex = 160;
            this.label11.Text = "Tanggal Kirim";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtKodePegawai);
            this.groupBox2.Controls.Add(this.btnPencarianPemesananProduk);
            this.groupBox2.Controls.Add(this.txtAlamatPelanggan);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.txtKodePemesananProduk);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.txtTanggalPemesanan);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtNamaPelanggan);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtKodePelanggan);
            this.groupBox2.Location = new System.Drawing.Point(20, 191);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(1184, 278);
            this.groupBox2.TabIndex = 167;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Pemesanan Produk";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 69);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(161, 17);
            this.label2.TabIndex = 147;
            this.label2.Text = "Kode Pegawai Pemesan";
            // 
            // txtKodePegawai
            // 
            this.txtKodePegawai.Enabled = false;
            this.txtKodePegawai.Location = new System.Drawing.Point(216, 65);
            this.txtKodePegawai.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtKodePegawai.Name = "txtKodePegawai";
            this.txtKodePegawai.ReadOnly = true;
            this.txtKodePegawai.Size = new System.Drawing.Size(213, 22);
            this.txtKodePegawai.TabIndex = 146;
            // 
            // btnPencarianPemesananProduk
            // 
            this.btnPencarianPemesananProduk.BackgroundImage = global::PT_Union_Ceramics_Utama.Properties.Resources.search;
            this.btnPencarianPemesananProduk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPencarianPemesananProduk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPencarianPemesananProduk.Location = new System.Drawing.Point(403, 31);
            this.btnPencarianPemesananProduk.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnPencarianPemesananProduk.Name = "btnPencarianPemesananProduk";
            this.btnPencarianPemesananProduk.Size = new System.Drawing.Size(28, 26);
            this.btnPencarianPemesananProduk.TabIndex = 145;
            this.btnPencarianPemesananProduk.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnPencarianPemesananProduk.UseVisualStyleBackColor = true;
            this.btnPencarianPemesananProduk.Click += new System.EventHandler(this.btnPencarianPemesananProduk_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(17, 196);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(123, 17);
            this.label8.TabIndex = 143;
            this.label8.Text = "Alamat Pelanggan";
            // 
            // txtKodePemesananProduk
            // 
            this.txtKodePemesananProduk.Enabled = false;
            this.txtKodePemesananProduk.Location = new System.Drawing.Point(216, 31);
            this.txtKodePemesananProduk.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtKodePemesananProduk.Name = "txtKodePemesananProduk";
            this.txtKodePemesananProduk.Size = new System.Drawing.Size(180, 22);
            this.txtKodePemesananProduk.TabIndex = 142;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dataGridViewPemesanan);
            this.groupBox3.Location = new System.Drawing.Point(460, 23);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Size = new System.Drawing.Size(697, 230);
            this.groupBox3.TabIndex = 141;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Detail Pengiriman Produk";
            // 
            // dataGridViewPemesanan
            // 
            this.dataGridViewPemesanan.AllowUserToAddRows = false;
            this.dataGridViewPemesanan.AllowUserToDeleteRows = false;
            this.dataGridViewPemesanan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPemesanan.Location = new System.Drawing.Point(19, 30);
            this.dataGridViewPemesanan.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataGridViewPemesanan.Name = "dataGridViewPemesanan";
            this.dataGridViewPemesanan.ReadOnly = true;
            this.dataGridViewPemesanan.RowTemplate.Height = 24;
            this.dataGridViewPemesanan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewPemesanan.Size = new System.Drawing.Size(661, 177);
            this.dataGridViewPemesanan.TabIndex = 143;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(17, 164);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(139, 17);
            this.label9.TabIndex = 140;
            this.label9.Text = "Tanggal Pemesanan";
            // 
            // txtTanggalPemesanan
            // 
            this.txtTanggalPemesanan.Enabled = false;
            this.txtTanggalPemesanan.Location = new System.Drawing.Point(216, 160);
            this.txtTanggalPemesanan.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtTanggalPemesanan.Name = "txtTanggalPemesanan";
            this.txtTanggalPemesanan.ReadOnly = true;
            this.txtTanggalPemesanan.Size = new System.Drawing.Size(213, 22);
            this.txtTanggalPemesanan.TabIndex = 139;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 132);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(117, 17);
            this.label6.TabIndex = 138;
            this.label6.Text = "Nama Pelanggan";
            // 
            // txtNamaPelanggan
            // 
            this.txtNamaPelanggan.Enabled = false;
            this.txtNamaPelanggan.Location = new System.Drawing.Point(216, 128);
            this.txtNamaPelanggan.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtNamaPelanggan.Name = "txtNamaPelanggan";
            this.txtNamaPelanggan.ReadOnly = true;
            this.txtNamaPelanggan.Size = new System.Drawing.Size(213, 22);
            this.txtNamaPelanggan.TabIndex = 137;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 34);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(169, 17);
            this.label3.TabIndex = 119;
            this.label3.Text = "Kode Pemesanan Produk";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 100);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 17);
            this.label4.TabIndex = 125;
            this.label4.Text = "Kode Pelanggan";
            // 
            // txtKodePelanggan
            // 
            this.txtKodePelanggan.Enabled = false;
            this.txtKodePelanggan.Location = new System.Drawing.Point(216, 96);
            this.txtKodePelanggan.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtKodePelanggan.Name = "txtKodePelanggan";
            this.txtKodePelanggan.ReadOnly = true;
            this.txtKodePelanggan.Size = new System.Drawing.Size(213, 22);
            this.txtKodePelanggan.TabIndex = 121;
            // 
            // txtKodeSuratJalan
            // 
            this.txtKodeSuratJalan.Enabled = false;
            this.txtKodeSuratJalan.Location = new System.Drawing.Point(185, 150);
            this.txtKodeSuratJalan.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtKodeSuratJalan.Name = "txtKodeSuratJalan";
            this.txtKodeSuratJalan.ReadOnly = true;
            this.txtKodeSuratJalan.Size = new System.Drawing.Size(264, 22);
            this.txtKodeSuratJalan.TabIndex = 165;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 154);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 17);
            this.label1.TabIndex = 166;
            this.label1.Text = "Kode Surat Jalan";
            // 
            // panelButtonExecute
            // 
            this.panelButtonExecute.Controls.Add(this.btnPrint);
            this.panelButtonExecute.Controls.Add(this.btnSave);
            this.panelButtonExecute.Location = new System.Drawing.Point(475, 569);
            this.panelButtonExecute.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panelButtonExecute.Name = "panelButtonExecute";
            this.panelButtonExecute.Size = new System.Drawing.Size(291, 46);
            this.panelButtonExecute.TabIndex = 170;
            // 
            // btnPrint
            // 
            this.btnPrint.Image = global::PT_Union_Ceramics_Utama.Properties.Resources.agt_print;
            this.btnPrint.Location = new System.Drawing.Point(36, 4);
            this.btnPrint.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(100, 34);
            this.btnPrint.TabIndex = 155;
            this.btnPrint.Text = "Cetak";
            this.btnPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnSave
            // 
            this.btnSave.Image = global::PT_Union_Ceramics_Utama.Properties.Resources.save;
            this.btnSave.Location = new System.Drawing.Point(159, 4);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 34);
            this.btnSave.TabIndex = 154;
            this.btnSave.Text = "Simpan";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnReset
            // 
            this.btnReset.Image = global::PT_Union_Ceramics_Utama.Properties.Resources.undod;
            this.btnReset.Location = new System.Drawing.Point(1112, 572);
            this.btnReset.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(92, 34);
            this.btnReset.TabIndex = 153;
            this.btnReset.Text = "Reset";
            this.btnReset.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReset.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // FormSuratJalan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1223, 615);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.txtKodeSuratJalan);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panelButtonExecute);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FormSuratJalan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form Surat Jalan";
            this.Controls.SetChildIndex(this.panelButtonExecute, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.txtKodeSuratJalan, 0);
            this.Controls.SetChildIndex(this.groupBox2, 0);
            this.Controls.SetChildIndex(this.groupBox5, 0);
            this.Controls.SetChildIndex(this.btnReset, 0);
            ((System.ComponentModel.ISupportInitialize)(this._bs)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPemesanan)).EndInit();
            this.panelButtonExecute.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.RichTextBox txtAlamatPelanggan;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnPencarianPemesananProduk;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txtKodePemesananProduk;
        private System.Windows.Forms.GroupBox groupBox3;
        public System.Windows.Forms.DataGridView dataGridViewPemesanan;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.TextBox txtTanggalPemesanan;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox txtNamaPelanggan;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtKodePelanggan;
        public System.Windows.Forms.TextBox txtKodeSuratJalan;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Panel panelButtonExecute;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox txtKodePegawai;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
    }
}