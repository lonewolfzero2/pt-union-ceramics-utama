﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using PT_Union_Ceramics_Utama.Controller.Helpers;
using PT_Union_Ceramics_Utama.Controller.Report;

namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    public partial class FormCetakPemesananProduk : Form
    {
        private readonly String _kode;

        public FormCetakPemesananProduk(String kode)
        {
            InitializeComponent();
            _kode = kode;

            var conn = DatabaseHandler.Instance.SqlConnection;

            string tanggal = DateTime.Today.Date.ToShortDateString();
            String p = "select tanggalorder='"+tanggal+"',spp.idOrder AS 'idorder', spp.idPegawai AS 'idpegawai', spp.idAgen AS 'idagen' , a.nama AS 'namaagen', a.alamat AS 'alamatagen', a.noTelepon AS 'notelepon',p.idProduk AS 'idproduk', p.namaProduk AS 'namaproduk', dsp.jumlahDipesan AS 'jumlahdipesan', dsp.subtotal AS 'subtotal', p.harga AS 'harga' from SuratPemesananProduk spp join DetailSuratPemesananProduk dsp on spp.idOrder=dsp.idOrder join Produk p on dsp.idProduk=p.idProduk join Agen a on a.kdPelanggan=spp.idAgen where spp.idOrder='" + _kode + "'";
            SqlDataAdapter data1 = new SqlDataAdapter(p, conn);

            DataSet1 ds = new DataSet1();
            data1.Fill(ds, "Penjualanproduk");

            PemesananProduk cr = new PemesananProduk();
            cr.SetDataSource(ds);
            crystalReportViewer1.ReportSource = cr;
            conn.Close();
        }
    }
}
