﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    public partial class FormPencarianPelanggan : GridForm
    {
        private string _kode = "";
        private string _nama = "";
        private string _alamat = "";
        private string _telepon = "";
        private readonly FormPemesananProduk _myFormPemesananProduk;

        public FormPencarianPelanggan(FormPemesananProduk parent) : base (null)
        {
            InitializeComponent();
            _myFormPemesananProduk = parent;

            cmbSearch.SelectedIndex = 0;
        }

        public override void Initiate()
        {
            _defaultDataGridView = dataGridView1;
            base.Initiate();
        }

        protected override object _data
        {
            get
            {
                return from a in _dataContext.Agens
                       select new {a.kdPelanggan, a.tanggalPendaftaran, a.nama, a.noTelepon, a.noFax, a.jenisKelamin, a.email, a.kota, a.alamat};
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtKataKunci.Text.Equals(""))
            {
                MessageBox.Show(@"Isi kata kuncinya dulu", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (cmbSearch.SelectedIndex == 0)
                {
                    try
                    {
                        var dataKodePelanggan = (from q in _dataContext.Agens
                                                 where q.kdPelanggan.Contains(txtKataKunci.Text)
                                                 select q).First();
                        UpdateGridView(dataKodePelanggan);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show(@"Data tidak ditemukan", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                else if (cmbSearch.SelectedIndex == 1)
                {
                    try
                    {
                        var dataNamaPelanggan = (from q in _dataContext.Agens
                                                 where q.nama.Contains(txtKataKunci.Text)
                                                 select q).ToList();
                        UpdateGridView(dataNamaPelanggan);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show(@"Data tidak ditemukan", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            RefreshForm();
        }

        private void btnPilih_Click(object sender, EventArgs e)
        {
            if (_kode.Equals(""))
            {
                MessageBox.Show(@"Pilih dulu datanya", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                _myFormPemesananProduk.txtKodePelanggan.Text = _kode;
                _myFormPemesananProduk.txtNamaPelanggan.Text = _nama;
                _myFormPemesananProduk.txtAlamat.Text = _alamat;
                _myFormPemesananProduk.txtTelepon.Text = _telepon;
                Dispose();
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            _kode = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            _nama = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            _telepon = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();
            _alamat = dataGridView1.Rows[e.RowIndex].Cells[8].Value.ToString();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            _kode = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            _nama = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            _telepon = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();
            _alamat = dataGridView1.Rows[e.RowIndex].Cells[8].Value.ToString();
        }
    }
}
