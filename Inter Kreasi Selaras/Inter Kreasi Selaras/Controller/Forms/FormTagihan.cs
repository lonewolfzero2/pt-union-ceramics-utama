﻿using System;
using System.Linq;
using System.Windows.Forms;
using PT_Union_Ceramics_Utama.Controller.Core;
using PT_Union_Ceramics_Utama.Controller.Helpers;

namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    public partial class FormTagihan : BaseLogoForm
    {
        FormPencarianSuratJalan _fpsj;
        public FormTagihan(Form parent) : base(parent)
        {
            InitializeComponent();
        }

        public override void Initiate()
        {
            _defaultDataGridView = dataGridView1;
            SetKodeTagihan();
            SaveMode(true);

            base.Initiate();
        }

        private void SetKodeTagihan()
        {
            var dataKodeTagihan = (from q in _dataContext.Tagihans
                                     orderby q.idTagihan descending
                                     select q.idTagihan).Take(1).ToList();
            txtKodeTagihan.Text = dataKodeTagihan.ToCode();
        }

        private void SaveMode(bool flag)
        {
            btnSave.Enabled = flag;
            btnPrint.Enabled = !flag;
            btnReset.Enabled = !flag;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (_fpsj == null || _fpsj.IsDisposed)
            {
                _fpsj = new FormPencarianSuratJalan(this);
                _fpsj.Initiate();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtKodeSuratJalan.Text.Equals(""))
            {
                MessageBox.Show(@"Lengkapi data tagihan", @"Penyimpanan Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                var dataJalan = (from q in _dataContext.SuratJalans
                                     where q.idSuratJalan == txtKodeSuratJalan.Text
                                     select new { q.tanggalJalan}).First();
                DateTime tanggalJalan = DateTime.Parse(txtTanggalPengiriman.Text);
                if (tanggalJalan > dateTimePicker1.Value)
                {
                    MessageBox.Show(@"Tanggal Jatuh Tempo harus sebelum Tanggal Pemesanan", @"Penyimpanan Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    var dataSuratJalan = (from q in _dataContext.Tagihans
                                          from w in _dataContext.SuratJalans
                                       where  w.idOrder == q.deskripsi
                                       select w.idOrder);
                    if (dataSuratJalan.Any())
                    {
                        MessageBox.Show(@"No. Surat Jalan ini sudah pernah dibuat", @"Penyimpanan Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        Tagihan tagih = new Tagihan();
                        tagih.idTagihan = txtKodeTagihan.Text;
                        tagih.idSuratJalan = txtKodeSuratJalan.Text;
                        tagih.grandTotal = long.Parse(txtGrandTotal.Text.ToString());
                        tagih.idPegawai = ActiveUser.Instance.Id;
                        _dataContext.Tagihans.InsertOnSubmit(tagih);
                        _dataContext.SubmitChanges();
                        MessageBox.Show(@"Tagihan berhasil dibuat", @"Penyimpanan Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        SaveMode(false);
                    }
                }
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            SetKodeTagihan();
            txtKodeSuratJalan.Text = "";
            txtTanggalPengiriman.Text = "";
            txtKodePelanggan.Text = "";
            txtNamaPelanggan.Text = "";
            txtAlamat.Text = "";
            UpdateGridView(null, dataGridView2);

            txtSubTotal.Text = "";
            txtGrandTotal.Text = "";
            SaveMode(true);
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            FormCetakTagihan fkt = new FormCetakTagihan(txtKodeTagihan.Text,txtPajak.Text,txtGrandTotal.Text);
            fkt.Show();
            SaveMode(true);
            Dispose();
        } 
    }
}
