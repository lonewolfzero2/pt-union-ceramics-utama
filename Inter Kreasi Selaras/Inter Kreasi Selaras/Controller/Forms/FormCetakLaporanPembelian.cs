﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using PT_Union_Ceramics_Utama.Controller.Helpers;
using PT_Union_Ceramics_Utama.Controller.Report;

namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    public partial class FormCetakLaporanPembelian : Form
    {

        public FormCetakLaporanPembelian()
        {
            InitializeComponent();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var conn = DatabaseHandler.Instance.SqlConnection;
            //conn.Open();
            String taw = tanggalawal.Value.ToString();
            String tak = tanggalakhir.Value.ToString();
            //string tanggal = DateTime.Today.Date.ToShortDateString();
            String p = "Select p.idPO AS 'IDPO', p.tanggalPO AS 'tanngalPO', p.idSupplier AS 'idsupplier', dp.jumlahDiminta AS 'jumlahminta', s.namaPerusahaan AS 'namaperusahaan', s.pic AS 'pic',bb.namaBahanBaku AS 'namabahanbaku',bb.idBahanBaku AS 'idbahanbaku', CONVERT(VARCHAR(11),'" + taw + "',6) AS 'tanggalawal', CONVERT(VARCHAR(11),'" + tak + "',6) AS 'tanggalakhir' from PurchaseOrder p join DetailPO dp on p.idPO=dp.idPO join BahanBaku bb on bb.idBahanBaku=dp.idBahanBaku join Supplier s on p.idSupplier=s.idSupplier where p.tanggalPO Between'" + taw + "'and'" + tak + "'";
            SqlDataAdapter data1 = new SqlDataAdapter(p, conn);

            DataSet1 ds = new DataSet1();
            data1.Fill(ds, "Pembelian");

            SuratPemesananBB spbb = new SuratPemesananBB();
            spbb.SetDataSource(ds);
            crystalReportViewer1.ReportSource = spbb;
            conn.Close();
        }
    }
}
