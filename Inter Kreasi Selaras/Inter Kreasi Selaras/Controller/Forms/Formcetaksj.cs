﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using PT_Union_Ceramics_Utama.Controller.Helpers;
using PT_Union_Ceramics_Utama.Controller.Report;


namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    public partial class Formcetaksj : Form
    {
        private readonly String _kode;
        private readonly String _tanggal;

        public Formcetaksj(String kode, String tanggal)
        {
            InitializeComponent();
            _kode = kode;
            _tanggal = tanggal;

            var conn = DatabaseHandler.Instance.SqlConnection;

            DateTime TanggalCetak=DateTime.Now;
            String p = "Select sj.idSuratJalan AS 'kdsuratjalan',sj.tanggalJalan AS 'tanggaljalan',tanggalcetak='" + TanggalCetak + "',a.kdPelanggan AS 'kodepelanggan',a.nama AS 'namapelanggan',a.noTelepon AS 'notelponpelanggan',p.idProduk AS 'kodeproduk',a.nama AS 'namaproduk',a.alamat AS 'alamatpelanggan',dsp.jumlahDipesan AS 'jumlahpesan'  From SuratJalan sj join SuratPemesananProduk spp on sj.idOrder=spp.idOrder join DetailSuratPemesananProduk dsp on spp.idOrder=dsp.idOrder join Produk p on dsp.idProduk=p.idProduk join Agen a on a.kdPelanggan=spp.idAgen where sj.idSuratJalan='" + _kode + "'";
            SqlDataAdapter data1 = new SqlDataAdapter(p, conn);

            DataSet1 ds = new DataSet1();
            data1.Fill(ds, "SuratJalan");

            var cr = new Report.SuratJalan();
            cr.SetDataSource(ds);
            crystalReportViewer1.ReportSource = cr;
            conn.Close();
        }
    }
}
