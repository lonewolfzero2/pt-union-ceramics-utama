﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    public partial class FormPencarianSuratJalan : GridForm
    {
        private string _kodeSuratJalan = "";
        private string _kodeSuratPemesanan = "";
        private string _tanggalJalan = "";
        private readonly FormTagihan _myFrmTagihan;

        public FormPencarianSuratJalan(FormTagihan parent) : base(null)
        {
            InitializeComponent();
            _myFrmTagihan = parent;

            cmbSearch.SelectedIndex = 0;
        }

        public override void Initiate()
        {
            _defaultDataGridView = dataGridView1;
            base.Initiate();
        }

        protected override object _data
        {
            get
            {
                return from q in _dataContext.SuratJalans
                       select new {q.idSuratJalan, q.idOrder, q.tanggalJalan};
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtKataKunci.Text.Equals(""))
            {
                MessageBox.Show(@"Isi kata kuncinya dulu", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (cmbSearch.SelectedIndex == 0)
                {
                    try
                    {
                        var dataSuratJalan= (from q in _dataContext.SuratJalans
                                                 where q.idSuratJalan == txtKataKunci.Text
                                                 select q).First();
                        UpdateGridView(dataSuratJalan);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show(@"Data tidak ditemukan", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                else if (cmbSearch.SelectedIndex == 1)
                {
                    try
                    {
                        var dataSuratPemesanan = (from q in _dataContext.SuratJalans
                                                  where q.idOrder == txtKataKunci.Text
                                                 select q).ToList();
                        UpdateGridView(dataSuratPemesanan);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show(@"Data tidak ditemukan", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            RefreshForm();
        }

        private void btnPilih_Click(object sender, EventArgs e)
        {
            if(_kodeSuratPemesanan.Equals(""))
            {
                MessageBox.Show(@"Pilih dulu datanya", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                var dataPelanggan = (from q in _dataContext.SuratPemesananProduks
                                         where q.idOrder == _kodeSuratPemesanan
                                         join p in _dataContext.Agens
                                             on q.idAgen equals p.kdPelanggan
                                         select new {q.idAgen, p.nama, p.alamat}).First();
                _myFrmTagihan.txtKodeSuratJalan.Text = _kodeSuratJalan;
                _myFrmTagihan.txtTanggalPengiriman.Text = _tanggalJalan;
                _myFrmTagihan.txtKodePelanggan.Text = dataPelanggan.idAgen;
                _myFrmTagihan.txtNamaPelanggan.Text = dataPelanggan.nama;
                _myFrmTagihan.txtAlamat.Text = dataPelanggan.alamat;

                var dataDetailPemesanan = (from q in _dataContext.DetailSuratPemesananProduks
                                         where q.idOrder == _kodeSuratPemesanan
                                           join p in _dataContext.Produks
                                           on q.idProduk equals p.idProduk
                                           select new { q.idProduk, p.namaProduk, p.harga, q.jumlahDipesan, q.subtotal }).ToList();
                _myFrmTagihan.dataGridView2.DataSource = dataDetailPemesanan;

                double subTotal = 0;

                for (int i = 0; i < _myFrmTagihan.dataGridView2.Rows.Count; i++)
			    {
                    subTotal += Double.Parse(_myFrmTagihan.dataGridView2.Rows[i].Cells[4].Value.ToString());
                }
                _myFrmTagihan.txtSubTotal.Text = subTotal.ToString();
                _myFrmTagihan.txtGrandTotal.Text = ((double.Parse(_myFrmTagihan.txtSubTotal.Text) * 10 / 100) + (double.Parse(_myFrmTagihan.txtSubTotal.Text))).ToString();
                _myFrmTagihan.txtPajak.Text = (double.Parse(_myFrmTagihan.txtSubTotal.Text) * 10 / 100).ToString();
                var dataTanggalPemesanan = (from q in _dataContext.SuratPemesananProduks
                                            where q.idOrder == _kodeSuratPemesanan
                                            select q.tanggalOrder).First();
                
                Dispose();
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            _kodeSuratJalan = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            _kodeSuratPemesanan = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            _tanggalJalan = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            _kodeSuratJalan = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            _kodeSuratPemesanan = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            _tanggalJalan = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
        }
    }
}
