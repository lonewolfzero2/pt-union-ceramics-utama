﻿using System;
using System.Linq;
using System.Windows.Forms;
using PT_Union_Ceramics_Utama.Controller.Core;

namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    public partial class BaseFormPencarian : GridForm
    {
        private readonly BaseLogoForm _parent;
        private readonly object _dataTable;
        private string _header;


        public BaseFormPencarian(BaseLogoForm parent, string header, object dataContext) : base(null)
        {
            InitializeComponent();
            
            _parent = parent;
            _header = header;
            _dataTable = dataContext;

            cmbSearch.SelectedIndex = 0;
        }

        public override void Initiate()
        {
            _defaultDataGridView = dataGridView1;

            base.Initiate();

            // Update the Search Category Drop Down list
            cmbSearch.Items.Clear();
            foreach (DataGridViewTextBoxColumn column in _defaultDataGridView.Columns)
                cmbSearch.Items.Add(column.Name);
        }

        protected override object _data
        {
            get { return _dataTable;}
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtKataKunci.Text))
            {
                // User does not provide any category, then we just reset the Grid
                foreach (var row in _defaultDataGridView.Rows.Cast<DataGridViewRow>().Where(row => row.Cells[0].Value != null))
                    row.Visible = true;
            }
            else
            {
                // Remove all selections
                _defaultDataGridView.CurrentCell = null;
                var index = cmbSearch.SelectedIndex;
                // Loop through all values, then adjust visibility
                foreach (DataGridViewRow row in _defaultDataGridView.Rows)
                {
                    var value = row.Cells[index].Value;
                    if (value != null)
                        row.Visible = value.ToString().Contains(txtKataKunci.Text);
                }
                _defaultDataGridView.Refresh();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            RefreshForm();
        }

        private void btnPilih_Click(object sender, EventArgs e)
        {
            if (_defaultDataGridView.CurrentRow != null)
            {
                _parent.UpdateSelectedData(_header, _defaultDataGridView.CurrentRow);
                _parent.Refresh();
                Dispose();
            }
            else
            {
                MessageBox.Show(@"Pilih dulu datanya", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
