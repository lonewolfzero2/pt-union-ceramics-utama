﻿namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    partial class FormPermintaanBahanBaku
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridPermintaan = new System.Windows.Forms.DataGridView();
            this.kdBahanBaku = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nama = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jumlah = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtNamaBahanBaku = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtJumlah = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNamaPIC = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtKodePembelian = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnSearchPemasok = new System.Windows.Forms.Button();
            this.txtKodePemasok = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.dataGridProduk = new System.Windows.Forms.DataGridView();
            this.txtNamaPerusahaan = new System.Windows.Forms.TextBox();
            this.txtAlamat = new System.Windows.Forms.RichTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnEmpty = new System.Windows.Forms.Button();
            this.btnAddToCart = new System.Windows.Forms.Button();
            this.txtKodeBahanBaku = new System.Windows.Forms.TextBox();
            this.panelButtonExecute = new System.Windows.Forms.Panel();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this._bs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPermintaan)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridProduk)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.panelButtonExecute.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridPermintaan
            // 
            this.dataGridPermintaan.AllowUserToAddRows = false;
            this.dataGridPermintaan.AllowUserToDeleteRows = false;
            this.dataGridPermintaan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridPermintaan.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.kdBahanBaku,
            this.nama,
            this.jumlah});
            this.dataGridPermintaan.Location = new System.Drawing.Point(15, 22);
            this.dataGridPermintaan.Name = "dataGridPermintaan";
            this.dataGridPermintaan.ReadOnly = true;
            this.dataGridPermintaan.RowTemplate.Height = 24;
            this.dataGridPermintaan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridPermintaan.Size = new System.Drawing.Size(375, 127);
            this.dataGridPermintaan.TabIndex = 111;
            this.dataGridPermintaan.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridPermintaan_CellContentClick);
            // 
            // kdBahanBaku
            // 
            this.kdBahanBaku.HeaderText = "Kode Bahan Baku";
            this.kdBahanBaku.Name = "kdBahanBaku";
            this.kdBahanBaku.ReadOnly = true;
            // 
            // nama
            // 
            this.nama.HeaderText = "Nama Bahan Baku";
            this.nama.Name = "nama";
            this.nama.ReadOnly = true;
            // 
            // jumlah
            // 
            this.jumlah.HeaderText = "Jumlah Permintaan";
            this.jumlah.Name = "jumlah";
            this.jumlah.ReadOnly = true;
            // 
            // txtNamaBahanBaku
            // 
            this.txtNamaBahanBaku.Enabled = false;
            this.txtNamaBahanBaku.Location = new System.Drawing.Point(122, 52);
            this.txtNamaBahanBaku.Name = "txtNamaBahanBaku";
            this.txtNamaBahanBaku.ReadOnly = true;
            this.txtNamaBahanBaku.Size = new System.Drawing.Size(153, 20);
            this.txtNamaBahanBaku.TabIndex = 67;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 13);
            this.label3.TabIndex = 52;
            this.label3.Text = "Nama Bahan Baku";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 81);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 13);
            this.label7.TabIndex = 52;
            this.label7.Text = "Jumlah Permintaan";
            // 
            // txtJumlah
            // 
            this.txtJumlah.Location = new System.Drawing.Point(122, 78);
            this.txtJumlah.Name = "txtJumlah";
            this.txtJumlah.Size = new System.Drawing.Size(45, 20);
            this.txtJumlah.TabIndex = 26;
            this.txtJumlah.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtJumlah_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 13);
            this.label5.TabIndex = 51;
            this.label5.Text = "Kode Bahan Baku";
            // 
            // txtNamaPIC
            // 
            this.txtNamaPIC.Enabled = false;
            this.txtNamaPIC.Location = new System.Drawing.Point(122, 61);
            this.txtNamaPIC.Name = "txtNamaPIC";
            this.txtNamaPIC.ReadOnly = true;
            this.txtNamaPIC.Size = new System.Drawing.Size(153, 20);
            this.txtNamaPIC.TabIndex = 71;
            this.txtNamaPIC.Tag = "Supplier.pic";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 72;
            this.label2.Text = "Kode Supplier";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 64);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 13);
            this.label6.TabIndex = 73;
            this.label6.Text = "Nama P.I.C";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 121);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 63;
            this.label1.Text = "Kode Permintaan";
            // 
            // txtKodePembelian
            // 
            this.txtKodePembelian.Enabled = false;
            this.txtKodePembelian.Location = new System.Drawing.Point(114, 118);
            this.txtKodePembelian.Name = "txtKodePembelian";
            this.txtKodePembelian.ReadOnly = true;
            this.txtKodePembelian.Size = new System.Drawing.Size(200, 20);
            this.txtKodePembelian.TabIndex = 20;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.groupBox1.Controls.Add(this.dataGridPermintaan);
            this.groupBox1.Location = new System.Drawing.Point(316, 387);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(411, 160);
            this.groupBox1.TabIndex = 116;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "List Permintaan Bahan Baku";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.groupBox2.Controls.Add(this.btnSearchPemasok);
            this.groupBox2.Controls.Add(this.txtKodePemasok);
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.txtNamaPerusahaan);
            this.groupBox2.Controls.Add(this.txtAlamat);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.txtNamaPIC);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(13, 151);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(714, 230);
            this.groupBox2.TabIndex = 118;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Supplier";
            // 
            // btnSearchPemasok
            // 
            this.btnSearchPemasok.BackgroundImage = global::PT_Union_Ceramics_Utama.Properties.Resources.search;
            this.btnSearchPemasok.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSearchPemasok.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearchPemasok.Location = new System.Drawing.Point(254, 34);
            this.btnSearchPemasok.Name = "btnSearchPemasok";
            this.btnSearchPemasok.Size = new System.Drawing.Size(21, 21);
            this.btnSearchPemasok.TabIndex = 122;
            this.btnSearchPemasok.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnSearchPemasok.UseVisualStyleBackColor = true;
            this.btnSearchPemasok.Click += new System.EventHandler(this.btnSearchPemasok_Click);
            // 
            // txtKodePemasok
            // 
            this.txtKodePemasok.Location = new System.Drawing.Point(122, 35);
            this.txtKodePemasok.Name = "txtKodePemasok";
            this.txtKodePemasok.ReadOnly = true;
            this.txtKodePemasok.Size = new System.Drawing.Size(126, 20);
            this.txtKodePemasok.TabIndex = 121;
            this.txtKodePemasok.Tag = "Supplier.idSupplier";
            this.txtKodePemasok.TextChanged += new System.EventHandler(this.txtKodePemasok_TextChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.SystemColors.ControlLight;
            this.groupBox4.Controls.Add(this.dataGridProduk);
            this.groupBox4.Location = new System.Drawing.Point(295, 24);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(405, 183);
            this.groupBox4.TabIndex = 120;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Produk Supplier";
            // 
            // dataGridProduk
            // 
            this.dataGridProduk.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridProduk.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridProduk.Location = new System.Drawing.Point(13, 19);
            this.dataGridProduk.Name = "dataGridProduk";
            this.dataGridProduk.RowTemplate.Height = 24;
            this.dataGridProduk.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridProduk.Size = new System.Drawing.Size(379, 150);
            this.dataGridProduk.TabIndex = 81;
            this.dataGridProduk.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridProduk_CellClick);
            this.dataGridProduk.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridProduk_CellContentClick);
            // 
            // txtNamaPerusahaan
            // 
            this.txtNamaPerusahaan.Enabled = false;
            this.txtNamaPerusahaan.Location = new System.Drawing.Point(122, 87);
            this.txtNamaPerusahaan.Name = "txtNamaPerusahaan";
            this.txtNamaPerusahaan.ReadOnly = true;
            this.txtNamaPerusahaan.Size = new System.Drawing.Size(153, 20);
            this.txtNamaPerusahaan.TabIndex = 79;
            this.txtNamaPerusahaan.Tag = "Supplier.namaPerusahaan";
            // 
            // txtAlamat
            // 
            this.txtAlamat.Location = new System.Drawing.Point(122, 113);
            this.txtAlamat.Name = "txtAlamat";
            this.txtAlamat.ReadOnly = true;
            this.txtAlamat.Size = new System.Drawing.Size(153, 96);
            this.txtAlamat.TabIndex = 78;
            this.txtAlamat.Tag = "Supplier.alamat";
            this.txtAlamat.Text = "";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 142);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(39, 13);
            this.label9.TabIndex = 77;
            this.label9.Text = "Alamat";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 90);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(95, 13);
            this.label10.TabIndex = 80;
            this.label10.Text = "Nama Perusahaan";
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.groupBox3.Controls.Add(this.btnDelete);
            this.groupBox3.Controls.Add(this.btnEmpty);
            this.groupBox3.Controls.Add(this.btnAddToCart);
            this.groupBox3.Controls.Add(this.txtKodeBahanBaku);
            this.groupBox3.Controls.Add(this.txtNamaBahanBaku);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.txtJumlah);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Location = new System.Drawing.Point(7, 387);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(303, 160);
            this.groupBox3.TabIndex = 119;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Informasi Bahan Baku";
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(131, 117);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(60, 32);
            this.btnDelete.TabIndex = 158;
            this.btnDelete.Text = "Hapus";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEmpty
            // 
            this.btnEmpty.Location = new System.Drawing.Point(197, 117);
            this.btnEmpty.Name = "btnEmpty";
            this.btnEmpty.Size = new System.Drawing.Size(90, 32);
            this.btnEmpty.TabIndex = 157;
            this.btnEmpty.Text = "Kosongkan List";
            this.btnEmpty.UseVisualStyleBackColor = true;
            this.btnEmpty.Click += new System.EventHandler(this.btnEmpty_Click);
            // 
            // btnAddToCart
            // 
            this.btnAddToCart.Location = new System.Drawing.Point(14, 117);
            this.btnAddToCart.Name = "btnAddToCart";
            this.btnAddToCart.Size = new System.Drawing.Size(111, 32);
            this.btnAddToCart.TabIndex = 156;
            this.btnAddToCart.Text = "Tambahkan ke List";
            this.btnAddToCart.UseVisualStyleBackColor = true;
            this.btnAddToCart.Click += new System.EventHandler(this.btnAddToCart_Click);
            // 
            // txtKodeBahanBaku
            // 
            this.txtKodeBahanBaku.Enabled = false;
            this.txtKodeBahanBaku.Location = new System.Drawing.Point(122, 26);
            this.txtKodeBahanBaku.Name = "txtKodeBahanBaku";
            this.txtKodeBahanBaku.ReadOnly = true;
            this.txtKodeBahanBaku.Size = new System.Drawing.Size(153, 20);
            this.txtKodeBahanBaku.TabIndex = 71;
            // 
            // panelButtonExecute
            // 
            this.panelButtonExecute.Controls.Add(this.btnPrint);
            this.panelButtonExecute.Controls.Add(this.btnSave);
            this.panelButtonExecute.Location = new System.Drawing.Point(308, 553);
            this.panelButtonExecute.Name = "panelButtonExecute";
            this.panelButtonExecute.Size = new System.Drawing.Size(176, 37);
            this.panelButtonExecute.TabIndex = 157;
            // 
            // btnPrint
            // 
            this.btnPrint.Image = global::PT_Union_Ceramics_Utama.Properties.Resources.agt_print;
            this.btnPrint.Location = new System.Drawing.Point(3, 6);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 28);
            this.btnPrint.TabIndex = 155;
            this.btnPrint.Text = "Cetak";
            this.btnPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Visible = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnSave
            // 
            this.btnSave.Image = global::PT_Union_Ceramics_Utama.Properties.Resources.save;
            this.btnSave.Location = new System.Drawing.Point(94, 6);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 28);
            this.btnSave.TabIndex = 154;
            this.btnSave.Text = "Simpan";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnReset
            // 
            this.btnReset.Image = global::PT_Union_Ceramics_Utama.Properties.Resources.undod;
            this.btnReset.Location = new System.Drawing.Point(658, 559);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(69, 28);
            this.btnReset.TabIndex = 153;
            this.btnReset.Text = "Reset";
            this.btnReset.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReset.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Visible = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // FormPermintaanBahanBaku
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(743, 595);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.panelButtonExecute);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtKodePembelian);
            this.Controls.Add(this.label1);
            this.Name = "FormPermintaanBahanBaku";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form Permintaan Bahan Baku";
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.txtKodePembelian, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.groupBox2, 0);
            this.Controls.SetChildIndex(this.groupBox3, 0);
            this.Controls.SetChildIndex(this.panelButtonExecute, 0);
            this.Controls.SetChildIndex(this.btnReset, 0);
            ((System.ComponentModel.ISupportInitialize)(this._bs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPermintaan)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridProduk)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panelButtonExecute.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridPermintaan;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtJumlah;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtKodePembelian;
        private System.Windows.Forms.TextBox txtNamaBahanBaku;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtKodeBahanBaku;
        private System.Windows.Forms.Button btnSearchPemasok;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnEmpty;
        private System.Windows.Forms.Button btnAddToCart;
        private System.Windows.Forms.Panel panelButtonExecute;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnReset;
        public System.Windows.Forms.TextBox txtNamaPIC;
        public System.Windows.Forms.TextBox txtNamaPerusahaan;
        public System.Windows.Forms.RichTextBox txtAlamat;
        public System.Windows.Forms.DataGridView dataGridProduk;
        public System.Windows.Forms.TextBox txtKodePemasok;
        private System.Windows.Forms.DataGridViewTextBoxColumn kdBahanBaku;
        private System.Windows.Forms.DataGridViewTextBoxColumn nama;
        private System.Windows.Forms.DataGridViewTextBoxColumn jumlah;

    }
}