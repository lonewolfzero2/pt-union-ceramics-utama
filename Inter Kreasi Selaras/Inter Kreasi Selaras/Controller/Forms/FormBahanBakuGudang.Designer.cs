﻿namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    partial class FormBahanBakuGudang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbSearch = new System.Windows.Forms.ComboBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panelPencarian = new System.Windows.Forms.GroupBox();
            this.txtKataKunci = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtNama = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtUkuran = new System.Windows.Forms.TextBox();
            this.panelBahanBaku = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtKodeBahanBaku = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this._bs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panelPencarian.SuspendLayout();
            this.panelBahanBaku.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmbSearch
            // 
            this.cmbSearch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSearch.FormattingEnabled = true;
            this.cmbSearch.Items.AddRange(new object[] {
            "Kode Bahan Baku",
            "Nama Bahan Baku",
            "ROP"});
            this.cmbSearch.Location = new System.Drawing.Point(14, 49);
            this.cmbSearch.Name = "cmbSearch";
            this.cmbSearch.Size = new System.Drawing.Size(131, 21);
            this.cmbSearch.TabIndex = 161;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(15, 120);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(579, 150);
            this.dataGridView1.TabIndex = 160;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // panelPencarian
            // 
            this.panelPencarian.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panelPencarian.Controls.Add(this.cmbSearch);
            this.panelPencarian.Controls.Add(this.txtKataKunci);
            this.panelPencarian.Controls.Add(this.btnSearch);
            this.panelPencarian.Controls.Add(this.btnRefresh);
            this.panelPencarian.Controls.Add(this.label14);
            this.panelPencarian.Controls.Add(this.label9);
            this.panelPencarian.Location = new System.Drawing.Point(324, 279);
            this.panelPencarian.Name = "panelPencarian";
            this.panelPencarian.Size = new System.Drawing.Size(271, 117);
            this.panelPencarian.TabIndex = 163;
            this.panelPencarian.TabStop = false;
            this.panelPencarian.Text = "Pencarian";
            // 
            // txtKataKunci
            // 
            this.txtKataKunci.Location = new System.Drawing.Point(158, 49);
            this.txtKataKunci.Name = "txtKataKunci";
            this.txtKataKunci.Size = new System.Drawing.Size(100, 20);
            this.txtKataKunci.TabIndex = 162;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(58, 88);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 160;
            this.btnSearch.Text = "Cari";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(139, 88);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnRefresh.TabIndex = 159;
            this.btnRefresh.Text = "Muat Ulang";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(155, 26);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 13);
            this.label14.TabIndex = 41;
            this.label14.Text = "Kata Kunci :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 24);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(97, 13);
            this.label9.TabIndex = 29;
            this.label9.Text = "Cari Berdasarkan : ";
            // 
            // txtNama
            // 
            this.txtNama.Location = new System.Drawing.Point(122, 49);
            this.txtNama.Name = "txtNama";
            this.txtNama.Size = new System.Drawing.Size(155, 20);
            this.txtNama.TabIndex = 15;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(168, 76);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(20, 13);
            this.label7.TabIndex = 123;
            this.label7.Text = "Kg";
            // 
            // txtUkuran
            // 
            this.txtUkuran.Location = new System.Drawing.Point(122, 72);
            this.txtUkuran.Name = "txtUkuran";
            this.txtUkuran.Size = new System.Drawing.Size(44, 20);
            this.txtUkuran.TabIndex = 122;
            // 
            // panelBahanBaku
            // 
            this.panelBahanBaku.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panelBahanBaku.Controls.Add(this.label7);
            this.panelBahanBaku.Controls.Add(this.txtUkuran);
            this.panelBahanBaku.Controls.Add(this.label1);
            this.panelBahanBaku.Controls.Add(this.label5);
            this.panelBahanBaku.Controls.Add(this.label2);
            this.panelBahanBaku.Controls.Add(this.txtKodeBahanBaku);
            this.panelBahanBaku.Controls.Add(this.txtNama);
            this.panelBahanBaku.Location = new System.Drawing.Point(16, 279);
            this.panelBahanBaku.Name = "panelBahanBaku";
            this.panelBahanBaku.Size = new System.Drawing.Size(296, 142);
            this.panelBahanBaku.TabIndex = 162;
            this.panelBahanBaku.TabStop = false;
            this.panelBahanBaku.Text = "Detail Informasi";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Kode Bahan Baku";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "Berat";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Nama";
            // 
            // txtKodeBahanBaku
            // 
            this.txtKodeBahanBaku.Location = new System.Drawing.Point(122, 23);
            this.txtKodeBahanBaku.Name = "txtKodeBahanBaku";
            this.txtKodeBahanBaku.ReadOnly = true;
            this.txtKodeBahanBaku.Size = new System.Drawing.Size(155, 20);
            this.txtKodeBahanBaku.TabIndex = 14;
            // 
            // FormBahanBakuGudang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(612, 428);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.panelPencarian);
            this.Controls.Add(this.panelBahanBaku);
            this.Name = "FormBahanBakuGudang";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Master Bahan Baku";
            this.Controls.SetChildIndex(this.panelBahanBaku, 0);
            this.Controls.SetChildIndex(this.panelPencarian, 0);
            this.Controls.SetChildIndex(this.dataGridView1, 0);
            ((System.ComponentModel.ISupportInitialize)(this._bs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panelPencarian.ResumeLayout(false);
            this.panelPencarian.PerformLayout();
            this.panelBahanBaku.ResumeLayout(false);
            this.panelBahanBaku.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbSearch;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox panelPencarian;
        private System.Windows.Forms.TextBox txtKataKunci;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtNama;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtUkuran;
        private System.Windows.Forms.GroupBox panelBahanBaku;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtKodeBahanBaku;

    }
}