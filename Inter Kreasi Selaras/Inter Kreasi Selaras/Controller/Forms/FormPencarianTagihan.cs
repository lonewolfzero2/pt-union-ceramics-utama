﻿using System;
using System.Linq;
using System.Windows.Forms;
using PT_Union_Ceramics_Utama.Controller.Core;

namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    public partial class FormPencarianTagihan : GridForm
    {
        private string _kodeTagihan = "", _kdSuratJalan = "";
        private double _grandTotal;
        private DateTime _tanggalJatuhTempo;

        private readonly FormKwitansi _myFrmKwitansi;

        public FormPencarianTagihan(FormKwitansi parent) : base(null)
        {
            InitializeComponent();
            _myFrmKwitansi = parent;

            cmbSearch.SelectedIndex = 0;
        }

        public override void Initiate()
        {
            _defaultDataGridView = dataGridView1;
            base.Initiate();
        }

        protected override object _data
        {
            get
            {
                return from q in _dataContext.Tagihans
                       from w in _dataContext.SuratJalans
                       where w.idSuratJalan== q.idSuratJalan
                       select new {q.idTagihan, w.idSuratJalan, q.grandTotal};
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtKataKunci.Text.Equals(""))
            {
                MessageBox.Show(@"Isi kata kuncinya dulu", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (cmbSearch.SelectedIndex == 0)
                {
                    try
                    {
                        var dataTagihan = (from q in _dataContext.Tagihans
                                              where q.idTagihan == txtKataKunci.Text
                                              select q).First();
                        UpdateGridView(dataTagihan);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show(@"Data tidak ditemukan", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                else if (cmbSearch.SelectedIndex == 1)
                {
                    try
                    {
                        var dataTagihan = (from q in _dataContext.Tagihans
                                           where q.idTagihan == txtKataKunci.Text
                                                  select q).ToList();
                        UpdateGridView(dataTagihan);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show(@"Data tidak ditemukan", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            RefreshForm();
        }

        private void btnPilih_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(_kodeTagihan))
            {
                MessageBox.Show(@"Pilih dulu datanya", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                var dataPelanggan = (from q in _dataContext.SuratJalans
                            where q.idSuratJalan == _kdSuratJalan
                            join p in _dataContext.SuratPemesananProduks
                                on q.idOrder equals p.idOrder
                            join r in _dataContext.Agens
                                on p.idAgen equals r.kdPelanggan
                            select new { r.kdPelanggan, r.nama, p.idOrder}).First();
                _myFrmKwitansi.txtKodeTagihan.Text = _kodeTagihan;
                _myFrmKwitansi.txtKodePelanggan.Text = dataPelanggan.kdPelanggan;
                _myFrmKwitansi.txtNamaPelanggan.Text = dataPelanggan.nama;

                var dataDetailPemesanan = (from q in _dataContext.DetailSuratPemesananProduks
                                           where q.idOrder == dataPelanggan.idOrder
                                           join p in _dataContext.Produks
                                           on q.idProduk equals p.idProduk
                                           select new { q.idProduk, p.namaProduk, p.harga, q.jumlahDipesan, q.subtotal }).ToList();
                _myFrmKwitansi.dataGridView1.DataSource = dataDetailPemesanan;

                _myFrmKwitansi.txtTotalTagihan.Text = _grandTotal.ToString();
                _myFrmKwitansi.txtTanggalJatuhTempo.Text = _tanggalJatuhTempo.ToLongDateString();

                var dataTanggalPemesanan = (from q in _dataContext.SuratPemesananProduks
                                            where q.idOrder == dataPelanggan.idOrder
                                            select q).First();
                _myFrmKwitansi.TanggalPemesanan = dataTanggalPemesanan.tanggalOrder.ToString();
                if (DateTime.Now <= _tanggalJatuhTempo)
                {
                    _myFrmKwitansi.txtBunga.Text = "0";
                }
                else if (DateTime.Now > _tanggalJatuhTempo)
                {
                    int selisih = (DateTime.Now - _tanggalJatuhTempo).Days;
                    if(selisih>=100) selisih = 100;
                    _myFrmKwitansi.txtBunga.Text = (selisih.ToString());
                }
                _myFrmKwitansi.txtGrandTotal.Text = (double.Parse(_myFrmKwitansi.txtTotalTagihan.Text)+(double.Parse(_myFrmKwitansi.txtTotalTagihan.Text) * (double.Parse(_myFrmKwitansi.txtBunga.Text) / 100))).ToString();
                _myFrmKwitansi.panelButtonExecute.Enabled = true;
                Dispose();
            }       
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            _kodeTagihan = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            _kdSuratJalan = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            _grandTotal = double.Parse(dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString());
            _tanggalJatuhTempo = DateTime.Parse(dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString());
        }

        private void FormPencarianTagihan_Load(object sender, EventArgs e)
        {
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            _kodeTagihan = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            _kdSuratJalan = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            _grandTotal = double.Parse(dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString());
        }

        private void txtKataKunci_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
