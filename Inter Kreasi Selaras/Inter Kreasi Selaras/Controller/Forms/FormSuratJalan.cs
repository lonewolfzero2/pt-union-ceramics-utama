﻿using System;
using System.Linq;
using System.Windows.Forms;
using PT_Union_Ceramics_Utama.Controller.Core;
using PT_Union_Ceramics_Utama.Controller.Helpers;

namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    public partial class FormSuratJalan : BaseLogoForm
    {
        public string TanggalPemesanan;
        private FormPencarianPemesanan _fppp;

        public FormSuratJalan(Form parent) : base(parent)
        {
            InitializeComponent();

            //btnPrint.Visible = false;
        }

        public override void Initiate()
        {
            SetKodeSuratJalan();
            SaveMode(true);

            _defaultDataGridView = dataGridViewPemesanan;

            base.Initiate();
        }

        private void SetKodeSuratJalan()
        {
            var dataKodeSuratJalan = (from q in _dataContext.SuratJalans
                                      orderby q.idSuratJalan descending
                                      select q.idSuratJalan).Take(1).ToList();
            txtKodeSuratJalan.Text = dataKodeSuratJalan.ToCode();
        }


        private void SaveMode(bool flag)
        {
            btnSave.Enabled = flag;
            btnPrint.Enabled = !flag;
            btnReset.Enabled = !flag;
        }

        private void btnPencarianPemesananProduk_Click(object sender, EventArgs e)
        {
            if (_fppp == null || _fppp.IsDisposed)
            {
                _fppp = new FormPencarianPemesanan(this, null);
                _fppp.Initiate();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtKodePemesananProduk.Text.Equals(""))
            {
                MessageBox.Show(@"Lengkapi data pemesanan produk", @"Penyimpanan Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                var dataSurat = (from q in _dataContext.SuratJalans
                                 where q.idOrder == txtKodePemesananProduk.Text
                                 select q.idOrder);
                if (dataSurat.Any())
                {
                    MessageBox.Show(@"Surat Jalan ini sudah dibuat", @"Penyimpanan Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (DateTime.Parse(TanggalPemesanan) > dateTimePicker1.Value)
                {
                    MessageBox.Show(@"Tanggal Surat Jalan harus sebelum Tanggal Pemesanan", @"Penyimpanan Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    SuratJalan sj = new SuratJalan();
                    sj.idSuratJalan = txtKodeSuratJalan.Text;
                    sj.idOrder = txtKodePemesananProduk.Text;
                    sj.tanggalJalan = dateTimePicker1.Value;
                    sj.idPegawai = txtKodePegawai.Text;
                    _dataContext.SuratJalans.InsertOnSubmit(sj);
                    _dataContext.SubmitChanges();
                    MessageBox.Show(@"Surat Jalan berhasil disimpan", @"Penyimpanan Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    SaveMode(false);
                }
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            SetKodeSuratJalan();
            txtKodePemesananProduk.Text = "";
            txtKodePegawai.Text = "";
            txtTanggalPemesanan.Text = "";
            txtKodePelanggan.Text = "";
            txtNamaPelanggan.Text = "";
            txtAlamatPelanggan.Text = "";
            UpdateGridView(null, dataGridViewPemesanan);
            SaveMode(true);
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            //SaveMode(true);
            string date = dateTimePicker1.Value.Date.ToShortDateString();
            Formcetaksj fcsj = new Formcetaksj(txtKodeSuratJalan.Text, date);
            fcsj.Show();
            Dispose();
        }
    }
}
