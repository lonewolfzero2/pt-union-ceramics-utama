﻿using System;
using System.Linq;
using System.Windows.Forms;
using PT_Union_Ceramics_Utama.Controller.Core;

namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    public partial class FormLogin : BaseLogoForm
    {
        public FormLogin(Form parent) : base(parent)
        {
            InitializeComponent();
            txtUsername.Text = "krywn003";
            txtPassword.Text = "tiga";
            comboBox1.SelectedIndex = 1 ;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            var dataLogin = (from q in _dataContext.Pegawais
                             where q.idPegawai == txtUsername.Text
                                   && q.password == txtPassword.Text
                                   && q.divisi == comboBox1.SelectedItem.ToString()
                             select new {q.idPegawai, q.nama, q.divisi});
            if (dataLogin.Any())
            {
                var foundUser = dataLogin.First();
                ActiveUser.Instance.DoLogin(foundUser.idPegawai, foundUser.nama, comboBox1.SelectedItem.ToString());

                var mdi = MdiParent as MDI;
                if (mdi != null)
                    mdi.SetUserLogin();
                else
                    throw new Exception("");
                Dispose();
            }
            else
            {
                MessageBox.Show(@"Login Gagal", @"Error Login", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
