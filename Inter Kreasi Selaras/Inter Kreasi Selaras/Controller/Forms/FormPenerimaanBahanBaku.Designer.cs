﻿namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    partial class FormPenerimaanBahanBaku
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtKodePenerimaan = new System.Windows.Forms.TextBox();
            this.btnAddToCart = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.datagridviewPenerimaan = new System.Windows.Forms.DataGridView();
            this.kdBahanBaku = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nama = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jumlahDiterima = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SisaPenerimaan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtKodeBahanBaku = new System.Windows.Forms.TextBox();
            this.txtKodePemasok = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtJumlahDiterima = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtKodePegawai = new System.Windows.Forms.TextBox();
            this.datagridviewPermintaan = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtKodeSuratPermintaan = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtTanggal = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNamaPerusahaan = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnEmptyCart = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.txtNamaBahanBaku = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtJumlahDiminta = new System.Windows.Forms.TextBox();
            this.panelButtonExecute = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this._bs)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datagridviewPenerimaan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datagridviewPermintaan)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.panelButtonExecute.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 129);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 65;
            this.label1.Text = "Kode Penerimaan";
            // 
            // txtKodePenerimaan
            // 
            this.txtKodePenerimaan.Enabled = false;
            this.txtKodePenerimaan.Location = new System.Drawing.Point(127, 126);
            this.txtKodePenerimaan.Name = "txtKodePenerimaan";
            this.txtKodePenerimaan.ReadOnly = true;
            this.txtKodePenerimaan.Size = new System.Drawing.Size(200, 20);
            this.txtKodePenerimaan.TabIndex = 64;
            // 
            // btnAddToCart
            // 
            this.btnAddToCart.Location = new System.Drawing.Point(43, 140);
            this.btnAddToCart.Name = "btnAddToCart";
            this.btnAddToCart.Size = new System.Drawing.Size(112, 23);
            this.btnAddToCart.TabIndex = 141;
            this.btnAddToCart.Text = "Tambahkan ke List";
            this.btnAddToCart.UseVisualStyleBackColor = true;
            this.btnAddToCart.Click += new System.EventHandler(this.btnAddToCart_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.groupBox1.Controls.Add(this.datagridviewPenerimaan);
            this.groupBox1.Location = new System.Drawing.Point(405, 361);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(492, 194);
            this.groupBox1.TabIndex = 140;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "List Penerimaan Barang";
            // 
            // datagridviewPenerimaan
            // 
            this.datagridviewPenerimaan.AllowUserToAddRows = false;
            this.datagridviewPenerimaan.AllowUserToDeleteRows = false;
            this.datagridviewPenerimaan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.datagridviewPenerimaan.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.kdBahanBaku,
            this.nama,
            this.jumlahDiterima,
            this.SisaPenerimaan});
            this.datagridviewPenerimaan.Location = new System.Drawing.Point(16, 22);
            this.datagridviewPenerimaan.Name = "datagridviewPenerimaan";
            this.datagridviewPenerimaan.ReadOnly = true;
            this.datagridviewPenerimaan.RowTemplate.Height = 24;
            this.datagridviewPenerimaan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.datagridviewPenerimaan.Size = new System.Drawing.Size(460, 156);
            this.datagridviewPenerimaan.TabIndex = 137;
            this.datagridviewPenerimaan.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.datagridviewPenerimaan_CellContentClick);
            // 
            // kdBahanBaku
            // 
            this.kdBahanBaku.HeaderText = "Kode Bahan Baku";
            this.kdBahanBaku.Name = "kdBahanBaku";
            this.kdBahanBaku.ReadOnly = true;
            // 
            // nama
            // 
            this.nama.HeaderText = "Nama Bahan Baku";
            this.nama.Name = "nama";
            this.nama.ReadOnly = true;
            // 
            // jumlahDiterima
            // 
            this.jumlahDiterima.HeaderText = "Jumlah Diterima";
            this.jumlahDiterima.Name = "jumlahDiterima";
            this.jumlahDiterima.ReadOnly = true;
            // 
            // SisaPenerimaan
            // 
            this.SisaPenerimaan.HeaderText = "Sisa Penerimaan";
            this.SisaPenerimaan.Name = "SisaPenerimaan";
            this.SisaPenerimaan.ReadOnly = true;
            // 
            // txtKodeBahanBaku
            // 
            this.txtKodeBahanBaku.Enabled = false;
            this.txtKodeBahanBaku.Location = new System.Drawing.Point(208, 24);
            this.txtKodeBahanBaku.Name = "txtKodeBahanBaku";
            this.txtKodeBahanBaku.ReadOnly = true;
            this.txtKodeBahanBaku.Size = new System.Drawing.Size(161, 20);
            this.txtKodeBahanBaku.TabIndex = 137;
            // 
            // txtKodePemasok
            // 
            this.txtKodePemasok.Enabled = false;
            this.txtKodePemasok.Location = new System.Drawing.Point(211, 90);
            this.txtKodePemasok.Name = "txtKodePemasok";
            this.txtKodePemasok.ReadOnly = true;
            this.txtKodePemasok.Size = new System.Drawing.Size(161, 20);
            this.txtKodePemasok.TabIndex = 136;
            this.txtKodePemasok.Tag = "PO.idSupplier";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 105);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(99, 13);
            this.label7.TabIndex = 131;
            this.label7.Text = "Jumlah Penerimaan";
            // 
            // txtJumlahDiterima
            // 
            this.txtJumlahDiterima.Location = new System.Drawing.Point(208, 102);
            this.txtJumlahDiterima.Name = "txtJumlahDiterima";
            this.txtJumlahDiterima.Size = new System.Drawing.Size(53, 20);
            this.txtJumlahDiterima.TabIndex = 129;
            this.txtJumlahDiterima.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtJumlahDiterima_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 13);
            this.label6.TabIndex = 130;
            this.label6.Text = "Kode Bahan Baku";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(178, 13);
            this.label3.TabIndex = 119;
            this.label3.Text = "Kode Surat Permintaan Bahan Baku";
            // 
            // txtKodePegawai
            // 
            this.txtKodePegawai.Enabled = false;
            this.txtKodePegawai.Location = new System.Drawing.Point(211, 64);
            this.txtKodePegawai.Name = "txtKodePegawai";
            this.txtKodePegawai.ReadOnly = true;
            this.txtKodePegawai.Size = new System.Drawing.Size(161, 20);
            this.txtKodePegawai.TabIndex = 121;
            this.txtKodePegawai.Tag = "PO.idPegawai";
            // 
            // datagridviewPermintaan
            // 
            this.datagridviewPermintaan.AllowUserToAddRows = false;
            this.datagridviewPermintaan.AllowUserToDeleteRows = false;
            this.datagridviewPermintaan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.datagridviewPermintaan.Location = new System.Drawing.Point(14, 19);
            this.datagridviewPermintaan.Name = "datagridviewPermintaan";
            this.datagridviewPermintaan.ReadOnly = true;
            this.datagridviewPermintaan.RowTemplate.Height = 24;
            this.datagridviewPermintaan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.datagridviewPermintaan.Size = new System.Drawing.Size(443, 126);
            this.datagridviewPermintaan.TabIndex = 127;
            this.datagridviewPermintaan.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.datagridviewPermintaan_CellClick);
            this.datagridviewPermintaan.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.datagridviewPermintaan_CellContentClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 13);
            this.label2.TabIndex = 126;
            this.label2.Text = "Kode Pegawai Pemesan";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 125;
            this.label4.Text = "Kode Supplier";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.groupBox2.Controls.Add(this.btnSearch);
            this.groupBox2.Controls.Add(this.txtKodeSuratPermintaan);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.txtTanggal);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.txtNamaPerusahaan);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtKodePemasok);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtKodePegawai);
            this.groupBox2.Location = new System.Drawing.Point(12, 157);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(885, 198);
            this.groupBox2.TabIndex = 140;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Permintaan Bahan Baku";
            // 
            // btnSearch
            // 
            this.btnSearch.BackgroundImage = global::PT_Union_Ceramics_Utama.Properties.Resources.search;
            this.btnSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearch.Location = new System.Drawing.Point(351, 38);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(21, 21);
            this.btnSearch.TabIndex = 148;
            this.btnSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtKodeSuratPermintaan
            // 
            this.txtKodeSuratPermintaan.Location = new System.Drawing.Point(211, 38);
            this.txtKodeSuratPermintaan.Name = "txtKodeSuratPermintaan";
            this.txtKodeSuratPermintaan.ReadOnly = true;
            this.txtKodeSuratPermintaan.Size = new System.Drawing.Size(134, 20);
            this.txtKodeSuratPermintaan.TabIndex = 142;
            this.txtKodeSuratPermintaan.Tag = "PO.idPO";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.datagridviewPermintaan);
            this.groupBox3.Location = new System.Drawing.Point(393, 21);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(476, 158);
            this.groupBox3.TabIndex = 141;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Detail Permintaan Bahan Baku";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(14, 146);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(102, 13);
            this.label10.TabIndex = 139;
            this.label10.Text = "Tanggal Permintaan";
            // 
            // txtTanggal
            // 
            this.txtTanggal.Enabled = false;
            this.txtTanggal.Location = new System.Drawing.Point(211, 143);
            this.txtTanggal.Name = "txtTanggal";
            this.txtTanggal.ReadOnly = true;
            this.txtTanggal.Size = new System.Drawing.Size(161, 20);
            this.txtTanggal.TabIndex = 140;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 120);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 13);
            this.label5.TabIndex = 137;
            this.label5.Text = "Nama Perusahaan";
            // 
            // txtNamaPerusahaan
            // 
            this.txtNamaPerusahaan.Enabled = false;
            this.txtNamaPerusahaan.Location = new System.Drawing.Point(211, 117);
            this.txtNamaPerusahaan.Name = "txtNamaPerusahaan";
            this.txtNamaPerusahaan.ReadOnly = true;
            this.txtNamaPerusahaan.Size = new System.Drawing.Size(161, 20);
            this.txtNamaPerusahaan.TabIndex = 138;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.SystemColors.ControlLight;
            this.groupBox4.Controls.Add(this.btnEmptyCart);
            this.groupBox4.Controls.Add(this.btnDelete);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.btnAddToCart);
            this.groupBox4.Controls.Add(this.txtNamaBahanBaku);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.txtJumlahDiminta);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.txtKodeBahanBaku);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.txtJumlahDiterima);
            this.groupBox4.Location = new System.Drawing.Point(12, 361);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(384, 194);
            this.groupBox4.TabIndex = 141;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Informasi Bahan Baku";
            // 
            // btnEmptyCart
            // 
            this.btnEmptyCart.Location = new System.Drawing.Point(245, 140);
            this.btnEmptyCart.Name = "btnEmptyCart";
            this.btnEmptyCart.Size = new System.Drawing.Size(96, 23);
            this.btnEmptyCart.TabIndex = 143;
            this.btnEmptyCart.Text = "Kosongkan List";
            this.btnEmptyCart.UseVisualStyleBackColor = true;
            this.btnEmptyCart.Click += new System.EventHandler(this.btnEmptyCart_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(161, 140);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(78, 23);
            this.btnDelete.TabIndex = 142;
            this.btnDelete.Text = "Hapus";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(15, 53);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(97, 13);
            this.label12.TabIndex = 140;
            this.label12.Text = "Nama Bahan Baku";
            // 
            // txtNamaBahanBaku
            // 
            this.txtNamaBahanBaku.Enabled = false;
            this.txtNamaBahanBaku.Location = new System.Drawing.Point(208, 50);
            this.txtNamaBahanBaku.Name = "txtNamaBahanBaku";
            this.txtNamaBahanBaku.ReadOnly = true;
            this.txtNamaBahanBaku.Size = new System.Drawing.Size(161, 20);
            this.txtNamaBahanBaku.TabIndex = 141;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(16, 79);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(78, 13);
            this.label11.TabIndex = 139;
            this.label11.Text = "Jumlah Diminta";
            // 
            // txtJumlahDiminta
            // 
            this.txtJumlahDiminta.Location = new System.Drawing.Point(208, 76);
            this.txtJumlahDiminta.Name = "txtJumlahDiminta";
            this.txtJumlahDiminta.ReadOnly = true;
            this.txtJumlahDiminta.Size = new System.Drawing.Size(54, 20);
            this.txtJumlahDiminta.TabIndex = 138;
            // 
            // panelButtonExecute
            // 
            this.panelButtonExecute.Controls.Add(this.btnSave);
            this.panelButtonExecute.Location = new System.Drawing.Point(374, 561);
            this.panelButtonExecute.Name = "panelButtonExecute";
            this.panelButtonExecute.Size = new System.Drawing.Size(170, 37);
            this.panelButtonExecute.TabIndex = 158;
            // 
            // btnSave
            // 
            this.btnSave.Image = global::PT_Union_Ceramics_Utama.Properties.Resources.save;
            this.btnSave.Location = new System.Drawing.Point(57, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 28);
            this.btnSave.TabIndex = 154;
            this.btnSave.Text = "Simpan";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnReset
            // 
            this.btnReset.Image = global::PT_Union_Ceramics_Utama.Properties.Resources.undod;
            this.btnReset.Location = new System.Drawing.Point(828, 564);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(69, 28);
            this.btnReset.TabIndex = 153;
            this.btnReset.Text = "Reset";
            this.btnReset.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReset.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Visible = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // FormPenerimaanBahanBaku
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(912, 595);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.panelButtonExecute);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtKodePenerimaan);
            this.Name = "FormPenerimaanBahanBaku";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form Penerimaan Bahan Baku";
            this.Controls.SetChildIndex(this.txtKodePenerimaan, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.groupBox2, 0);
            this.Controls.SetChildIndex(this.groupBox4, 0);
            this.Controls.SetChildIndex(this.panelButtonExecute, 0);
            this.Controls.SetChildIndex(this.btnReset, 0);
            ((System.ComponentModel.ISupportInitialize)(this._bs)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.datagridviewPenerimaan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datagridviewPermintaan)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.panelButtonExecute.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtKodePenerimaan;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtJumlahDiterima;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtKodeBahanBaku;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView datagridviewPenerimaan;
        private System.Windows.Forms.Button btnAddToCart;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtNamaBahanBaku;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtJumlahDiminta;
        private System.Windows.Forms.Button btnEmptyCart;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Panel panelButtonExecute;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnReset;
        public System.Windows.Forms.TextBox txtKodePegawai;
        public System.Windows.Forms.DataGridView datagridviewPermintaan;
        public System.Windows.Forms.TextBox txtKodePemasok;
        public System.Windows.Forms.TextBox txtNamaPerusahaan;
        public System.Windows.Forms.TextBox txtTanggal;
        public System.Windows.Forms.TextBox txtKodeSuratPermintaan;
        private System.Windows.Forms.DataGridViewTextBoxColumn kdBahanBaku;
        private System.Windows.Forms.DataGridViewTextBoxColumn nama;
        private System.Windows.Forms.DataGridViewTextBoxColumn jumlahDiterima;
        private System.Windows.Forms.DataGridViewTextBoxColumn SisaPenerimaan;
    }
}