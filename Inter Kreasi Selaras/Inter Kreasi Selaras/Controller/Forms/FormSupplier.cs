﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using PT_Union_Ceramics_Utama.Controller.Helpers;
using PT_Union_Ceramics_Utama.Controller.Helpers.Enums;

namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    public partial class FormSupplier : GridForm
    {
        private DmlModes _dmlMode;

        public FormSupplier(Form parent) : base(parent)
        {
            InitializeComponent();
            //txtTanggalPendaftaran.Text = DateTime.Now.ToShortDateString();

            cmbSearch.SelectedIndex = 1;
        }

        public override void Initiate()
        {
            _defaultDataGridView = dataGridView1;
            _textBoxCollection = new TextBoxCollection(dataGridView1, panelSupplier);
            base.Initiate();
        }

        protected override object _data
        {
            get
            {
                return from a in _dataContext.Suppliers
                       select new {a.idSupplier, a.namaPerusahaan, a.pic, a.noTelepon, a.noFax, a.email, a.kota, a.alamat};
            }
        }

        protected override void EnablePanel(bool status)
        {
            dataGridView1.Enabled = status;
            panelButtonDML.Enabled = status;
            panelButtonExecute.Enabled = !status;
            panelSupplier.Enabled = !status;
            panelPencarian.Enabled = status;
            panelButtonSearch.Enabled = status;

            if (_dmlMode == DmlModes.Memasukan) txtKodeSupplier.Enabled = true;
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            _textBoxCollection.ClearValues(DmlModes.Menghapus);
            EnablePanel(true);
        }

        private void btnTambah_Click(object sender, EventArgs e)
        {
            _dmlMode = DmlModes.Memasukan;
            _textBoxCollection.ClearValues(DmlModes.Memasukan);
            EnablePanel(false);
            var data = (from pasok in _dataContext.Suppliers
                        orderby pasok.idSupplier descending
                        select pasok.idSupplier).Take(1).ToList();
            txtKodeSupplier.Text = data.ToCode();
            txtKodeSupplier.Enabled = false;
            //txtTanggalPendaftaran.Text = DateTime.Now.ToShortDateString();
        }

        private void btnUbah_Click(object sender, EventArgs e)
        {
            if (_isDataGridClicked)
            {
                _dmlMode = DmlModes.Mengubah;
                EnablePanel(false);
                _isDataGridClicked = false;
            }
            else
            {
                MessageBox.Show(@"Pilih dulu datanya", @"Pengubahan Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnHapus_Click(object sender, EventArgs e)
        {
            if (_isDataGridClicked)
            {
                _dmlMode = DmlModes.Menghapus;
                _isDataGridClicked = false;

                var data = (from q in _dataContext.Suppliers
                            where q.idSupplier== txtKodeSupplier.Text
                            select q).First();
                DialogResult intReturnValue = MessageBox.Show(@"Apa Anda yakin ingin menghapus data ini?", @"Penghapusan Data", MessageBoxButtons.YesNo,MessageBoxIcon.Exclamation);
                if (intReturnValue == DialogResult.Yes)
                {
                    _dataContext.Suppliers.DeleteOnSubmit(data);
                    _dataContext.SubmitChanges();
                    _textBoxCollection.ClearValues(DmlModes.Menghapus);
                    RefreshForm();
                    EnablePanel(true);
                    MessageBox.Show(@"Data berhasil di hapus", @"Penghapusan Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show(@"Pilih dulu datanya", @"Penghapusan Data", MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }

        private void txtNoTelp_KeyPress(object sender, KeyPressEventArgs e)
        {
            HandleContactFieldPress(e);
        }
        private void txtNoFax_KeyPress(object sender, KeyPressEventArgs e)
        {
            HandleContactFieldPress(e);
        }

        private void HandleContactFieldPress(KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                e.Handled = true;
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (!_textBoxCollection.IsEmpty)
            {
                int doManipulate = 1;
                if (!txtEmail.IsEmail())
                {
                    MessageBox.Show(@"Format Email adalah username@domain.com");
                    doManipulate = 0;
                }
                if (doManipulate == 1)
                {
                    Supplier sup = new Supplier();
                    if (_dmlMode != DmlModes.Menghapus)
                    {
                        switch (_dmlMode)
                        {
                            case DmlModes.Memasukan:
                                {
                                    sup.idSupplier = txtKodeSupplier.Text;
                                    
                                    sup.namaPerusahaan = txtNamaPerusahaan.Text;
                                    sup.pic = txtPIC.Text;
                                    sup.noTelepon = txtNoTelp.Text;
                                    sup.noFax = txtFax.Text;
                                    sup.email = txtEmail.Text;
                                    sup.kota = txtKota.Text;
                                    sup.alamat = txtAlamat.Text;
                                    _dataContext.Suppliers.InsertOnSubmit(sup);
                                }
                                break;
                            case DmlModes.Mengubah:
                                {
                                    var data = (from q in _dataContext.Suppliers
                                                where q.idSupplier == txtKodeSupplier.Text
                                                select q).First();
                                    data.namaPerusahaan = txtNamaPerusahaan.Text;
                                    data.pic = txtPIC.Text;
                                    data.noTelepon = txtNoTelp.Text;
                                    data.noFax = txtFax.Text;
                                    data.email = txtEmail.Text;
                                    data.kota = txtKota.Text;
                                    data.alamat = txtAlamat.Text;
                                }
                                break;
                        }
                        _dataContext.SubmitChanges();
                        _textBoxCollection.ClearValues(_dmlMode);
                    }
                    RefreshForm();
                    MessageBox.Show(@"Berhasil entry data");

                    _textBoxCollection.ClearValues(DmlModes.Menghapus);
                    EnablePanel(true);
                }
            }
        }
        
        private bool CekEmail()
        {
            if (txtEmail.Text.Trim() != "")
            {
                Match rex = Regex.Match(txtEmail.Text.Trim(' '), "^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,3})$", RegexOptions.IgnoreCase);
                if (rex.Success == false)
                {
                    txtEmail.Focus();
                    return false;
                }
                return true;
            }
            return true;
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            RefreshForm();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {

            if (txtKataKunci.Text.Equals(""))
            {
                MessageBox.Show(@"Isi kata kuncinya dulu", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (cmbSearch.SelectedItem.ToString().Equals("Kode Supplier"))
                {
                    try
                    {
                        var dataKodeProduk = (from q in _dataContext.Suppliers
                                              where q.idSupplier.Contains(txtKataKunci.Text)
                                              select q).ToList();
                        UpdateGridView(dataKodeProduk);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show(@"Data tidak ditemukan", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                else if (cmbSearch.SelectedItem.ToString().Equals("Nama Perusahaan"))
                {
                    try
                    {
                        var dataNamaProduk = (from q in _dataContext.Suppliers
                                              where q.namaPerusahaan.Contains(txtKataKunci.Text)
                                              select q).ToList();
                        UpdateGridView(dataNamaProduk);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show(@"Data tidak ditemukan", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }

            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            OnDataGridClicked(e.RowIndex);
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            OnDataGridClicked(e.RowIndex);
        }

    }
}
