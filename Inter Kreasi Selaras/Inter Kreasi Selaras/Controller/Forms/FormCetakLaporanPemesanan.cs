﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using PT_Union_Ceramics_Utama.Controller.Helpers;
using PT_Union_Ceramics_Utama.Controller.Report;

namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    public partial class FormCetakLaporanPemesanan : Form
    {

        public FormCetakLaporanPemesanan()
        {
            InitializeComponent();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var conn = DatabaseHandler.Instance.SqlConnection;
            //conn.Open();

            String taw = tanggalawal.Value.ToString();
            String tak = tanggalakhir.Value.ToString();
            //string tanggal = DateTime.Today.Date.ToShortDateString();
            String p = "Select spp.idOrder, p.idProduk, p.namaProduk,p.harga, dsp.jumlahDipesan, dsp.subtotal, spp.tanggalOrder, CONVERT(VARCHAR(11),'" + taw + "',6) AS 'tanggalawal', CONVERT(VARCHAR(11),'" + tak + "',6) AS 'tanggalakhir' From SuratPemesananProduk spp join DetailSuratPemesananProduk dsp on spp.idOrder=dsp.idOrder join produk p on p.idProduk=dsp.idProduk WHERE spp.tanggalOrder BETWEEN '" + taw + "'And '" + tak + "'";
            SqlDataAdapter data1 = new SqlDataAdapter(p, conn);

            DataSet1 ds = new DataSet1();
            data1.Fill(ds, "Penjualan");

            LaporanPenjualan lpj = new LaporanPenjualan();
            lpj.SetDataSource(ds);
            crystalReportViewer1.ReportSource = lpj;
            conn.Close();
        }
    }
}
