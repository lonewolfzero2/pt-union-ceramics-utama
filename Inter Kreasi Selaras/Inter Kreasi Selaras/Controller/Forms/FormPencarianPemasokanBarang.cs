﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    public partial class FormPencarianPemasokanBarang : GridForm
    {
        private string _kdSuratPermintaanPemasokans = "";
        private string _kdPemasok = "";
        private string _kdPegawai = "";
        private string _tanggalPermintaan = "";
        private readonly FormPenerimaanBahanBaku _myFormPenerimaanBarang;

        public FormPencarianPemasokanBarang(FormPenerimaanBahanBaku parent) : base(null)
        {
            InitializeComponent();
            _myFormPenerimaanBarang = parent;
        }

        public override void Initiate()
        {
            _defaultDataGridView = dataGridView1;
            base.Initiate();
        }

        protected override object _data
        {
            get
            {
                return from q in _dataContext.PurchaseOrders
                       select new {q.idPO, q.tanggalPO, q.idSupplier, q.idPegawai};
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtKataKunci.Text.Equals(""))
            {
                MessageBox.Show(@"Isi kata kuncinya dulu", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (cmbSearch.SelectedIndex == 0)
                {
                    try
                    {
                        var dataPembelian = (from q in _dataContext.PurchaseOrders
                                             where q.idPO.Contains(txtKataKunci.Text)
                                              select q).First();
                        UpdateGridView(dataPembelian);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show(@"Data tidak ditemukan", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                else if (cmbSearch.SelectedIndex == 1)
                {
                    try
                    {
                        var dataPembelian = (from q in _dataContext.PurchaseOrders
                                                  where q.idSupplier.Contains(txtKataKunci.Text)
                                                  select q).ToList();
                        UpdateGridView(dataPembelian);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show(@"Data tidak ditemukan", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            RefreshForm();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            _kdSuratPermintaanPemasokans = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            _tanggalPermintaan = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            _kdPemasok = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            _kdPegawai = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
        }

        private void btnPilih_Click(object sender, EventArgs e)
        {
            if (_kdSuratPermintaanPemasokans.Equals(""))
            {
                MessageBox.Show(@"Pilih dulu datanya", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                var namaPerusahaan = (from q in _dataContext.Suppliers
                                      where q.idSupplier == _kdPemasok
                                      select q.namaPerusahaan).First();
                var detailPermintaan = (from q in _dataContext.DetailPOs
                                        where q.idPO == _kdSuratPermintaanPemasokans
                                        join p in _dataContext.BahanBakus
                                        on q.idBahanBaku equals p.idBahanBaku
                                        select new { q.idBahanBaku, p.namaBahanBaku, q.jumlahDiminta}).ToList();

                _myFormPenerimaanBarang.txtKodeSuratPermintaan.Text = _kdSuratPermintaanPemasokans;
                _myFormPenerimaanBarang.txtKodePegawai.Text = _kdPegawai;
                _myFormPenerimaanBarang.txtKodePemasok.Text = _kdPemasok;
                _myFormPenerimaanBarang.txtNamaPerusahaan.Text = namaPerusahaan;
                _myFormPenerimaanBarang.txtTanggal.Text = _tanggalPermintaan;

                _myFormPenerimaanBarang.datagridviewPermintaan.DataSource = detailPermintaan;
                Dispose();
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            _kdSuratPermintaanPemasokans = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            _tanggalPermintaan = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            _kdPemasok = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            _kdPegawai = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
        }

    }
}
