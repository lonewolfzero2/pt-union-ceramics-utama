﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using PT_Union_Ceramics_Utama.Controller.Core;
using PT_Union_Ceramics_Utama.Controller.Helpers;
using PT_Union_Ceramics_Utama.Controller.Helpers.Enums;

namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    public partial class FormPermintaanBahanBaku :GridForm
    {
        private const int POSISI_CURRENT = 0;
        private int _clickedRow;
        //private FormPencarianPemasok _fpp;

        public FormPermintaanBahanBaku(Form parent) : base(parent)
        {
            InitializeComponent();
            SetKodePembelian();
            panelButtonExecute.Enabled = false;
            SaveMode(true);
            panelButtonExecute.Enabled = false;

            _defaultDataGridView = dataGridProduk;
            btnPrint.Visible = false;
        }


        private void SaveMode(bool flag)
        {
            btnSave.Enabled = flag;
            btnPrint.Enabled = !flag;
            btnReset.Enabled = !flag;
        }

        private void SetKodePembelian()
        { 
            var kodePembelian = (from q in _dataContext.PurchaseOrders
                                     orderby q.idPO descending
                                     select q.idPO).Take(1).ToList();
            txtKodePembelian.Text = kodePembelian.ToCode();
        }

        private void EraseAllFields()
        {
            txtKodePemasok.Text = "";
            txtNamaPIC.Text = "";
            txtNamaPerusahaan.Text = "";
            txtAlamat.Text = "";
            dataGridProduk.Rows.Clear();

            txtKodeBahanBaku.Text = "";
            txtNamaBahanBaku.Text = "";
            txtJumlah.Text = "";
            dataGridPermintaan.Rows.Clear();
        }

        private void btnSearchPemasok_Click(object sender, EventArgs e)
        {
            var formPencarianSupplier = new BaseFormPencarian(this, "Supplier.",DataContexts.DataContext.Suppliers);
            formPencarianSupplier.Initiate();
        }

        private void txtKodePemasok_TextChanged(object sender, EventArgs e)
        {
            refresh();
        }

        public void refresh()
        {
            var data = from a in _dataContext.BahanBakus
                       where a.idsupplier == txtKodePemasok.Text
                       select new { a.idBahanBaku, a.namaBahanBaku, a.Berat };
            dataGridProduk.DataSource=data;

        }

        private void txtJumlah_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }

        private void dataGridProduk_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            txtKodeBahanBaku.Text = dataGridProduk.Rows[e.RowIndex].Cells[0].Value.ToString();
            txtNamaBahanBaku.Text = dataGridProduk.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtJumlah.Text = "";
        }

        private void btnAddToCart_Click(object sender, EventArgs e)
        {
            int posisiUbah = 0;
            if (!txtKodeBahanBaku.Text.Equals("") && !txtKodePemasok.Text.Equals("")&& !txtJumlah.Text.Equals(""))
            {
                    object[] buffer = new object[3];
                    buffer[0] = txtKodeBahanBaku.Text;
                    buffer[1] = txtNamaBahanBaku.Text;
                    buffer[2] = txtJumlah.Text;
                    
                    bool ada = false;
                    for (int i = 0; i < dataGridPermintaan.Rows.Count; i++)
                    {
                        if (dataGridPermintaan.Rows[i].Cells[0].Value.ToString().Equals(buffer[0].ToString()))
                        {
                            ada = true;
                            posisiUbah = i;
                            break;
                        }

                    }
                    if (ada == false)
                    {
                        List<DataGridViewRow> rows = new List<DataGridViewRow>();
                        rows.Add(new DataGridViewRow());
                        rows[POSISI_CURRENT].CreateCells(dataGridPermintaan, buffer);
                        dataGridPermintaan.Rows.AddRange(rows.ToArray());
                    }
                    else
                    {
                        int jumlahCurrent = int.Parse(dataGridPermintaan.Rows[posisiUbah].Cells[2].Value.ToString());
                        int jumlahYangMauDimasukkan = jumlahCurrent + (int.Parse(txtJumlah.Text));

                        // ubah jumlah
                        dataGridPermintaan.Rows[posisiUbah].Cells[2].Value = jumlahCurrent + (int.Parse(txtJumlah.Text));

                    }
                    

                    if (dataGridPermintaan.Rows.Count > 0)
                    {
                        panelButtonExecute.Enabled = true;
                        btnSearchPemasok.Enabled = false;
                    }
                
            }
            else
            {
                MessageBox.Show(@"Lengkapi data produk dan pemasok", @"Penambahan Pesanan", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void dataGridPermintaan_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            _clickedRow = e.RowIndex;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {

            if (dataGridPermintaan.Rows.Count > 0)
            {

                DialogResult result = MessageBox.Show(@"Anda yakin ingin hapus pesanan ini?", @"Penghapusan Data", MessageBoxButtons.YesNo,MessageBoxIcon.Exclamation);
                if (result == DialogResult.Yes)
                {
                    dataGridPermintaan.Rows.RemoveAt(_clickedRow);
                    if (dataGridPermintaan.Rows.Count == 0)
                    {
                        panelButtonExecute.Enabled = false;
                        btnSearchPemasok.Enabled = true;
                    }
                    if (dataGridPermintaan.Rows.Count > 0)
                    {
                        btnSearchPemasok.Enabled = false;
                    }
                    txtKodeBahanBaku.Text = "";
                    txtNamaBahanBaku.Text = "";
                    txtJumlah.Text = "";
                }
            }
            else
            {
                MessageBox.Show(@"Pilih dulu datanya", @"Penghapusan Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (dataGridPermintaan.Rows.Count > 0)
            {
                panelButtonExecute.Enabled = true;
                btnSearchPemasok.Enabled = false;
            }
            if (dataGridPermintaan.Rows.Count == 0)
            {
                panelButtonExecute.Enabled = true;

            }
        }

        private void btnEmpty_Click(object sender, EventArgs e)
        {
            if (dataGridPermintaan.Rows.Count > 0)
            {
                DialogResult result = MessageBox.Show(@"Anda yakin ingin hapus semua pesanan?", @"Pengosongan List", MessageBoxButtons.YesNo,MessageBoxIcon.Exclamation);
                if (result == DialogResult.Yes)
                {
                    dataGridPermintaan.Rows.Clear();
                    panelButtonExecute.Enabled = false;
                    btnSearchPemasok.Enabled = true;
                    txtKodeBahanBaku.Text = "";
                    txtNamaBahanBaku.Text = "";
                    txtJumlah.Text = "";
                }
            }
            else
            {
                MessageBox.Show(@"Lengkapi data permintaan dulu", @"Pengosongan List",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            EraseAllFields();
            panelButtonExecute.Enabled = false;
            SaveMode(true);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            PurchaseOrder spp = new PurchaseOrder();
            spp.idPO = txtKodePembelian.Text;
            spp.tanggalPO = DateTime.Now;
            spp.idSupplier = txtKodePemasok.Text;
            // nanti diganti sama yang lagi login
            spp.idPegawai = ActiveUser.Instance.Id;
            _dataContext.PurchaseOrders.InsertOnSubmit(spp);
            _dataContext.SubmitChanges();

            for (int i = 0; i < dataGridPermintaan.Rows.Count; i++)
            {
                var dataKodeDSPP = (from q in _dataContext.DetailPOs
                                    orderby q.idDetailPO descending
                                    select q.idDetailPO).Take(1).ToList();

                var dspp = new DetailPO
                    {
                        idDetailPO = dataKodeDSPP.ToCode(),
                        idPO = txtKodePembelian.Text,
                        idBahanBaku = dataGridPermintaan.Rows[i].Cells[0].Value.ToString(),
                        jumlahDiminta = int.Parse(txtJumlah.Text),
                    };
                _dataContext.DetailPOs.InsertOnSubmit(dspp);
                _dataContext.SubmitChanges();
            }

            MessageBox.Show(@"Surat Permintaan sudah dikeluarkan", @"Penyimpanan Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
            SaveMode(false);
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            //FormCetakSrtPemesananBahanBaku fcsbb = new FormCetakSrtPemesananBahanBaku(txtKodePembelian.Text);
            //fcsbb.Show();
            //SaveMode(true);
            //Dispose();
        }

        private void dataGridProduk_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtKodeBahanBaku.Text = dataGridProduk.Rows[e.RowIndex].Cells[0].Value.ToString();
            txtNamaBahanBaku.Text = dataGridProduk.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtJumlah.Text = "";
        }


    }
}
