﻿namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    partial class FormBahanBaku
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panelBahanBaku = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtUkuran = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtKodeBahanBaku = new System.Windows.Forms.TextBox();
            this.txtNama = new System.Windows.Forms.TextBox();
            this.panelPencarian = new System.Windows.Forms.GroupBox();
            this.cmbSearch = new System.Windows.Forms.ComboBox();
            this.txtKataKunci = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panelButtonDML = new System.Windows.Forms.Panel();
            this.btnHapus = new System.Windows.Forms.Button();
            this.btnUbah = new System.Windows.Forms.Button();
            this.btnTambah = new System.Windows.Forms.Button();
            this.panelButtonExecute = new System.Windows.Forms.Panel();
            this.btnBatal = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this._bs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panelBahanBaku.SuspendLayout();
            this.panelPencarian.SuspendLayout();
            this.panelButtonDML.SuspendLayout();
            this.panelButtonExecute.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(11, 116);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(579, 150);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // panelBahanBaku
            // 
            this.panelBahanBaku.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panelBahanBaku.Controls.Add(this.label7);
            this.panelBahanBaku.Controls.Add(this.txtUkuran);
            this.panelBahanBaku.Controls.Add(this.label1);
            this.panelBahanBaku.Controls.Add(this.label5);
            this.panelBahanBaku.Controls.Add(this.label2);
            this.panelBahanBaku.Controls.Add(this.txtKodeBahanBaku);
            this.panelBahanBaku.Controls.Add(this.txtNama);
            this.panelBahanBaku.Location = new System.Drawing.Point(11, 277);
            this.panelBahanBaku.Name = "panelBahanBaku";
            this.panelBahanBaku.Size = new System.Drawing.Size(296, 142);
            this.panelBahanBaku.TabIndex = 125;
            this.panelBahanBaku.TabStop = false;
            this.panelBahanBaku.Text = "Detail Informasi";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(168, 77);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(22, 13);
            this.label7.TabIndex = 123;
            this.label7.Text = "KG";
            // 
            // txtUkuran
            // 
            this.txtUkuran.Location = new System.Drawing.Point(122, 74);
            this.txtUkuran.Name = "txtUkuran";
            this.txtUkuran.Size = new System.Drawing.Size(44, 20);
            this.txtUkuran.TabIndex = 122;
            this.txtUkuran.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtUkuran_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Kode Bahan Baku";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "Berat";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Nama";
            // 
            // txtKodeBahanBaku
            // 
            this.txtKodeBahanBaku.Location = new System.Drawing.Point(122, 23);
            this.txtKodeBahanBaku.Name = "txtKodeBahanBaku";
            this.txtKodeBahanBaku.ReadOnly = true;
            this.txtKodeBahanBaku.Size = new System.Drawing.Size(155, 20);
            this.txtKodeBahanBaku.TabIndex = 14;
            // 
            // txtNama
            // 
            this.txtNama.Location = new System.Drawing.Point(122, 49);
            this.txtNama.Name = "txtNama";
            this.txtNama.Size = new System.Drawing.Size(155, 20);
            this.txtNama.TabIndex = 15;
            // 
            // panelPencarian
            // 
            this.panelPencarian.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panelPencarian.Controls.Add(this.cmbSearch);
            this.panelPencarian.Controls.Add(this.txtKataKunci);
            this.panelPencarian.Controls.Add(this.btnSearch);
            this.panelPencarian.Controls.Add(this.btnRefresh);
            this.panelPencarian.Controls.Add(this.label14);
            this.panelPencarian.Controls.Add(this.label9);
            this.panelPencarian.Location = new System.Drawing.Point(319, 277);
            this.panelPencarian.Name = "panelPencarian";
            this.panelPencarian.Size = new System.Drawing.Size(271, 117);
            this.panelPencarian.TabIndex = 126;
            this.panelPencarian.TabStop = false;
            this.panelPencarian.Text = "Pencarian";
            // 
            // cmbSearch
            // 
            this.cmbSearch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSearch.FormattingEnabled = true;
            this.cmbSearch.Items.AddRange(new object[] {
            "Kode Bahan Baku",
            "Nama Bahan Baku"});
            this.cmbSearch.Location = new System.Drawing.Point(14, 49);
            this.cmbSearch.Name = "cmbSearch";
            this.cmbSearch.Size = new System.Drawing.Size(131, 21);
            this.cmbSearch.TabIndex = 161;
            // 
            // txtKataKunci
            // 
            this.txtKataKunci.Location = new System.Drawing.Point(158, 49);
            this.txtKataKunci.Name = "txtKataKunci";
            this.txtKataKunci.Size = new System.Drawing.Size(100, 20);
            this.txtKataKunci.TabIndex = 162;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(58, 88);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 160;
            this.btnSearch.Text = "Cari";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(139, 88);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnRefresh.TabIndex = 159;
            this.btnRefresh.Text = "Muat Ulang";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(155, 26);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 13);
            this.label14.TabIndex = 41;
            this.label14.Text = "Kata Kunci :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 24);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(97, 13);
            this.label9.TabIndex = 29;
            this.label9.Text = "Cari Berdasarkan : ";
            // 
            // panelButtonDML
            // 
            this.panelButtonDML.Controls.Add(this.btnHapus);
            this.panelButtonDML.Controls.Add(this.btnUbah);
            this.panelButtonDML.Controls.Add(this.btnTambah);
            this.panelButtonDML.Location = new System.Drawing.Point(27, 425);
            this.panelButtonDML.Name = "panelButtonDML";
            this.panelButtonDML.Size = new System.Drawing.Size(271, 30);
            this.panelButtonDML.TabIndex = 158;
            // 
            // btnHapus
            // 
            this.btnHapus.Location = new System.Drawing.Point(185, 3);
            this.btnHapus.Name = "btnHapus";
            this.btnHapus.Size = new System.Drawing.Size(75, 23);
            this.btnHapus.TabIndex = 122;
            this.btnHapus.Text = "Hapus";
            this.btnHapus.UseVisualStyleBackColor = true;
            this.btnHapus.Visible = false;
            this.btnHapus.Click += new System.EventHandler(this.btnHapus_Click);
            // 
            // btnUbah
            // 
            this.btnUbah.Location = new System.Drawing.Point(98, 3);
            this.btnUbah.Name = "btnUbah";
            this.btnUbah.Size = new System.Drawing.Size(75, 23);
            this.btnUbah.TabIndex = 123;
            this.btnUbah.Text = "Ubah";
            this.btnUbah.UseVisualStyleBackColor = true;
            this.btnUbah.Click += new System.EventHandler(this.btnUbah_Click);
            // 
            // btnTambah
            // 
            this.btnTambah.Location = new System.Drawing.Point(11, 3);
            this.btnTambah.Name = "btnTambah";
            this.btnTambah.Size = new System.Drawing.Size(75, 23);
            this.btnTambah.TabIndex = 121;
            this.btnTambah.Text = "Tambah";
            this.btnTambah.UseVisualStyleBackColor = true;
            this.btnTambah.Click += new System.EventHandler(this.btnTambah_Click);
            // 
            // panelButtonExecute
            // 
            this.panelButtonExecute.Controls.Add(this.btnBatal);
            this.panelButtonExecute.Controls.Add(this.btnSave);
            this.panelButtonExecute.Location = new System.Drawing.Point(202, 461);
            this.panelButtonExecute.Name = "panelButtonExecute";
            this.panelButtonExecute.Size = new System.Drawing.Size(197, 37);
            this.panelButtonExecute.TabIndex = 124;
            // 
            // btnBatal
            // 
            this.btnBatal.Image = global::PT_Union_Ceramics_Utama.Properties.Resources.undod;
            this.btnBatal.Location = new System.Drawing.Point(117, 3);
            this.btnBatal.Name = "btnBatal";
            this.btnBatal.Size = new System.Drawing.Size(69, 28);
            this.btnBatal.TabIndex = 155;
            this.btnBatal.Text = "Reset";
            this.btnBatal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBatal.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnBatal.UseVisualStyleBackColor = true;
            this.btnBatal.Click += new System.EventHandler(this.btnBatal_Click);
            // 
            // btnSave
            // 
            this.btnSave.Image = global::PT_Union_Ceramics_Utama.Properties.Resources.save;
            this.btnSave.Location = new System.Drawing.Point(21, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 28);
            this.btnSave.TabIndex = 156;
            this.btnSave.Text = "Simpan";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // FormBahanBaku
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(603, 498);
            this.Controls.Add(this.panelButtonExecute);
            this.Controls.Add(this.panelButtonDML);
            this.Controls.Add(this.panelPencarian);
            this.Controls.Add(this.panelBahanBaku);
            this.Controls.Add(this.dataGridView1);
            this.Name = "FormBahanBaku";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Master Bahan Baku";
            this.Controls.SetChildIndex(this.dataGridView1, 0);
            this.Controls.SetChildIndex(this.panelBahanBaku, 0);
            this.Controls.SetChildIndex(this.panelPencarian, 0);
            this.Controls.SetChildIndex(this.panelButtonDML, 0);
            this.Controls.SetChildIndex(this.panelButtonExecute, 0);
            ((System.ComponentModel.ISupportInitialize)(this._bs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panelBahanBaku.ResumeLayout(false);
            this.panelBahanBaku.PerformLayout();
            this.panelPencarian.ResumeLayout(false);
            this.panelPencarian.PerformLayout();
            this.panelButtonDML.ResumeLayout(false);
            this.panelButtonExecute.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox panelBahanBaku;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtUkuran;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtKodeBahanBaku;
        private System.Windows.Forms.TextBox txtNama;
        private System.Windows.Forms.GroupBox panelPencarian;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panelButtonDML;
        private System.Windows.Forms.Button btnHapus;
        private System.Windows.Forms.Button btnUbah;
        private System.Windows.Forms.Button btnTambah;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Panel panelButtonExecute;
        private System.Windows.Forms.Button btnBatal;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ComboBox cmbSearch;
        private System.Windows.Forms.TextBox txtKataKunci;
    }
}