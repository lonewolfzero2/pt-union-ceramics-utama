﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using PT_Union_Ceramics_Utama.Controller.Core;
using PT_Union_Ceramics_Utama.Controller.Helpers;

namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    public partial class FormPemesananProduk : BaseLogoForm
    {
        private const int POSISI_CURRENT = 0;

        private int _clickedRow;

        public FormPemesananProduk(Form parent) : base(parent)
        {
            InitializeComponent();
            SetKodePenjualan();
            panelButtonExecute.Enabled = false;
            txtKodePegawai.Text = ActiveUser.Instance.Id;
            SaveMode(true);
        }

        public override void Initiate()
        {
            _defaultDataGridView = dataGridView1;
            base.Initiate();
        }

        private void SetKodePenjualan()
        {
            List<string> dataKodePenjualan = (from q in _dataContext.SuratPemesananProduks
                                              orderby q.idOrder descending
                                              select q.idOrder).Take(1).ToList();
            txtKodePemesanan.Text = dataKodePenjualan.ToCode();
        }

        private void SaveMode(bool flag)
        {
            btnSave.Enabled = flag;
            btnPrint.Enabled = !flag;
            btnReset.Enabled = !flag;
        }

        private void EraseAllFields()
        {
            SetKodePenjualan();
            txtKodePelanggan.Text = "";
            txtNamaPelanggan.Text = "";
            txtAlamat.Text = "";
            txtTelepon.Text = "";

            txtKodeProduk.Text = "";
            txtNamaProduk.Text = "";
            txtHarga.Text = "";
            txtStok.Text = "";
            txtJumlah.Text = "";
            UpdateGridView(null);
        }

        private void btnSearchPelanggan_Click(object sender, EventArgs e)
        {
            var formPencarianPelanggan = new BaseFormPencarian(this, "Pelanggan.", DataContexts.DataContext.Agens);
            formPencarianPelanggan.Initiate();
        }

        private void btnSearchProduk_Click(object sender, EventArgs e)
        {
            var formPencarianProduk = new BaseFormPencarian(this, "Produk.", DataContexts.DataContext.Produks);
            formPencarianProduk.Initiate();
        }

        private void txtJumlah_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }

        private void btnAddToCart_Click(object sender, EventArgs e)
        {
            var posisiUbah = 0;
            if (!string.IsNullOrEmpty(txtKodeProduk.Text) && !string.IsNullOrEmpty(txtKodePelanggan.Text) && !string.IsNullOrEmpty(txtKodeProduk.Text) && !string.IsNullOrEmpty(txtJumlah.Text))
            {
                var isiTextBoxStok = int.Parse(txtStok.Text);
                var isiTextBoxJumlah = int.Parse(txtJumlah.Text);
                if (isiTextBoxJumlah > isiTextBoxStok || isiTextBoxJumlah < 1)
                {
                    MessageBox.Show(@"Jumlah pesanan tidak tersedia", @"Penambahan Pesanan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    var buffer = new object[5];
                    buffer[0] = txtKodeProduk.Text;
                    buffer[1] = txtNamaProduk.Text;
                    buffer[2] = txtHarga.Text;
                    buffer[3] = txtJumlah.Text;
                    buffer[4] = int.Parse(buffer[2].ToString())*int.Parse(buffer[3].ToString());

                    var ada = false;
                    for (var i = 0; i < dataGridView1.Rows.Count; i++)
                    {
                        if (dataGridView1.Rows[i].Cells[0].Value.ToString().Equals(buffer[0].ToString()))
                        {
                            ada = true;
                            posisiUbah = i;
                            break;
                        }
                    }
                    if (!ada)
                    {
                        var rows = new List<DataGridViewRow>();
                        rows.Add(new DataGridViewRow());
                        rows[POSISI_CURRENT].CreateCells(dataGridView1, buffer);
                        dataGridView1.Rows.AddRange(rows.ToArray());
                    }
                    else
                    {
                        var jumlahCurrent = int.Parse(dataGridView1.Rows[posisiUbah].Cells[3].Value.ToString());
                        var jumlahYangMauDimasukkan = jumlahCurrent + (int.Parse(txtJumlah.Text));
                        isiTextBoxStok = int.Parse(txtStok.Text);
                        if (jumlahYangMauDimasukkan > isiTextBoxStok)
                        {
                            MessageBox.Show(@"Jumlah pesanan tidak tersedia", @"Penambahan Pesanan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            // ubah jumlah
                            dataGridView1.Rows[posisiUbah].Cells[3].Value = jumlahCurrent + (int.Parse(txtJumlah.Text));

                            // ubah sub total
                            var subTotalNext = int.Parse(dataGridView1.Rows[posisiUbah].Cells[3].Value.ToString())*int.Parse(dataGridView1.Rows[POSISI_CURRENT].Cells[2].Value.ToString());
                            dataGridView1.Rows[posisiUbah].Cells[4].Value = subTotalNext;
                        }
                    }

                    if (dataGridView1.Rows.Count > 0)
                    {
                        panelButtonExecute.Enabled = true;
                    }
                }
            }
            else
            {
                MessageBox.Show(@"Lengkapi data produk dan pelanggan", @"Penambahan Pesanan", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            _clickedRow = e.RowIndex;
            txtJumlah.Text = "";
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count > 0)
            {
                DialogResult result = MessageBox.Show(@"Anda yakin ingin hapus pesanan ini?", @"Penghapusan Data", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (result == DialogResult.Yes)
                {
                    dataGridView1.Rows.RemoveAt(_clickedRow);
                    if (dataGridView1.Rows.Count == 0)
                    {
                        panelButtonExecute.Enabled = false;
                    }
                }
            }
            else
            {
                MessageBox.Show(@"Pilih dulu datanya", @"Penghapusan Data", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
            }
            if (dataGridView1.Rows.Count > 0)
            {
                panelButtonExecute.Enabled = true;
            }
        }

        private void btnEmpty_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count > 0)
            {
                DialogResult result = MessageBox.Show(@"Anda yakin ingin hapus semua pesanan?", @"Pengosongan List", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (result == DialogResult.Yes)
                {
                    dataGridView1.Rows.Clear();
                    panelButtonExecute.Enabled = false;
                }
            }
            else
            {
                MessageBox.Show(@"Lengkapi data pemesanan dulu", @"Pengosongan List", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            EraseAllFields();
            panelButtonExecute.Enabled = false;
            SaveMode(false);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var spp = new SuratPemesananProduk
                {
                    idOrder = txtKodePemesanan.Text, 
                    tanggalOrder = DateTime.Now, 
                    idAgen = txtKodePelanggan.Text, 
                    idPegawai = ActiveUser.Instance.Id
                };

            _dataContext.SuratPemesananProduks.InsertOnSubmit(spp);
            _dataContext.SubmitChanges();

            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                List<string> dataKodeDSPP = (from q in _dataContext.DetailSuratPemesananProduks
                                             orderby q.idDetailOrder descending
                                             select q.idDetailOrder).Take(1).ToList();
                var dspp = new DetailSuratPemesananProduk
                    {
                        idDetailOrder = dataKodeDSPP.ToCode(),
                        idOrder = txtKodePemesanan.Text,
                        idProduk = dataGridView1.Rows[i].Cells[0].Value.ToString(),
                        jumlahDipesan = int.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString()),
                        subtotal = int.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString())
                    };
                _dataContext.DetailSuratPemesananProduks.InsertOnSubmit(dspp);
                _dataContext.SubmitChanges();

                var prod = new Produk();
                Produk dataProduk = (from q in _dataContext.Produks
                                     where q.idProduk == dataGridView1.Rows[i].Cells[0].Value.ToString()
                                     select q).First();
                dataProduk.jumlah -= int.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                _dataContext.SubmitChanges();
            }

            MessageBox.Show(@"Pemesanan siap diproses", @"Pengosongan List", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //EraseAllFields();
            SaveMode(false);
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            FormCetakPemesananProduk f1 = new FormCetakPemesananProduk(txtKodePemesanan.Text);
            f1.Show();
            SaveMode(true);
            Dispose();
            EraseAllFields();
        }
    }
}