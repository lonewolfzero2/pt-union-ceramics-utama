﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using PT_Union_Ceramics_Utama.Controller.Core;
using PT_Union_Ceramics_Utama.Controller.Helpers;
using PT_Union_Ceramics_Utama.Controller.Helpers.Enums;

namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    public partial class FormPengeluaranBahanBakucs : GridForm
    {
        int _clickedRow = 0;
        public int posisiCurrent = 0;
        private int jumlahstock = 0;

        public FormPengeluaranBahanBakucs(Form parent): base(parent)
        {
            InitializeComponent();
            SetKodePenerimaan();
            _defaultDataGridView = dataGridBahanBaku;

            txtKodePegawai.Text = ActiveUser.Instance.Id;
            txtNamaPegawai.Text = ActiveUser.Instance.Name;
            txtTanggal.Text = DateTime.Now.ToString();
        }
        public override void Initiate()
        {
            _defaultDataGridView = dataGridBahanBaku;
            base.Initiate();
        }

        protected override object _data
        {
            get
            {
                return from a in _dataContext.BahanBakus
                       select new { a.idBahanBaku, a.namaBahanBaku, a.Berat };
            }
        }

        private void SaveMode(bool flag)
        {
            btnSave.Enabled = flag;
            btnReset.Enabled = !flag;
        }

        private void SetKodePenerimaan()
        {
            var dataKodePenerimaan = (from q in _dataContext.SuratPengeluaranBahanBakus
                                      orderby q.idSuratPengeluaranBahanBaku descending
                                      select q.idSuratPengeluaranBahanBaku).Take(1).ToList();
            txtKodePengeluaranBB.Text = dataKodePenerimaan.ToCode();
        }

        private void EraseAllFields()
        {

            txtKodePegawai.Text = "";
            txtNamaPegawai.Text = "";
            txtTanggal.Text = "";
            UpdateGridView(null, dataGridBahanBaku);

            txtKodePengeluaranBB.Text = "";
            txtKodeBahanBaku.Text = "";
            txtNamaBahanBaku.Text = "";
            txtJumlah.Text = "";
            UpdateGridView(null, dataGridPengeluaran);
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            EraseAllFields();
            panelButtonExecute.Enabled = false;
            SaveMode(true);
        }

        private void dataGridBahanBaku_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            txtKodeBahanBaku.Text = dataGridBahanBaku.Rows[e.RowIndex].Cells[0].Value.ToString();
            txtNamaBahanBaku.Text = dataGridBahanBaku.Rows[e.RowIndex].Cells[1].Value.ToString();
            jumlahstock=int.Parse(dataGridBahanBaku.Rows[e.RowIndex].Cells[2].Value.ToString());

            txtJumlah.Text = "";
            _clickedRow = e.RowIndex;
        }

        private void btnAddToCart_Click(object sender, EventArgs e)
        {
            if (txtKodeBahanBaku.Text.Equals("") || txtKodePengeluaranBB.Text.Equals("") || txtJumlah.Text.Equals(""))
            {
                MessageBox.Show(@"Lengkapi data Bahan Baku", @"Penambahan Penerimaan", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                int posisiUbah = 0;
                int jumlahTerima = int.Parse(txtJumlah.Text);
                if (jumlahTerima == 0)
                {
                    MessageBox.Show(@"Jumlah yang Diterima tidak boleh 0", @"Penambahan Penerimaan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    object[] buffer = new object[3];
                    buffer[0] = txtKodeBahanBaku.Text;
                    buffer[1] = txtNamaBahanBaku.Text;
                    buffer[2] = txtJumlah.Text;

                    bool ada = false;
                    for (int i = 0; i < dataGridPengeluaran.Rows.Count; i++)
                    {
                        if (dataGridPengeluaran.Rows[i].Cells[0].Value.ToString().Equals(buffer[0].ToString()))
                        {
                            ada = true;
                            posisiUbah = i;
                            break;
                        }

                    }

                    if (ada == false)
                    {
                        if (int.Parse(txtJumlah.Text) > jumlahstock)
                        {
                            MessageBox.Show("Jumlah yang anda minta tidak tersedia", @"Penambahan Jumlah", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            List<DataGridViewRow> rows = new List<DataGridViewRow>();
                            rows.Add(new DataGridViewRow());
                            rows[posisiCurrent].CreateCells(dataGridPengeluaran, buffer);
                            dataGridPengeluaran.Rows.AddRange(rows.ToArray());
                            btnAddToCart.Enabled = false;
                        }
                    }
                    else
                    {
                        int jumlahcurrent = int.Parse(dataGridPengeluaran.Rows[posisiUbah].Cells[2].Value.ToString());
                        int jumlahyangmaudimasukan=0;
                        if (int.Parse(txtJumlah.Text) > jumlahstock)
                        {
                            MessageBox.Show("Jumlah yang anda minta tidak tersedia", @"Penambahan Jumlah", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            jumlahyangmaudimasukan = jumlahcurrent + (int.Parse(txtJumlah.Text));
                        }

                        dataGridPengeluaran.Rows[posisiUbah].Cells[2].Value = jumlahyangmaudimasukan;
                    }
                }
            }
            if (dataGridBahanBaku.Rows.Count > 0)
            {
                panelButtonExecute.Enabled = true;
                btnAddToCart.Enabled = false;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dataGridPengeluaran.Rows.Count > 0)
            {
                DialogResult result = MessageBox.Show(@"Anda yakin ingin hapus pesanan ini?", @"Penghapusan Data", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (result == DialogResult.Yes)
                {
                    dataGridPengeluaran.Rows.RemoveAt(_clickedRow-1);
                    if (dataGridPengeluaran.Rows.Count == 0)
                    {
                        panelButtonExecute.Enabled = false;
                    }
                }
                if (dataGridBahanBaku.Rows.Count == 0)
                {
                    panelButtonExecute.Enabled = true;
                    btnAddToCart.Enabled = true;

                }
            }
        }

        private void btnEmpty_Click(object sender, EventArgs e)
        {
            if (dataGridPengeluaran.Rows.Count > 0)
            {
                DialogResult result = MessageBox.Show(@"Anda yakin ingin hapus semua pesanan?", @"Pengosongan List", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (result == DialogResult.Yes)
                {
                    dataGridPengeluaran.Rows.Clear();
                    panelButtonExecute.Enabled = false;
                }
            }
            else
            {
                MessageBox.Show(@"Lengkapi data pemesanan dulu", @"Pengosongan List", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SuratPengeluaranBahanBaku spp = new SuratPengeluaranBahanBaku();
                spp.idSuratPengeluaranBahanBaku = txtKodePengeluaranBB.Text;
                spp.idPegawai = ActiveUser.Instance.Id;
                spp.tanggalkeluar = DateTime.Now;
                // nanti diganti sama karyawan yg lagi login
                _dataContext.SuratPengeluaranBahanBakus.InsertOnSubmit(spp);

                _dataContext.SubmitChanges();

                for (int i = 0; i < dataGridPengeluaran.Rows.Count; i++)
                {
                    var dataKodeDSPP = (from q in _dataContext.DetailSuratPengeluaranBahanBakus
                                        orderby q.idDetailSuratPengeluaranBahanBaku descending
                                        select q.idDetailSuratPengeluaranBahanBaku).Take(1).ToList();
                    var dspp = new DetailSuratPengeluaranBahanBaku
                    {
                        idDetailSuratPengeluaranBahanBaku = dataKodeDSPP.ToCode(),
                        idSuratPengeluaranBahanBaku = txtKodePengeluaranBB.Text,
                        idBahanBaku = dataGridPengeluaran.Rows[i].Cells[0].Value.ToString(),
                        jumlahKeluar = int.Parse(dataGridPengeluaran.Rows[i].Cells[2].Value.ToString())
                    };

                    _dataContext.DetailSuratPengeluaranBahanBakus.InsertOnSubmit(dspp);
                    _dataContext.SubmitChanges();

                    BahanBaku bb = new BahanBaku();
                    var databb = (from q in _dataContext.BahanBakus
                                      where q.idBahanBaku == dataGridPengeluaran.Rows[i].Cells[0].Value.ToString()
                                      select q).First();
                    databb.Berat -= int.Parse(dataGridPengeluaran.Rows[i].Cells[2].Value.ToString());
                    _dataContext.SubmitChanges();
                }

                MessageBox.Show(@"Penerimaan berhasil diproses", @"Penyimpanan Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //saveMode(false);
                EraseAllFields();
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void dataGridPengeluaran_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            _clickedRow = e.RowIndex;
        }
    }
}
