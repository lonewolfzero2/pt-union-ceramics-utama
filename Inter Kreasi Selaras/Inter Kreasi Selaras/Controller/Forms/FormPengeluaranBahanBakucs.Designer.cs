﻿namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    partial class FormPengeluaranBahanBakucs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelButtonExecute = new System.Windows.Forms.Panel();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtKodePengeluaranBB = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnEmpty = new System.Windows.Forms.Button();
            this.btnAddToCart = new System.Windows.Forms.Button();
            this.txtKodeBahanBaku = new System.Windows.Forms.TextBox();
            this.txtNamaBahanBaku = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtJumlah = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtKodePegawai = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.dataGridBahanBaku = new System.Windows.Forms.DataGridView();
            this.txtTanggal = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtNamaPegawai = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridPengeluaran = new System.Windows.Forms.DataGridView();
            this.kdBahanBaku = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nama = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jumlah = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnReset = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this._bs)).BeginInit();
            this.panelButtonExecute.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridBahanBaku)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPengeluaran)).BeginInit();
            this.SuspendLayout();
            // 
            // panelButtonExecute
            // 
            this.panelButtonExecute.Controls.Add(this.btnPrint);
            this.panelButtonExecute.Controls.Add(this.btnSave);
            this.panelButtonExecute.Location = new System.Drawing.Point(310, 539);
            this.panelButtonExecute.Name = "panelButtonExecute";
            this.panelButtonExecute.Size = new System.Drawing.Size(176, 37);
            this.panelButtonExecute.TabIndex = 162;
            // 
            // btnPrint
            // 
            this.btnPrint.Image = global::PT_Union_Ceramics_Utama.Properties.Resources.agt_print;
            this.btnPrint.Location = new System.Drawing.Point(3, 6);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 28);
            this.btnPrint.TabIndex = 155;
            this.btnPrint.Text = "Cetak";
            this.btnPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Visible = false;
            // 
            // btnSave
            // 
            this.btnSave.Image = global::PT_Union_Ceramics_Utama.Properties.Resources.save;
            this.btnSave.Location = new System.Drawing.Point(94, 6);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 28);
            this.btnSave.TabIndex = 154;
            this.btnSave.Text = "Simpan";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.groupBox3.Controls.Add(this.txtKodePengeluaranBB);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.btnDelete);
            this.groupBox3.Controls.Add(this.btnEmpty);
            this.groupBox3.Controls.Add(this.btnAddToCart);
            this.groupBox3.Controls.Add(this.txtKodeBahanBaku);
            this.groupBox3.Controls.Add(this.txtNamaBahanBaku);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.txtJumlah);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Location = new System.Drawing.Point(9, 329);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(345, 204);
            this.groupBox3.TabIndex = 160;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Informasi Pengeluaran Bahan Baku";
            // 
            // txtKodePengeluaranBB
            // 
            this.txtKodePengeluaranBB.Enabled = false;
            this.txtKodePengeluaranBB.Location = new System.Drawing.Point(179, 41);
            this.txtKodePengeluaranBB.Name = "txtKodePengeluaranBB";
            this.txtKodePengeluaranBB.ReadOnly = true;
            this.txtKodePengeluaranBB.Size = new System.Drawing.Size(153, 20);
            this.txtKodePengeluaranBB.TabIndex = 160;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(157, 13);
            this.label1.TabIndex = 159;
            this.label1.Text = "Kode Pengeluaran Bahan Baku";
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(131, 155);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(60, 32);
            this.btnDelete.TabIndex = 158;
            this.btnDelete.Text = "Hapus";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEmpty
            // 
            this.btnEmpty.Location = new System.Drawing.Point(197, 155);
            this.btnEmpty.Name = "btnEmpty";
            this.btnEmpty.Size = new System.Drawing.Size(90, 32);
            this.btnEmpty.TabIndex = 157;
            this.btnEmpty.Text = "Kosongkan List";
            this.btnEmpty.UseVisualStyleBackColor = true;
            this.btnEmpty.Click += new System.EventHandler(this.btnEmpty_Click);
            // 
            // btnAddToCart
            // 
            this.btnAddToCart.Location = new System.Drawing.Point(14, 155);
            this.btnAddToCart.Name = "btnAddToCart";
            this.btnAddToCart.Size = new System.Drawing.Size(111, 32);
            this.btnAddToCart.TabIndex = 156;
            this.btnAddToCart.Text = "Tambahkan ke List";
            this.btnAddToCart.UseVisualStyleBackColor = true;
            this.btnAddToCart.Click += new System.EventHandler(this.btnAddToCart_Click);
            // 
            // txtKodeBahanBaku
            // 
            this.txtKodeBahanBaku.Enabled = false;
            this.txtKodeBahanBaku.Location = new System.Drawing.Point(179, 67);
            this.txtKodeBahanBaku.Name = "txtKodeBahanBaku";
            this.txtKodeBahanBaku.ReadOnly = true;
            this.txtKodeBahanBaku.Size = new System.Drawing.Size(153, 20);
            this.txtKodeBahanBaku.TabIndex = 71;
            // 
            // txtNamaBahanBaku
            // 
            this.txtNamaBahanBaku.Enabled = false;
            this.txtNamaBahanBaku.Location = new System.Drawing.Point(179, 93);
            this.txtNamaBahanBaku.Name = "txtNamaBahanBaku";
            this.txtNamaBahanBaku.ReadOnly = true;
            this.txtNamaBahanBaku.Size = new System.Drawing.Size(153, 20);
            this.txtNamaBahanBaku.TabIndex = 67;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 13);
            this.label5.TabIndex = 51;
            this.label5.Text = "Kode Bahan Baku";
            // 
            // txtJumlah
            // 
            this.txtJumlah.Location = new System.Drawing.Point(179, 119);
            this.txtJumlah.Name = "txtJumlah";
            this.txtJumlah.Size = new System.Drawing.Size(45, 20);
            this.txtJumlah.TabIndex = 26;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 122);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 13);
            this.label7.TabIndex = 52;
            this.label7.Text = "Jumlah Permintaan";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 13);
            this.label3.TabIndex = 52;
            this.label3.Text = "Nama Bahan Baku";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.groupBox2.Controls.Add(this.txtKodePegawai);
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.txtTanggal);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.txtNamaPegawai);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(15, 127);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(714, 196);
            this.groupBox2.TabIndex = 159;
            this.groupBox2.TabStop = false;
            // 
            // txtKodePegawai
            // 
            this.txtKodePegawai.Location = new System.Drawing.Point(122, 70);
            this.txtKodePegawai.Name = "txtKodePegawai";
            this.txtKodePegawai.ReadOnly = true;
            this.txtKodePegawai.Size = new System.Drawing.Size(153, 20);
            this.txtKodePegawai.TabIndex = 121;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.SystemColors.ControlLight;
            this.groupBox4.Controls.Add(this.dataGridBahanBaku);
            this.groupBox4.Location = new System.Drawing.Point(295, 24);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(405, 159);
            this.groupBox4.TabIndex = 120;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Bahan Baku";
            // 
            // dataGridBahanBaku
            // 
            this.dataGridBahanBaku.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridBahanBaku.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridBahanBaku.Location = new System.Drawing.Point(13, 19);
            this.dataGridBahanBaku.Name = "dataGridBahanBaku";
            this.dataGridBahanBaku.RowTemplate.Height = 24;
            this.dataGridBahanBaku.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridBahanBaku.Size = new System.Drawing.Size(379, 123);
            this.dataGridBahanBaku.TabIndex = 81;
            this.dataGridBahanBaku.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridBahanBaku_CellContentClick);
            // 
            // txtTanggal
            // 
            this.txtTanggal.Enabled = false;
            this.txtTanggal.Location = new System.Drawing.Point(122, 122);
            this.txtTanggal.Name = "txtTanggal";
            this.txtTanggal.ReadOnly = true;
            this.txtTanggal.Size = new System.Drawing.Size(153, 20);
            this.txtTanggal.TabIndex = 79;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 125);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(46, 13);
            this.label10.TabIndex = 80;
            this.label10.Text = "Tanggal";
            // 
            // txtNamaPegawai
            // 
            this.txtNamaPegawai.Enabled = false;
            this.txtNamaPegawai.Location = new System.Drawing.Point(122, 96);
            this.txtNamaPegawai.Name = "txtNamaPegawai";
            this.txtNamaPegawai.ReadOnly = true;
            this.txtNamaPegawai.Size = new System.Drawing.Size(153, 20);
            this.txtNamaPegawai.TabIndex = 71;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 99);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 13);
            this.label6.TabIndex = 73;
            this.label6.Text = "Nama Pegawai";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 72;
            this.label2.Text = "Kode Pegawai";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.groupBox1.Controls.Add(this.dataGridPengeluaran);
            this.groupBox1.Location = new System.Drawing.Point(360, 329);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(369, 204);
            this.groupBox1.TabIndex = 158;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "List Pengeluaran Bahan Baku";
            // 
            // dataGridPengeluaran
            // 
            this.dataGridPengeluaran.AllowUserToAddRows = false;
            this.dataGridPengeluaran.AllowUserToDeleteRows = false;
            this.dataGridPengeluaran.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridPengeluaran.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.kdBahanBaku,
            this.nama,
            this.jumlah});
            this.dataGridPengeluaran.Location = new System.Drawing.Point(12, 22);
            this.dataGridPengeluaran.Name = "dataGridPengeluaran";
            this.dataGridPengeluaran.ReadOnly = true;
            this.dataGridPengeluaran.RowTemplate.Height = 24;
            this.dataGridPengeluaran.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridPengeluaran.Size = new System.Drawing.Size(345, 165);
            this.dataGridPengeluaran.TabIndex = 111;
            this.dataGridPengeluaran.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridPengeluaran_CellContentClick);
            // 
            // kdBahanBaku
            // 
            this.kdBahanBaku.HeaderText = "Kode Bahan Baku";
            this.kdBahanBaku.Name = "kdBahanBaku";
            this.kdBahanBaku.ReadOnly = true;
            // 
            // nama
            // 
            this.nama.HeaderText = "Nama Bahan Baku";
            this.nama.Name = "nama";
            this.nama.ReadOnly = true;
            // 
            // jumlah
            // 
            this.jumlah.HeaderText = "Jumlah Permintaan";
            this.jumlah.Name = "jumlah";
            this.jumlah.ReadOnly = true;
            // 
            // btnReset
            // 
            this.btnReset.Image = global::PT_Union_Ceramics_Utama.Properties.Resources.undod;
            this.btnReset.Location = new System.Drawing.Point(660, 545);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(69, 28);
            this.btnReset.TabIndex = 161;
            this.btnReset.Text = "Reset";
            this.btnReset.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReset.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Visible = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // FormPengeluaranBahanBakucs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(741, 580);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.panelButtonExecute);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "FormPengeluaranBahanBakucs";
            this.Text = "FormPengeluaranBahanBakucs";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.groupBox2, 0);
            this.Controls.SetChildIndex(this.groupBox3, 0);
            this.Controls.SetChildIndex(this.panelButtonExecute, 0);
            this.Controls.SetChildIndex(this.btnReset, 0);
            ((System.ComponentModel.ISupportInitialize)(this._bs)).EndInit();
            this.panelButtonExecute.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridBahanBaku)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPengeluaran)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Panel panelButtonExecute;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnEmpty;
        private System.Windows.Forms.Button btnAddToCart;
        private System.Windows.Forms.TextBox txtKodeBahanBaku;
        private System.Windows.Forms.TextBox txtNamaBahanBaku;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtJumlah;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.TextBox txtKodePegawai;
        private System.Windows.Forms.GroupBox groupBox4;
        public System.Windows.Forms.DataGridView dataGridBahanBaku;
        public System.Windows.Forms.TextBox txtTanggal;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.TextBox txtNamaPegawai;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridPengeluaran;
        private System.Windows.Forms.DataGridViewTextBoxColumn kdBahanBaku;
        private System.Windows.Forms.DataGridViewTextBoxColumn nama;
        private System.Windows.Forms.DataGridViewTextBoxColumn jumlah;
        private System.Windows.Forms.TextBox txtKodePengeluaranBB;
        private System.Windows.Forms.Label label1;
    }
}