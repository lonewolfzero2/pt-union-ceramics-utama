﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using PT_Union_Ceramics_Utama.Controller.Helpers;
using PT_Union_Ceramics_Utama.Controller.Report;

namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    public partial class FormCetakLaporanGudang : Form
    {
        private int _kode;
        private String taw;
        private String tak;

        public FormCetakLaporanGudang(Form parent,int kode)
        {
            InitializeComponent();
            _kode = kode;
            taw = tanggalawal.Value.ToString();
            tak = tanggalakhir.Value.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (_kode == 1)
            {
                //pengeluaran bahan baku
                var conn = DatabaseHandler.Instance.SqlConnection;
                //conn.Open();
                taw = tanggalawal.Value.ToString();
                tak = tanggalakhir.Value.ToString();
                //string tanggal = DateTime.Today.Date.ToShortDateString();
                String p = "SELECT  dspbb.idBahanBaku AS 'idbahanbaku', bb.namaBahanBaku AS 'namabahanbaku', sum(dspbb.jumlahKeluar) AS 'jumlahkeluar', CONVERT(VARCHAR(11),'" + taw + "',6) AS 'tanggalawal', CONVERT(VARCHAR(11),'" + tak + "',6) AS 'tanggalakhir' FROM SuratPengeluaranBahanBaku spbb join DetailSuratPengeluaranBahanBaku dspbb on spbb.idSuratPengeluaranBahanBaku=dspbb.idSuratPengeluaranBahanBaku join BahanBaku bb ON dspbb.idBahanBaku = bb.idBahanBaku WHERE spbb.tanggalkeluar BETWEEN '" + taw + "' AND '" + tak + "' group by dspbb.idBahanBaku, bb.namaBahanBaku";
                SqlDataAdapter data1 = new SqlDataAdapter(p, conn);

                DataSet1 ds = new DataSet1();
                data1.Fill(ds, "PengeluaranBahanBaku");

                LaporanPengeluaranBahanBaku spbb = new LaporanPengeluaranBahanBaku();
                spbb.SetDataSource(ds);
                crystalReportViewer1.ReportSource = spbb;
                conn.Close();
            }
            else if (_kode == 2)
            {
                //penerimaan bahan baku
                var conn = DatabaseHandler.Instance.SqlConnection;
                //conn.Open();
                taw = tanggalawal.Value.ToString();
                tak = tanggalakhir.Value.ToString();
                //string tanggal = DateTime.Today.Date.ToShortDateString();
                String p = "select dp.idBahanBaku AS 'idbahanbaku', bb.namaBahanBaku AS 'namabahanbaku', sum(dp.jumlahDiminta) AS 'jumlah',CONVERT(VARCHAR(11),'" + taw + "',6) AS 'tanggalawal', CONVERT(VARCHAR(11),'" + tak + "',6) AS 'tanggalakhir' from PurchaseOrder p join DetailPO dp on p.idPO=dp.idPO join BahanBaku bb on bb.idBahanBaku=dp.idBahanBaku group by dp.idBahanBaku, bb.namaBahanBaku";

                SqlDataAdapter data1 = new SqlDataAdapter(p, conn);

                DataSet1 ds = new DataSet1();
                data1.Fill(ds, "PenerimaanBahanBaku");

                CopyofSuratPemesananBB spbb = new CopyofSuratPemesananBB();
                spbb.SetDataSource(ds);
                crystalReportViewer1.ReportSource = spbb;
                conn.Close();
            }
            else if (_kode == 3)
            {
                //penerimaanproduk
                var conn = DatabaseHandler.Instance.SqlConnection;
                //conn.Open();

                taw = tanggalawal.Value.ToString();
                tak = tanggalakhir.Value.ToString();
                //string tanggal = DateTime.Today.Date.ToShortDateString();
                String p = "SELECT prd.idProduk AS 'idproduk', prd.namaProduk AS 'namaproduk', sum(dspp.jumlahDiterima) AS 'jumlahditerima', CONVERT(VARCHAR(11),'" + taw + "',6) AS 'tanggalawal', CONVERT(VARCHAR(11),'" + tak + "',6) AS 'tanggalakhir' FROM SuratPenerimaanProduk spp JOIN DetailSuratPenerimaanProduk dspp ON spp.idSuratPenerimaanProduk=dspp.idSuratPenerimaanProduk JOIN Produk prd ON dspp.idProduk = prd.idProduk JOIN Pegawai p ON spp.idPegawai = p.idPegawai WHERE spp.tanggalPenerimaanProduk BETWEEN '" + taw + "' AND '" + tak + "' group by prd.idProduk,prd.namaProduk";
                SqlDataAdapter data1 = new SqlDataAdapter(p, conn);

                DataSet1 ds = new DataSet1();
                data1.Fill(ds, "PenerimaanProduk");

                LaporanPenerimaanProduk spbb = new LaporanPenerimaanProduk();
                spbb.SetDataSource(ds);
                crystalReportViewer1.ReportSource = spbb;
                conn.Close();

            }
            else
            {
                //Pengeluaran Produk
                var conn = DatabaseHandler.Instance.SqlConnection;
                //conn.Open();

                taw = tanggalawal.Value.ToString();
                tak = tanggalakhir.Value.ToString();
                //string tanggal = DateTime.Today.Date.ToShortDateString();
                String p = "Select p.idProduk AS 'idproduk', p.namaProduk AS 'namaproduk', sum(dsp.jumlahDipesan) AS 'jumlah', CONVERT(VARCHAR(11),'" + taw + "',6) AS 'tanggalawal', CONVERT(VARCHAR(11),'" + tak + "',6) AS 'tanggalakhir' From SuratPemesananProduk spp join DetailSuratPemesananProduk dsp on spp.idOrder=dsp.idOrder join produk p on p.idProduk=dsp.idProduk WHERE spp.tanggalOrder BETWEEN '" + taw + "'And '" + tak + "' group by p.idProduk, p.namaProduk";
                SqlDataAdapter data1 = new SqlDataAdapter(p, conn);

                DataSet1 ds = new DataSet1();
                data1.Fill(ds, "Penjualan");

                Copy_of_LaporanPenjualan lpj = new Copy_of_LaporanPenjualan();
                lpj.SetDataSource(ds);
                crystalReportViewer1.ReportSource = lpj;
                conn.Close();
            }
        }

    }
}
