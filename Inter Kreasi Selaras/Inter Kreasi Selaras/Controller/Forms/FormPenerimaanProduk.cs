﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using PT_Union_Ceramics_Utama.Controller.Core;
using PT_Union_Ceramics_Utama.Controller.Helpers;


namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    public partial class FormPenerimaanProduk : BaseLogoForm
    {
        int _clickedRow = 0;
        public int posisiCurrent = 0;
        FormPencarianPengeluaranBahanBaku _fppbb;

        public FormPenerimaanProduk(Form parent) : base(parent)
        {
            InitializeComponent();
            SetKodePenerimaan();
        }

        private void SaveMode(bool flag)
        {
            btnSave.Enabled = flag;
            btnReset.Enabled = !flag;
        }

        private void SetKodePenerimaan()
        {
            var dataKodePenerimaan = (from q in _dataContext.SuratPenerimaanProduks
                                      orderby q.idSuratPenerimaanProduk descending
                                      select q.idSuratPenerimaanProduk).Take(1).ToList();
            txtKodePenerimaanProduk.Text = dataKodePenerimaan.ToCode();
        }

        private void EraseAllFields()
        {
            txtKodeSrtPengeluaranBahanBaku.Text = "";
            txtKodePegawai.Text = "";
            txtNamaBB.Text = "";
            txtKodePegawai.Text = "";
            txtJumlahKeluar.Text = "";
            txtTanggalKeluar.Text = "";
            UpdateGridView(null, dataGridProduk);

            txtKodeProduk.Text = "";
            txtNamaProduk.Text = "";
            txtHarga.Text = "";
            txtJumlahDiterima.Text = "";
            UpdateGridView(null, dataGridPermintaan);
        }

        private void btnSearchPemasok_Click(object sender, EventArgs e)
        {
            if (_fppbb == null || _fppbb.IsDisposed)
            {
                _fppbb = new FormPencarianPengeluaranBahanBaku(this);
                _fppbb.Initiate();
            }
        }

        private void txtJumlahDiterima_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                e.Handled = true;
        }

        private void btnAddToCart_Click_1(object sender, EventArgs e)
        {
            if (txtKodeProduk.Text.Equals("") || txtKodePenerimaanProduk.Text.Equals("") || txtJumlahDiterima.Text.Equals(""))
            {
                MessageBox.Show(@"Lengkapi data Produk dan Bahan Baku", @"Penambahan Penerimaan", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                int posisiUbah = 0;
                int jumlahTerima = int.Parse(txtJumlahDiterima.Text);
                if (jumlahTerima == 0 )
                {
                    MessageBox.Show(@"Jumlah yang Diterima tidak boleh 0", @"Penambahan Penerimaan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    object[] buffer = new object[5];
                    buffer[0] = txtKodeProduk.Text;
                    buffer[1] = txtNamaProduk.Text;
                    buffer[2] = txtHarga.Text;
                    buffer[3] = txtJumlahDiterima.Text;

                    bool ada = false;
                    for (int i = 0; i < dataGridPermintaan.Rows.Count; i++)
                    {
                        if (dataGridPermintaan.Rows[i].Cells[0].Value.ToString().Equals(buffer[0].ToString()))
                        {
                            ada = true;
                            posisiUbah = i;
                            break;
                        }

                    }

                    if (ada == false)
                    {
                        List<DataGridViewRow> rows = new List<DataGridViewRow>();
                        rows.Add(new DataGridViewRow());
                        rows[posisiCurrent].CreateCells(dataGridPermintaan, buffer);
                        dataGridPermintaan.Rows.AddRange(rows.ToArray());
                    }
                    else
                    {
                        int jumlahcurrent = int.Parse(dataGridPermintaan.Rows[posisiUbah].Cells[3].Value.ToString());
                        int jumlahyangmaudimasukkan = jumlahcurrent + (int.Parse(txtJumlahDiterima.Text));
                        
                        dataGridPermintaan.Rows[posisiUbah].Cells[3].Value = jumlahyangmaudimasukkan;
                        }
                    }
                }
                if (dataGridPermintaan.Rows.Count > 0)
                {
                    panelButtonExecute.Enabled = true;
                }
        }

        private void dataGridProduk_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            txtKodeProduk.Text = dataGridProduk.Rows[e.RowIndex].Cells[0].Value.ToString();
            txtNamaProduk.Text = dataGridProduk.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtHarga.Text = dataGridProduk.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtJumlahDiterima.Text = "";
            _clickedRow = e.RowIndex;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show(@"Anda yakin ingin hapus pesanan ini?", @"Penghapusan Pesanan", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
            if (result == DialogResult.Yes)
            {
                dataGridPermintaan.Rows.RemoveAt(_clickedRow);
                if (dataGridPermintaan.Rows.Count == 0)
                {
                    panelButtonExecute.Enabled = false;
                }
            }
        }

        private void btnEmpty_Click(object sender, EventArgs e)
        {
            if (dataGridPermintaan.Rows.Count > 0)
            {
                DialogResult result = MessageBox.Show(@"Anda yakin ingin hapus semua pesanan?", @"Pengosongan List", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (result == DialogResult.Yes)
                {
                    dataGridPermintaan.Rows.Clear();
                    panelButtonExecute.Enabled = false;
                }
            }
            else
            {
                MessageBox.Show(@"Lengkapi data pemesanan dulu", @"Pengosongan List", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            EraseAllFields();
            panelButtonExecute.Enabled = false;
            SaveMode(true);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SuratPenerimaanProduk spp = new SuratPenerimaanProduk();
                spp.idSuratPenerimaanProduk = txtKodePenerimaanProduk.Text;
                spp.idpegawai = ActiveUser.Instance.Id;
                spp.tanggalPenerimaanProduk = DateTime.Now;
                // nanti diganti sama karyawan yg lagi login
                _dataContext.SuratPenerimaanProduks.InsertOnSubmit(spp);

                _dataContext.SubmitChanges();

                for (int i = 0; i < dataGridPermintaan.Rows.Count; i++)
                {
                    var dataKodeDSPP = (from q in _dataContext.DetailSuratPenerimaanProduks
                                        orderby q.idDetailSuratPenerimaanProduk descending
                                        select q.idDetailSuratPenerimaanProduk).Take(1).ToList();
                    var dspp = new DetailSuratPenerimaanProduk
                    {
                        idDetailSuratPenerimaanProduk = dataKodeDSPP.ToCode(),
                        idSuratPenerimaanProduk = txtKodePenerimaanProduk.Text,
                        idProduk = dataGridPermintaan.Rows[i].Cells[0].Value.ToString(),
                        jumlahDiterima = int.Parse(dataGridPermintaan.Rows[i].Cells[3].Value.ToString())
                    };

                    _dataContext.DetailSuratPenerimaanProduks.InsertOnSubmit(dspp);
                    _dataContext.SubmitChanges();

                    Produk prd = new Produk();
                    var dataproduk = (from q in _dataContext.Produks
                                         where q.idProduk == dataGridPermintaan.Rows[i].Cells[0].Value.ToString()
                                         select q).First();
                    dataproduk.jumlah += int.Parse(dataGridPermintaan.Rows[i].Cells[3].Value.ToString());
                    _dataContext.SubmitChanges();
                }

                MessageBox.Show(@"Penerimaan berhasil diproses", @"Penyimpanan Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //saveMode(false);
                EraseAllFields();
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void txtJumlahDiterima_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                e.Handled = true;
        }

        private void dataGridPermintaan_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            _clickedRow = e.RowIndex;
        }
    }
}
