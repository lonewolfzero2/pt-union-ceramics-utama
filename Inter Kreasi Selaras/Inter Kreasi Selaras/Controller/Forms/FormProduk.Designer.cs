﻿namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    partial class FormProduk
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtKodeProduk = new System.Windows.Forms.TextBox();
            this.txtNamaProduk = new System.Windows.Forms.TextBox();
            this.txtHarga = new System.Windows.Forms.TextBox();
            this.txtJumlah = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panelProduk = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panelPencarian = new System.Windows.Forms.GroupBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cmbSearch = new System.Windows.Forms.ComboBox();
            this.txtKataKunci = new System.Windows.Forms.TextBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnBatal = new System.Windows.Forms.Button();
            this.panelButtonExecute = new System.Windows.Forms.Panel();
            this.btnTambah = new System.Windows.Forms.Button();
            this.btnUbah = new System.Windows.Forms.Button();
            this.btnHapus = new System.Windows.Forms.Button();
            this.panelButtonDML = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this._bs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panelProduk.SuspendLayout();
            this.panelPencarian.SuspendLayout();
            this.panelButtonExecute.SuspendLayout();
            this.panelButtonDML.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 117);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(579, 150);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 105);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Jumlah Persediaan";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Harga Per-kg";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // txtKodeProduk
            // 
            this.txtKodeProduk.Location = new System.Drawing.Point(122, 23);
            this.txtKodeProduk.Name = "txtKodeProduk";
            this.txtKodeProduk.ReadOnly = true;
            this.txtKodeProduk.Size = new System.Drawing.Size(155, 20);
            this.txtKodeProduk.TabIndex = 6;
            this.txtKodeProduk.Tag = "0";
            // 
            // txtNamaProduk
            // 
            this.txtNamaProduk.Location = new System.Drawing.Point(122, 49);
            this.txtNamaProduk.Name = "txtNamaProduk";
            this.txtNamaProduk.Size = new System.Drawing.Size(155, 20);
            this.txtNamaProduk.TabIndex = 7;
            this.txtNamaProduk.Tag = "1";
            // 
            // txtHarga
            // 
            this.txtHarga.Location = new System.Drawing.Point(149, 77);
            this.txtHarga.Name = "txtHarga";
            this.txtHarga.Size = new System.Drawing.Size(128, 20);
            this.txtHarga.TabIndex = 8;
            this.txtHarga.Tag = "3";
            this.txtHarga.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHarga_KeyPress);
            // 
            // txtJumlah
            // 
            this.txtJumlah.Location = new System.Drawing.Point(122, 102);
            this.txtJumlah.Name = "txtJumlah";
            this.txtJumlah.Size = new System.Drawing.Size(57, 20);
            this.txtJumlah.TabIndex = 9;
            this.txtJumlah.Tag = "4";
            this.txtJumlah.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtJumlah_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nama Produk";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Kode Produk";
            // 
            // panelProduk
            // 
            this.panelProduk.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panelProduk.Controls.Add(this.label7);
            this.panelProduk.Controls.Add(this.label6);
            this.panelProduk.Controls.Add(this.txtHarga);
            this.panelProduk.Controls.Add(this.txtJumlah);
            this.panelProduk.Controls.Add(this.label1);
            this.panelProduk.Controls.Add(this.label5);
            this.panelProduk.Controls.Add(this.label2);
            this.panelProduk.Controls.Add(this.label4);
            this.panelProduk.Controls.Add(this.txtKodeProduk);
            this.panelProduk.Controls.Add(this.txtNamaProduk);
            this.panelProduk.Location = new System.Drawing.Point(12, 280);
            this.panelProduk.Name = "panelProduk";
            this.panelProduk.Size = new System.Drawing.Size(296, 156);
            this.panelProduk.TabIndex = 124;
            this.panelProduk.TabStop = false;
            this.panelProduk.Text = "Detail Informasi";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(188, 105);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(22, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "KG";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(119, 77);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Rp.";
            // 
            // panelPencarian
            // 
            this.panelPencarian.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panelPencarian.Controls.Add(this.btnSearch);
            this.panelPencarian.Controls.Add(this.label14);
            this.panelPencarian.Controls.Add(this.label9);
            this.panelPencarian.Controls.Add(this.cmbSearch);
            this.panelPencarian.Controls.Add(this.txtKataKunci);
            this.panelPencarian.Controls.Add(this.btnRefresh);
            this.panelPencarian.Location = new System.Drawing.Point(320, 280);
            this.panelPencarian.Name = "panelPencarian";
            this.panelPencarian.Size = new System.Drawing.Size(271, 117);
            this.panelPencarian.TabIndex = 125;
            this.panelPencarian.TabStop = false;
            this.panelPencarian.Text = "Pencarian";
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(59, 75);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 158;
            this.btnSearch.Text = "Cari";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(155, 26);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 13);
            this.label14.TabIndex = 41;
            this.label14.Text = "Kata Kunci :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 24);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(97, 13);
            this.label9.TabIndex = 29;
            this.label9.Text = "Cari Berdasarkan : ";
            // 
            // cmbSearch
            // 
            this.cmbSearch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSearch.FormattingEnabled = true;
            this.cmbSearch.Items.AddRange(new object[] {
            "Kode Produk",
            "Nama Produk"});
            this.cmbSearch.Location = new System.Drawing.Point(15, 43);
            this.cmbSearch.Name = "cmbSearch";
            this.cmbSearch.Size = new System.Drawing.Size(131, 21);
            this.cmbSearch.TabIndex = 38;
            this.cmbSearch.SelectedIndexChanged += new System.EventHandler(this.cmbSearch_SelectedIndexChanged);
            // 
            // txtKataKunci
            // 
            this.txtKataKunci.Location = new System.Drawing.Point(158, 43);
            this.txtKataKunci.Name = "txtKataKunci";
            this.txtKataKunci.Size = new System.Drawing.Size(100, 20);
            this.txtKataKunci.TabIndex = 40;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(140, 75);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnRefresh.TabIndex = 39;
            this.btnRefresh.Text = "Muat Ulang";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnSave
            // 
            this.btnSave.Image = global::PT_Union_Ceramics_Utama.Properties.Resources.save;
            this.btnSave.Location = new System.Drawing.Point(7, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 28);
            this.btnSave.TabIndex = 156;
            this.btnSave.Text = "Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnYa_Click);
            // 
            // btnBatal
            // 
            this.btnBatal.Image = global::PT_Union_Ceramics_Utama.Properties.Resources.undod;
            this.btnBatal.Location = new System.Drawing.Point(100, 3);
            this.btnBatal.Name = "btnBatal";
            this.btnBatal.Size = new System.Drawing.Size(69, 28);
            this.btnBatal.TabIndex = 155;
            this.btnBatal.Text = "Reset";
            this.btnBatal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBatal.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnBatal.UseVisualStyleBackColor = true;
            this.btnBatal.Click += new System.EventHandler(this.btnBatal_Click);
            // 
            // panelButtonExecute
            // 
            this.panelButtonExecute.Controls.Add(this.btnBatal);
            this.panelButtonExecute.Controls.Add(this.btnSave);
            this.panelButtonExecute.Location = new System.Drawing.Point(218, 484);
            this.panelButtonExecute.Name = "panelButtonExecute";
            this.panelButtonExecute.Size = new System.Drawing.Size(181, 39);
            this.panelButtonExecute.TabIndex = 0;
            // 
            // btnTambah
            // 
            this.btnTambah.Location = new System.Drawing.Point(11, 3);
            this.btnTambah.Name = "btnTambah";
            this.btnTambah.Size = new System.Drawing.Size(75, 23);
            this.btnTambah.TabIndex = 121;
            this.btnTambah.Text = "Tambah";
            this.btnTambah.UseVisualStyleBackColor = true;
            this.btnTambah.Visible = false;
            this.btnTambah.Click += new System.EventHandler(this.btnTambah_Click);
            // 
            // btnUbah
            // 
            this.btnUbah.Location = new System.Drawing.Point(98, 3);
            this.btnUbah.Name = "btnUbah";
            this.btnUbah.Size = new System.Drawing.Size(75, 23);
            this.btnUbah.TabIndex = 123;
            this.btnUbah.Text = "Ubah";
            this.btnUbah.UseVisualStyleBackColor = true;
            this.btnUbah.Visible = false;
            this.btnUbah.Click += new System.EventHandler(this.btnUbah_Click);
            // 
            // btnHapus
            // 
            this.btnHapus.Location = new System.Drawing.Point(185, 3);
            this.btnHapus.Name = "btnHapus";
            this.btnHapus.Size = new System.Drawing.Size(75, 23);
            this.btnHapus.TabIndex = 122;
            this.btnHapus.Text = "Hapus";
            this.btnHapus.UseVisualStyleBackColor = true;
            this.btnHapus.Visible = false;
            this.btnHapus.Click += new System.EventHandler(this.btnHapus_Click);
            // 
            // panelButtonDML
            // 
            this.panelButtonDML.Controls.Add(this.btnHapus);
            this.panelButtonDML.Controls.Add(this.btnUbah);
            this.panelButtonDML.Controls.Add(this.btnTambah);
            this.panelButtonDML.Location = new System.Drawing.Point(24, 442);
            this.panelButtonDML.Name = "panelButtonDML";
            this.panelButtonDML.Size = new System.Drawing.Size(271, 30);
            this.panelButtonDML.TabIndex = 157;
            // 
            // FormProduk
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(603, 520);
            this.Controls.Add(this.panelButtonDML);
            this.Controls.Add(this.panelButtonExecute);
            this.Controls.Add(this.panelPencarian);
            this.Controls.Add(this.panelProduk);
            this.Controls.Add(this.dataGridView1);
            this.Name = "FormProduk";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Master Produk";
            this.Controls.SetChildIndex(this.dataGridView1, 0);
            this.Controls.SetChildIndex(this.panelProduk, 0);
            this.Controls.SetChildIndex(this.panelPencarian, 0);
            this.Controls.SetChildIndex(this.panelButtonExecute, 0);
            this.Controls.SetChildIndex(this.panelButtonDML, 0);
            ((System.ComponentModel.ISupportInitialize)(this._bs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panelProduk.ResumeLayout(false);
            this.panelProduk.PerformLayout();
            this.panelPencarian.ResumeLayout(false);
            this.panelPencarian.PerformLayout();
            this.panelButtonExecute.ResumeLayout(false);
            this.panelButtonDML.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;

        private System.Windows.Forms.TextBox txtKodeProduk;
        private System.Windows.Forms.TextBox txtNamaProduk;
        private System.Windows.Forms.TextBox txtHarga;
        private System.Windows.Forms.TextBox txtJumlah;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox panelProduk;
        private System.Windows.Forms.GroupBox panelPencarian;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cmbSearch;
        private System.Windows.Forms.TextBox txtKataKunci;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnBatal;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Panel panelButtonExecute;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnTambah;
        private System.Windows.Forms.Button btnUbah;
        private System.Windows.Forms.Button btnHapus;
        private System.Windows.Forms.Panel panelButtonDML;
    }
}