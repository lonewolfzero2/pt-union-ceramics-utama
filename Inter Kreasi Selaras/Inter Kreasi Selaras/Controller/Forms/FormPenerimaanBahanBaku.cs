﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using PT_Union_Ceramics_Utama.Controller.Core;
using PT_Union_Ceramics_Utama.Controller.Helpers;

namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    public partial class FormPenerimaanBahanBaku : BaseLogoForm
    {
        public int posisiCurrent = 0;
        int _clickedRow = 0;
        FormPencarianPemasokanBarang _fppb;

        public FormPenerimaanBahanBaku(Form parent) : base(parent)
        {
            InitializeComponent();
            SetKodePenerimaan();
        }

        private void SetKodePenerimaan()
        {
            var dataKodePenerimaan = (from q in _dataContext.SuratPenerimaanBahanBakus
                                      orderby q.idSuratPenerimaanBahanBaku descending
                                      select q.idSuratPenerimaanBahanBaku).Take(1).ToList();
            txtKodePenerimaan.Text = dataKodePenerimaan.ToCode();
        }


        private void SaveMode(bool flag)
        {
            btnSave.Enabled = flag;
            btnReset.Enabled = !flag;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            //var formPencarianPermintaanBahanBaku = new BaseFormPencarian(this, "PO.", DataContexts.DataContext.PurchaseOrders);
            //formPencarianPermintaanBahanBaku.Initiate();
            if (_fppb == null || _fppb.IsDisposed)
            {
                _fppb = new FormPencarianPemasokanBarang(this);
                _fppb.Initiate();
            }
        }

        private void EraseAllFields()
        {
            txtKodeSuratPermintaan.Text = "";
            txtKodePegawai.Text = "";
            txtKodePemasok.Text = "";
            txtNamaPerusahaan.Text = "";
            txtTanggal.Text = "";
            UpdateGridView(null, datagridviewPermintaan);

            txtKodeBahanBaku.Text = "";
            txtNamaBahanBaku.Text = "";
            txtJumlahDiminta.Text = "";
            txtJumlahDiterima.Text = "";
            UpdateGridView(null, datagridviewPenerimaan);
        }

        private void txtJumlahDiterima_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                e.Handled = true;
        }

        private void datagridviewPermintaan_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            txtKodeBahanBaku.Text = datagridviewPermintaan.Rows[e.RowIndex].Cells[0].Value.ToString();
            txtNamaBahanBaku.Text = datagridviewPermintaan.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtJumlahDiminta.Text = datagridviewPermintaan.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtJumlahDiterima.Text = "";
            _clickedRow = e.RowIndex;
        }

        private void btnAddToCart_Click(object sender, EventArgs e)
        {
            if (txtKodeSuratPermintaan.Text.Equals("") || txtKodePenerimaan.Text.Equals("") || txtJumlahDiterima.Text.Equals(""))
            {
                MessageBox.Show(@"Lengkapi data Pemasokan dan Bahan Baku", @"Penambahan Penerimaan", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                int posisiUbah = 0;
                int jumlahMinta = int.Parse(txtJumlahDiminta.Text);
                int jumlahTerima = int.Parse(txtJumlahDiterima.Text);
                if (jumlahTerima > jumlahMinta || jumlahTerima < 1)
                {
                    MessageBox.Show(@"Jumlah yang Diterima melebihi Jumlah Permintaan", @"Penambahan Penerimaan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    object[] buffer = new object[5];
                    buffer[0] = txtKodeBahanBaku.Text;
                    buffer[1] = txtNamaBahanBaku.Text;
                    buffer[2] = txtJumlahDiterima.Text;
                    buffer[3] = int.Parse(txtJumlahDiminta.Text) - int.Parse(txtJumlahDiterima.Text);

                    bool ada = false;
                    for (int i = 0; i < datagridviewPenerimaan.Rows.Count; i++)
                    {
                        if (datagridviewPenerimaan.Rows[i].Cells[0].Value.ToString().Equals(buffer[0].ToString()))
                        {
                            ada = true;
                            posisiUbah = i;
                            break;
                        }

                    }

                    if (ada == false)
                    {
                        List<DataGridViewRow> rows = new List<DataGridViewRow>();
                        rows.Add(new DataGridViewRow());
                        rows[posisiCurrent].CreateCells(datagridviewPenerimaan, buffer);
                        datagridviewPenerimaan.Rows.AddRange(rows.ToArray());
                    }
                    else
                    {
                        int jumlahCurrent = int.Parse(datagridviewPenerimaan.Rows[posisiUbah].Cells[2].Value.ToString());
                        int jumlahYangMauDimasukkan = jumlahCurrent + (int.Parse(txtJumlahDiterima.Text));
                        jumlahMinta = int.Parse(txtJumlahDiminta.Text);
                        if (jumlahYangMauDimasukkan > jumlahMinta)
                        {
                            MessageBox.Show(@"Jumlah yang Diterima melebihi Jumlah Permintaan", @"Penambahan Penerimaan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            // ubah jumlah
                            datagridviewPenerimaan.Rows[posisiUbah].Cells[2].Value = jumlahYangMauDimasukkan;
                        }
                    }

                }
                if (datagridviewPenerimaan.Rows.Count > 0)
                {
                    panelButtonExecute.Enabled = true;
                }
            }
        }

        private void datagridviewPenerimaan_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            _clickedRow = e.RowIndex;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (datagridviewPenerimaan.Rows.Count > 0)
            {

                DialogResult result = MessageBox.Show(@"Anda yakin ingin hapus pesanan ini?", @"Penghapusan Pesanan", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (result == DialogResult.Yes)
                {
                    datagridviewPenerimaan.Rows.RemoveAt(_clickedRow);
                    if (datagridviewPenerimaan.Rows.Count == 0)
                    {
                        panelButtonExecute.Enabled = false;
                    }
                }
            }
            else
            {
                MessageBox.Show(@"Pilih dulu datanya", @"Penghapusan Pesanan", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (datagridviewPenerimaan.Rows.Count > 0)
            {
                panelButtonExecute.Enabled = true;
            }
        }

        private void btnEmptyCart_Click(object sender, EventArgs e)
        {
            if (datagridviewPenerimaan.Rows.Count > 0)
            {
                DialogResult result = MessageBox.Show(@"Anda yakin ingin hapus semua pesanan?", @"Pengosongan List", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (result == DialogResult.Yes)
                {
                    datagridviewPenerimaan.Rows.Clear();
                    panelButtonExecute.Enabled = false;
                }
            }
            else
            {
                MessageBox.Show(@"Lengkapi data pemesanan dulu", @"Pengosongan List", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            EraseAllFields();
            panelButtonExecute.Enabled = false;
            SaveMode(true);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SuratPenerimaanBahanBaku spp = new SuratPenerimaanBahanBaku();
                spp.idSuratPenerimaanBahanBaku = txtKodePenerimaan.Text;
                spp.idPO = txtKodeSuratPermintaan.Text;
                spp.tanggalPenerimaanBahanBaku = DateTime.Now;
                // nanti diganti sama karyawan yg lagi login
                spp.idPegawai = ActiveUser.Instance.Id;
                _dataContext.SuratPenerimaanBahanBakus.InsertOnSubmit(spp);

                _dataContext.SubmitChanges();

                for (int i = 0; i < datagridviewPenerimaan.Rows.Count; i++)
                {
                    var dataKodeDSPP = (from q in _dataContext.DetailSuratPenerimaanBahanBakus
                                        orderby q.idDetailSuratPenerimaanBahanBaku descending
                                        select q.idDetailSuratPenerimaanBahanBaku).Take(1).ToList();
                    var dspp = new DetailSuratPenerimaanBahanBaku
                        {
                            idDetailSuratPenerimaanBahanBaku = dataKodeDSPP.ToCode(), 
                            idSuratPenerimaanBahanBaku = txtKodePenerimaan.Text, 
                            idBahanBaku = datagridviewPenerimaan.Rows[i].Cells[0].Value.ToString(), 
                            jumlahDiterima = int.Parse(datagridviewPenerimaan.Rows[i].Cells[2].Value.ToString())
                        };
                    _dataContext.DetailSuratPenerimaanBahanBakus.InsertOnSubmit(dspp);
                    BahanBaku baku = new BahanBaku();
                    var dataBahanBaku = (from q in _dataContext.BahanBakus
                                         where q.idBahanBaku == datagridviewPenerimaan.Rows[i].Cells[0].Value.ToString()
                                         select q).First();
                    dataBahanBaku.Berat += int.Parse(datagridviewPenerimaan.Rows[i].Cells[2].Value.ToString());
                    
                    
                    DetailPO DPPM = new DetailPO();
                    var permintaan = (from q in _dataContext.DetailPOs
                                         where q.idBahanBaku == datagridviewPenerimaan.Rows[i].Cells[0].Value.ToString()
                                         select q).First();
                    permintaan.jumlahDiminta = int.Parse(datagridviewPenerimaan.Rows[i].Cells[3].Value.ToString());
                    _dataContext.SubmitChanges();
                }

                MessageBox.Show(@"Penerimaan berhasil diproses", @"Penyimpanan Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //saveMode(false);
                EraseAllFields();
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            //LaporanStokGudang sg = new LaporanStokGudang();
            //sg.Show();
            Dispose();
        }

        private void datagridviewPermintaan_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtKodeBahanBaku.Text = datagridviewPermintaan.Rows[e.RowIndex].Cells[0].Value.ToString();
            txtNamaBahanBaku.Text = datagridviewPermintaan.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtJumlahDiminta.Text = datagridviewPermintaan.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtJumlahDiterima.Text = "";
            _clickedRow = e.RowIndex;
        }
    }
}
