﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using PT_Union_Ceramics_Utama.Controller.Helpers;

namespace PT_Union_Ceramics_Utama
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            var conn = DatabaseHandler.SqlConnection;
            conn.Open();

            String q = "Select * from BahanBaku";
            SqlDataAdapter dscmd = new SqlDataAdapter(q, conn);
            DataSet1 ds = new DataSet1();
            dscmd.Fill(ds, "BahanBaku");

            LaporanBahanBaku objRpt = new LaporanBahanBaku();
            objRpt.SetDataSource(ds);
            crystalReportViewer1.ReportSource = objRpt;
            crystalReportViewer1.Refresh();
            conn.Close();
        }
    }
}
