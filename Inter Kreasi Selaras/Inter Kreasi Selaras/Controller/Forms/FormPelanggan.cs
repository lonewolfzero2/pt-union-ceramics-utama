﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using PT_Union_Ceramics_Utama.Controller.Helpers;
using PT_Union_Ceramics_Utama.Controller.Helpers.Enums;

namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    public partial class FormPelanggan : GridForm
    {
        private DmlModes _dmlMode;
        private readonly List<string> _dataGridValueCollection = new List<string>();
        private bool _datagridClicked;

        public FormPelanggan(Form parent) : base(parent)
        {
            InitializeComponent();

            cmbSearch.SelectedIndex = 0;
            btnHapus.Visible = false;
        }

        public override void Initiate()
        {
            _defaultDataGridView = dataGridView1;
            _textBoxCollection = new TextBoxCollection(dataGridView1, groupBox1);
            base.Initiate();
        }

        protected override object _data
        {
            get
            {
                return from a in _dataContext.Agens
                       select new {a.kdPelanggan, a.tanggalPendaftaran, a.nama, a.noTelepon, a.noFax, a.jenisKelamin, a.email, a.kota, a.alamat};
            }
        }

        protected override void EnablePanel(bool status)
        {
            dataGridView1.Enabled = status;
            panelButtonDML.Enabled = status;
            panelButtonExecute.Enabled = !status;
            groupBox1.Enabled = !status;
            panelButtonSearch.Enabled = status;
        }

        public void SetTextbox()
        {
            MessageBox.Show(_textBoxCollection.Count.ToString());
            MessageBox.Show(_dataGridValueCollection.Count.ToString());
            for (int i = 0; i < 7; i++)
            {
                _textBoxCollection.ElementAt(i).Text = _dataGridValueCollection.ElementAt(i);
            }
            _textBoxCollection.ClearValues(DmlModes.Menghapus);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            txtKodePelanggan.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            txtTanggalPendaftaran.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtNama.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtTelepon.Text = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
            txtFax.Text = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();
            txtEmail.Text = dataGridView1.Rows[e.RowIndex].Cells[6].Value.ToString();
            txtKota.Text = dataGridView1.Rows[e.RowIndex].Cells[7].Value.ToString();
            txtAlamat.Text = dataGridView1.Rows[e.RowIndex].Cells[8].Value.ToString();
            
            var jenisKelamin = dataGridView1.Rows[e.RowIndex].Cells[5].Value.ToString();
            if (jenisKelamin.Contains("L"))
            {
                radioButton1.Checked = true;
            }
            else
            {
                radioButton2.Checked = true;
            }
            _datagridClicked = true;
        }

        private void btnTambah_Click(object sender, EventArgs e)
        {
            _dmlMode = DmlModes.Memasukan;
            _textBoxCollection.ClearValues(DmlModes.Memasukan);
            EnablePanel(false);
            var data = (from prod in _dataContext.Agens
                        orderby prod.kdPelanggan descending
                        select prod.kdPelanggan).Take(1).ToList();
            txtKodePelanggan.Text = data.ToCode();
            txtKodePelanggan.Enabled = false;
            txtTanggalPendaftaran.Text = DateTime.Now.ToShortDateString();
        }

        private void btnUbah_Click(object sender, EventArgs e)
        {
            if (_datagridClicked)
            {
                _dmlMode = DmlModes.Mengubah;
                EnablePanel(false);
                _datagridClicked = false;
            }
            else
            {
                MessageBox.Show(@"Pilih dulu datanya", @"Pengubahan Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnHapus_Click(object sender, EventArgs e)
        {
            if (_datagridClicked)
            {
                _dmlMode = DmlModes.Mengubah;
                _datagridClicked = false;

                var data = (from q in _dataContext.Agens
                            where q.kdPelanggan == txtKodePelanggan.Text
                            select q).First();
                DialogResult intReturnValue = MessageBox.Show(@"Apa Anda yakin ingin menghapus data ini?", @"Penghapusan Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                if (intReturnValue == DialogResult.OK)
                {
                    _dataContext.Agens.DeleteOnSubmit(data);
                    _dataContext.SubmitChanges();
                    _textBoxCollection.ClearValues(DmlModes.Menghapus);
                    RefreshForm();
                    EnablePanel(true);
                    MessageBox.Show(@"Data berhasil di hapus", @"Penghapusan Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show(@"Pilih dulu datanya", @"Penghapusan Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtKataKunci.Text))
            {
                MessageBox.Show(@"Isi kata kuncinya dulu", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (cmbSearch.SelectedIndex==0)
                {
                    try
                    {
                        var dataKodePelanggan = (from q in _dataContext.Agens
                                              where q.kdPelanggan.Contains(txtKataKunci.Text)
                                              select q).First();
                        UpdateGridView(dataKodePelanggan);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show(@"Data tidak ditemukan", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                else if (cmbSearch.SelectedIndex == 1)
                {
                    try
                    {
                        var dataNamaPelanggan = (from q in _dataContext.Agens
                                                 where q.nama.Contains(txtKataKunci.Text)
                                              select q).ToList();
                        UpdateGridView(dataNamaPelanggan);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show(@"Data tidak ditemukan", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
            }
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            RefreshForm();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!_textBoxCollection.IsEmpty)
            {
                if (txtEmail.IsEmail())
                {
                    Agen Pelanggan = new Agen();
                    string jenisKelamin = radioButton1.Checked?"Laki-Laki":"Perempuan";
                    if (_dmlMode != DmlModes.Menghapus)
                    {
                        switch (_dmlMode)
                        {
                            case DmlModes.Memasukan:
                                Pelanggan.kdPelanggan = txtKodePelanggan.Text;
                                Pelanggan.tanggalPendaftaran = Convert.ToDateTime(txtTanggalPendaftaran.Text);
                                Pelanggan.nama = txtNama.Text;
                                Pelanggan.noTelepon = txtTelepon.Text;
                                Pelanggan.noFax = txtFax.Text;
                                Pelanggan.jenisKelamin = jenisKelamin;
                                Pelanggan.email = txtEmail.Text;
                                Pelanggan.kota = txtKota.Text;
                                Pelanggan.alamat = txtAlamat.Text;
                                Pelanggan.tanggalLahir = "01/01/1900";
                                _dataContext.Agens.InsertOnSubmit(Pelanggan);
                                break;
                            case DmlModes.Mengubah:
                                {
                                    var data = (from q in _dataContext.Agens
                                                where q.kdPelanggan == txtKodePelanggan.Text
                                                select q).First();
                                    data.nama = txtNama.Text;
                                    data.noTelepon = txtTelepon.Text;
                                    data.noFax = txtFax.Text;
                                    data.jenisKelamin = jenisKelamin;
                                    data.email = txtEmail.Text;
                                    data.kota = txtKota.Text;
                                    data.alamat = txtAlamat.Text;
                                }
                                break;
                        }

                        _dataContext.SubmitChanges();
                        _textBoxCollection.ClearValues(_dmlMode);
                        EnablePanel(true);
                    }
                    RefreshForm();
                    var s = (_dmlMode == DmlModes.Mengubah) ? "mengubah" : "memasukkan";
                    MessageBox.Show(@"Berhasil " + s + @" data", @"Penyimpanan Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(@"Format Email adalah username@domain.com", @"Penyimpanan Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show(@"Data tidak boleh kosong", @"Penyimpanan Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtTelepon_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }

        private void txtFax_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtKodePelanggan.Text = "";
            txtNama.Text = "";
            txtAlamat.Text = "";
            txtTelepon.Text = "";
            txtFax.Text = "";
            txtEmail.Text = "";
            txtKota.Text = "";
            txtAlamat.Text = "";
            EnablePanel(true);
        }

        private bool CekEmail()
        {
            if (txtEmail.Text.Trim() != "")
            {
                Match rex = Regex.Match(txtEmail.Text.Trim(' '), "^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,3})$", RegexOptions.IgnoreCase);
                if (rex.Success == false)
                {
                    txtEmail.Focus();
                    return false;
                }
                return true;
            }
            return true;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtKodePelanggan.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            txtTanggalPendaftaran.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtNama.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtTelepon.Text = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
            txtFax.Text = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();
            txtEmail.Text = dataGridView1.Rows[e.RowIndex].Cells[6].Value.ToString();
            txtKota.Text = dataGridView1.Rows[e.RowIndex].Cells[7].Value.ToString();
            txtAlamat.Text = dataGridView1.Rows[e.RowIndex].Cells[8].Value.ToString();


            string jenisKelamin = dataGridView1.Rows[e.RowIndex].Cells[5].Value.ToString();
            if (jenisKelamin.Contains("L"))
            {
                radioButton1.Checked = true;
            }
            else
            {
                radioButton2.Checked = true;
            }
            _datagridClicked = true;
        }

        private void FormPelanggan_Load(object sender, EventArgs e)
        {

        }


        
    }
}
