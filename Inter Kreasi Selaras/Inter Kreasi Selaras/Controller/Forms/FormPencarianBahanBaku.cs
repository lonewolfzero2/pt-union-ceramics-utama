﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Inter_Kreasi_Selaras
{
    public partial class FormPencarianBahanBaku : Form
    {
        String kd = "";
        public FormPencarianBahanBaku()
        {
            InitializeComponent();
            String conn = "Data Source=FRONTIER\\EDBERT;Initial Catalog=Inter Kreasi Selaras;Integrated Security=True";
            string q = "select * from BahanBaku";
            SqlDataAdapter dataAdapter = new SqlDataAdapter(q, conn);

            DataTable table = new DataTable();
            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
            dataAdapter.Fill(table);
            bindingSource1.DataSource = table;

            dataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader);
            dataGridView1.ReadOnly = true;
            dataGridView1.DataSource = bindingSource1;
            
        }

        private void btnPilih_Click(object sender, EventArgs e)
        {
           FormCetakBahanBaku fcb = new FormCetakBahanBaku();
           this.Dispose();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            kd = dataGridView1.CurrentRow.Cells[0].Value.ToString();
        }
    }
}
