﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    public partial class FormPencarianPengeluaranBahanBaku : GridForm
    {
        private readonly FormPenerimaanProduk _myFormPenerimaanProduk;
        private string _kdSuratPengeluaranBahanBaku = "";
        private string _kdPegawai = "";
        private string _tanggalKeluar = "";

        public FormPencarianPengeluaranBahanBaku(FormPenerimaanProduk parent) : base(null)
        {
            InitializeComponent();
            _myFormPenerimaanProduk = parent;
        }

        public override void Initiate()
        {
            _defaultDataGridView = dataGridView1;
            base.Initiate();
        }

        protected override object _data
        {
            get
            {
                return from q in _dataContext.SuratPengeluaranBahanBakus
                       select new { q.idSuratPengeluaranBahanBaku, q.tanggalkeluar, q.idPegawai };
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtKataKunci.Text.Equals(""))
            {
                MessageBox.Show(@"Isi kata kuncinya dulu", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (cmbSearch.SelectedIndex == 0)
                {
                    try
                    {
                        var dataPembelian = (from q in _dataContext.SuratPengeluaranBahanBakus
                                             where q.idSuratPengeluaranBahanBaku.Contains(txtKataKunci.Text)
                                             select q).First();
                        UpdateGridView(dataPembelian);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show(@"Data tidak ditemukan", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                else if (cmbSearch.SelectedIndex == 1)
                {
                    try
                    {
                        var dataPembelian = (from q in _dataContext.SuratPengeluaranBahanBakus
                                             join w in _dataContext.DetailSuratPengeluaranBahanBakus
                                             on q.idSuratPengeluaranBahanBaku equals w.idSuratPengeluaranBahanBaku
                                             where w.idBahanBaku.Contains(txtKataKunci.Text)
                                             select q).ToList();
                        UpdateGridView(dataPembelian);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show(@"Data tidak ditemukan", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            RefreshForm();
        }

        private void btnPilih_Click_1(object sender, EventArgs e)
        {
            if (_kdSuratPengeluaranBahanBaku.Equals(""))
            {
                MessageBox.Show(@"Pilih dulu datanya", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                var JumlahKeluar = (from q in _dataContext.DetailSuratPengeluaranBahanBakus
                                      join w in _dataContext.SuratPengeluaranBahanBakus
                                      on q.idSuratPengeluaranBahanBaku equals w.idSuratPengeluaranBahanBaku
                                      where q.idSuratPengeluaranBahanBaku == _kdSuratPengeluaranBahanBaku
                                      select q.jumlahKeluar).First();

                var NamaBahanBaku = (from q in _dataContext.DetailSuratPengeluaranBahanBakus
                                    join p in _dataContext.BahanBakus
                                    on q.idBahanBaku equals p.idBahanBaku
                                    where q.idSuratPengeluaranBahanBaku == _kdSuratPengeluaranBahanBaku
                                    select p.namaBahanBaku).First();

                var produk = (from q in _dataContext.Produks
                                        select q);

                _myFormPenerimaanProduk.txtKodeSrtPengeluaranBahanBaku.Text = _kdSuratPengeluaranBahanBaku;
                _myFormPenerimaanProduk.txtNamaBB.Text = NamaBahanBaku;
                _myFormPenerimaanProduk.txtKodePegawai.Text = _kdPegawai;
                _myFormPenerimaanProduk.txtJumlahKeluar.Text = JumlahKeluar.ToString();
                _myFormPenerimaanProduk.txtTanggalKeluar.Text = _tanggalKeluar;

                _myFormPenerimaanProduk.dataGridProduk.DataSource = produk;
                Dispose();
            }
        }

        private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            _kdSuratPengeluaranBahanBaku = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            _tanggalKeluar = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            _kdPegawai = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
        }

    }
}
