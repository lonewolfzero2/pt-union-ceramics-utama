﻿using System;
using System.Linq;
using System.Windows.Forms;
using PT_Union_Ceramics_Utama.Controller.Core;
using PT_Union_Ceramics_Utama.Controller.Helpers;
using PT_Union_Ceramics_Utama.Controller.Helpers.Enums;

namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    public partial class FormBahanBaku : GridForm
    {
        private DmlModes _dmlMode;

        public FormBahanBaku(Form parent) : base(parent)
        {
            InitializeComponent();

            cmbSearch.SelectedIndex = 1;
            _defaultDataGridView = dataGridView1;
        }

        public override void Initiate()
        {
            _defaultDataGridView = dataGridView1;
            _textBoxCollection = new TextBoxCollection(dataGridView1, panelBahanBaku);
            base.Initiate();
        }

        protected override object _data
        {
            get { return from a in _dataContext.BahanBakus select new {a.idBahanBaku, a.namaBahanBaku, a.Berat}; }
        }

        protected override void EnablePanel(bool status)
        {
            dataGridView1.Enabled = status;
            panelButtonDML.Enabled = status;
            panelButtonExecute.Enabled = !status;
            panelBahanBaku.Enabled = !status;
            panelPencarian.Enabled = status;
            btnRefresh.Enabled = status;

            if (_dmlMode == DmlModes.Memasukan)
                txtKodeBahanBaku.Enabled = true;
        }

        #region Interaction

        private void btnHapus_Click(object sender, EventArgs e)
        {
            if (_isDataGridClicked)
            {
                _dmlMode = DmlModes.Menghapus;
                _isDataGridClicked = false;

                var data = (from q in _dataContext.BahanBakus
                            where q.idBahanBaku == txtKodeBahanBaku.Text
                            select q).First();
                DialogResult intReturnValue = MessageBox.Show(@"Apa Anda yakin ingin menghapus data ini?", @"Penghapusan Data", MessageBoxButtons.YesNo,MessageBoxIcon.Exclamation);
                if (intReturnValue == DialogResult.Yes)
                {
                    _dataContext.BahanBakus.DeleteOnSubmit(data);
                    _dataContext.SubmitChanges();
                    _textBoxCollection.ClearValues(DmlModes.Menghapus);
                    RefreshForm();
                    EnablePanel(true);
                    MessageBox.Show(@"Data berhasil di hapus",@"Penghapusan Data",MessageBoxButtons.OK,MessageBoxIcon.Information);
                }
            }

            else
            {
                MessageBox.Show(@"Pilih dulu datanya",@"Penghapusan Data",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }

        private void btnUbah_Click(object sender, EventArgs e)
        {
            if (_isDataGridClicked)
            {
                _dmlMode = DmlModes.Mengubah;
                EnablePanel(false);
                _isDataGridClicked = false;
            }
            else
            {
                MessageBox.Show(@"Pilih dulu datanya",@"Pengubahan Data",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }

        private void btnTambah_Click(object sender, EventArgs e)
        {
            _dmlMode = DmlModes.Memasukan;
            _textBoxCollection.ClearValues(DmlModes.Memasukan);
            EnablePanel(false);
            var data = (from prod in _dataContext.BahanBakus
                        orderby prod.idBahanBaku descending
                        select prod.idBahanBaku).Take(1).ToList();
            txtKodeBahanBaku.Text = data.ToCode();
            txtKodeBahanBaku.Enabled = false;
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            txtKataKunci.Text = "";
            txtKodeBahanBaku.Text = "";
            txtNama.Text = "";
            txtUkuran.Text = "";
            EnablePanel(true);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (_textBoxCollection.IsEmpty)
            {
                MessageBox.Show(@"Data tidak boleh kosong", @"Penyimpanan Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                BahanBaku bahanBaku = new BahanBaku();
                if (_dmlMode != DmlModes.Menghapus)
                {
                    switch (_dmlMode)
                    {
                        case DmlModes.Memasukan:
                            {
                                bahanBaku.idBahanBaku = txtKodeBahanBaku.Text;
                                bahanBaku.namaBahanBaku = txtNama.Text;
                                int temp;
                                if (int.TryParse(txtUkuran.Text, out temp))
                                    bahanBaku.Berat = int.Parse(txtUkuran.Text);
                                _dataContext.BahanBakus.InsertOnSubmit(bahanBaku);
                            }
                            break;
                        case DmlModes.Mengubah:
                            {
                                var data = (from q in _dataContext.BahanBakus
                                            where q.idBahanBaku == txtKodeBahanBaku.Text
                                            select q).First();
                                data.namaBahanBaku = txtNama.Text;
                                bahanBaku.namaBahanBaku = txtNama.Text;
                                bahanBaku.Berat = int.Parse(txtUkuran.Text);
                            }
                            break;
                    }

                    _dataContext.SubmitChanges();
                    _textBoxCollection.ClearValues(_dmlMode);
                    EnablePanel(true);
                }
                RefreshForm();
                var s = (_dmlMode == DmlModes.Mengubah) ? "mengubah" : "memasukkan";
                MessageBox.Show(@"Berhasil " + s + @" data", @"Penyimpanan Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtUkuran_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }
        
        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (cmbSearch.SelectedIndex == 2)
            {
                // ROP
                txtKataKunci.ReadOnly = true;
                txtKataKunci.Enabled = true;
                try
                {
                    var dataKodeProduk = (from q in _dataContext.BahanBakus
                                          where q.Berat == 0
                                          select q).First();

                    UpdateGridView(dataKodeProduk);
                }
                catch (Exception)
                {
                    MessageBox.Show(@"Data tidak ditemukan", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else if (string.IsNullOrEmpty(txtKataKunci.Text))
                {
                    MessageBox.Show(@"Isi kata kuncinya dulu",@"Pencarian Data",MessageBoxButtons.OK,MessageBoxIcon.Error);
                }
                else
                {
                    if (cmbSearch.SelectedItem.ToString().Equals("Kode Bahan Baku"))
                    {
                        try
                        {
                            var dataKodeProduk = (from q in _dataContext.BahanBakus
                                              where q.idBahanBaku.Contains(txtKataKunci.Text)
                                              select q).First();
                            UpdateGridView(dataKodeProduk);
                        }
                        catch (Exception)
                        {
                            MessageBox.Show(@"Data tidak ditemukan", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else if (cmbSearch.SelectedItem.ToString().Equals("Nama Bahan Baku"))
                    {
                        try
                        {
                            var dataNamaProduk = (from q in _dataContext.BahanBakus
                                               where q.namaBahanBaku.Contains(txtKataKunci.Text)
                                               select q).ToList();
                            UpdateGridView(dataNamaProduk);
                        }
                        catch (Exception)
                        {
                            MessageBox.Show(@"Data tidak ditemukan", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else
                    {
                        txtKataKunci.ReadOnly=true;
                        try
                        {
                            var dataKodeProduk = (from q in _dataContext.BahanBakus
                                                  where q.Berat < 100
                                                  select q).First();
                            UpdateGridView(dataKodeProduk);
                        }
                        catch (Exception)
                        {
                            MessageBox.Show(@"Data tidak ditemukan", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                } 
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            RefreshForm();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            OnDataGridClicked(e.RowIndex);
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            OnDataGridClicked(e.RowIndex);
        }

        #endregion
    }
}
