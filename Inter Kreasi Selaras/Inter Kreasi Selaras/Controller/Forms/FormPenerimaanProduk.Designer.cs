﻿namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    partial class FormPenerimaanProduk
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelButtonExecute = new System.Windows.Forms.Panel();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtHarga = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtNamaProduk = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnEmpty = new System.Windows.Forms.Button();
            this.btnAddToCart = new System.Windows.Forms.Button();
            this.txtKodePenerimaanProduk = new System.Windows.Forms.TextBox();
            this.txtKodeProduk = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtJumlahDiterima = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtTanggalKeluar = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtKodePegawai = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSearchPemasok = new System.Windows.Forms.Button();
            this.txtKodeSrtPengeluaranBahanBaku = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.dataGridProduk = new System.Windows.Forms.DataGridView();
            this.txtJumlahKeluar = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtNamaBB = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridPermintaan = new System.Windows.Forms.DataGridView();
            this.KodeProduk = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NamaProduk = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Harga = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JumlahDiterima = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnReset = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this._bs)).BeginInit();
            this.panelButtonExecute.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridProduk)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPermintaan)).BeginInit();
            this.SuspendLayout();
            // 
            // panelButtonExecute
            // 
            this.panelButtonExecute.Controls.Add(this.btnPrint);
            this.panelButtonExecute.Controls.Add(this.btnSave);
            this.panelButtonExecute.Location = new System.Drawing.Point(323, 548);
            this.panelButtonExecute.Name = "panelButtonExecute";
            this.panelButtonExecute.Size = new System.Drawing.Size(176, 37);
            this.panelButtonExecute.TabIndex = 162;
            // 
            // btnPrint
            // 
            this.btnPrint.Image = global::PT_Union_Ceramics_Utama.Properties.Resources.agt_print;
            this.btnPrint.Location = new System.Drawing.Point(3, 6);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 28);
            this.btnPrint.TabIndex = 155;
            this.btnPrint.Text = "Cetak";
            this.btnPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Visible = false;
            // 
            // btnSave
            // 
            this.btnSave.Image = global::PT_Union_Ceramics_Utama.Properties.Resources.save;
            this.btnSave.Location = new System.Drawing.Point(94, 6);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 28);
            this.btnSave.TabIndex = 154;
            this.btnSave.Text = "Simpan";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.txtHarga);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.txtNamaProduk);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.btnDelete);
            this.groupBox3.Controls.Add(this.btnEmpty);
            this.groupBox3.Controls.Add(this.btnAddToCart);
            this.groupBox3.Controls.Add(this.txtKodePenerimaanProduk);
            this.groupBox3.Controls.Add(this.txtKodeProduk);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.txtJumlahDiterima);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Location = new System.Drawing.Point(22, 338);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(361, 204);
            this.groupBox3.TabIndex = 160;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Informasi Produk";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(198, 111);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(22, 13);
            this.label11.TabIndex = 163;
            this.label11.Text = "RP";
            // 
            // txtHarga
            // 
            this.txtHarga.Enabled = false;
            this.txtHarga.Location = new System.Drawing.Point(234, 104);
            this.txtHarga.Name = "txtHarga";
            this.txtHarga.Size = new System.Drawing.Size(120, 20);
            this.txtHarga.TabIndex = 161;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 107);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(36, 13);
            this.label9.TabIndex = 162;
            this.label9.Text = "Harga";
            // 
            // txtNamaProduk
            // 
            this.txtNamaProduk.Enabled = false;
            this.txtNamaProduk.Location = new System.Drawing.Point(201, 78);
            this.txtNamaProduk.Name = "txtNamaProduk";
            this.txtNamaProduk.ReadOnly = true;
            this.txtNamaProduk.Size = new System.Drawing.Size(153, 20);
            this.txtNamaProduk.TabIndex = 160;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 81);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 159;
            this.label1.Text = "Nama Produk";
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(157, 163);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(60, 32);
            this.btnDelete.TabIndex = 158;
            this.btnDelete.Text = "Hapus";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEmpty
            // 
            this.btnEmpty.Location = new System.Drawing.Point(223, 163);
            this.btnEmpty.Name = "btnEmpty";
            this.btnEmpty.Size = new System.Drawing.Size(90, 32);
            this.btnEmpty.TabIndex = 157;
            this.btnEmpty.Text = "Kosongkan List";
            this.btnEmpty.UseVisualStyleBackColor = true;
            this.btnEmpty.Click += new System.EventHandler(this.btnEmpty_Click);
            // 
            // btnAddToCart
            // 
            this.btnAddToCart.Location = new System.Drawing.Point(40, 163);
            this.btnAddToCart.Name = "btnAddToCart";
            this.btnAddToCart.Size = new System.Drawing.Size(111, 32);
            this.btnAddToCart.TabIndex = 156;
            this.btnAddToCart.Text = "Tambahkan ke List";
            this.btnAddToCart.UseVisualStyleBackColor = true;
            this.btnAddToCart.Click += new System.EventHandler(this.btnAddToCart_Click_1);
            // 
            // txtKodePenerimaanProduk
            // 
            this.txtKodePenerimaanProduk.Enabled = false;
            this.txtKodePenerimaanProduk.Location = new System.Drawing.Point(201, 26);
            this.txtKodePenerimaanProduk.Name = "txtKodePenerimaanProduk";
            this.txtKodePenerimaanProduk.ReadOnly = true;
            this.txtKodePenerimaanProduk.Size = new System.Drawing.Size(153, 20);
            this.txtKodePenerimaanProduk.TabIndex = 71;
            // 
            // txtKodeProduk
            // 
            this.txtKodeProduk.Enabled = false;
            this.txtKodeProduk.Location = new System.Drawing.Point(201, 52);
            this.txtKodeProduk.Name = "txtKodeProduk";
            this.txtKodeProduk.ReadOnly = true;
            this.txtKodeProduk.Size = new System.Drawing.Size(153, 20);
            this.txtKodeProduk.TabIndex = 67;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(156, 13);
            this.label5.TabIndex = 51;
            this.label5.Text = "Kode Surat Penerimaan Produk";
            // 
            // txtJumlahDiterima
            // 
            this.txtJumlahDiterima.Location = new System.Drawing.Point(201, 133);
            this.txtJumlahDiterima.Name = "txtJumlahDiterima";
            this.txtJumlahDiterima.Size = new System.Drawing.Size(45, 20);
            this.txtJumlahDiterima.TabIndex = 26;
            this.txtJumlahDiterima.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtJumlahDiterima_KeyPress_1);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 136);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 13);
            this.label7.TabIndex = 52;
            this.label7.Text = "Jumlah Diterima";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 52;
            this.label3.Text = "Kode Produk";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.groupBox2.Controls.Add(this.txtTanggalKeluar);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.txtKodePegawai);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.btnSearchPemasok);
            this.groupBox2.Controls.Add(this.txtKodeSrtPengeluaranBahanBaku);
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.txtJumlahKeluar);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.txtNamaBB);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(22, 146);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(720, 186);
            this.groupBox2.TabIndex = 159;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Bahan Baku";
            // 
            // txtTanggalKeluar
            // 
            this.txtTanggalKeluar.Enabled = false;
            this.txtTanggalKeluar.Location = new System.Drawing.Point(199, 144);
            this.txtTanggalKeluar.Name = "txtTanggalKeluar";
            this.txtTanggalKeluar.ReadOnly = true;
            this.txtTanggalKeluar.Size = new System.Drawing.Size(153, 20);
            this.txtTanggalKeluar.TabIndex = 125;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(11, 146);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 13);
            this.label8.TabIndex = 126;
            this.label8.Text = "Tanggal Keluar";
            // 
            // txtKodePegawai
            // 
            this.txtKodePegawai.Enabled = false;
            this.txtKodePegawai.Location = new System.Drawing.Point(199, 95);
            this.txtKodePegawai.Name = "txtKodePegawai";
            this.txtKodePegawai.ReadOnly = true;
            this.txtKodePegawai.Size = new System.Drawing.Size(153, 20);
            this.txtKodePegawai.TabIndex = 123;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 98);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 13);
            this.label4.TabIndex = 124;
            this.label4.Text = "Kode Pegawai";
            // 
            // btnSearchPemasok
            // 
            this.btnSearchPemasok.BackgroundImage = global::PT_Union_Ceramics_Utama.Properties.Resources.search;
            this.btnSearchPemasok.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSearchPemasok.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearchPemasok.Location = new System.Drawing.Point(331, 44);
            this.btnSearchPemasok.Name = "btnSearchPemasok";
            this.btnSearchPemasok.Size = new System.Drawing.Size(21, 21);
            this.btnSearchPemasok.TabIndex = 122;
            this.btnSearchPemasok.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnSearchPemasok.UseVisualStyleBackColor = true;
            this.btnSearchPemasok.Click += new System.EventHandler(this.btnSearchPemasok_Click);
            // 
            // txtKodeSrtPengeluaranBahanBaku
            // 
            this.txtKodeSrtPengeluaranBahanBaku.Location = new System.Drawing.Point(199, 43);
            this.txtKodeSrtPengeluaranBahanBaku.Name = "txtKodeSrtPengeluaranBahanBaku";
            this.txtKodeSrtPengeluaranBahanBaku.ReadOnly = true;
            this.txtKodeSrtPengeluaranBahanBaku.Size = new System.Drawing.Size(126, 20);
            this.txtKodeSrtPengeluaranBahanBaku.TabIndex = 121;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.SystemColors.ControlLight;
            this.groupBox4.Controls.Add(this.dataGridProduk);
            this.groupBox4.Location = new System.Drawing.Point(361, 24);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(339, 150);
            this.groupBox4.TabIndex = 120;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Produk";
            // 
            // dataGridProduk
            // 
            this.dataGridProduk.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridProduk.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridProduk.Location = new System.Drawing.Point(13, 19);
            this.dataGridProduk.Name = "dataGridProduk";
            this.dataGridProduk.RowTemplate.Height = 24;
            this.dataGridProduk.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridProduk.Size = new System.Drawing.Size(319, 116);
            this.dataGridProduk.TabIndex = 81;
            this.dataGridProduk.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridProduk_CellContentClick_1);
            // 
            // txtJumlahKeluar
            // 
            this.txtJumlahKeluar.Enabled = false;
            this.txtJumlahKeluar.Location = new System.Drawing.Point(199, 121);
            this.txtJumlahKeluar.Name = "txtJumlahKeluar";
            this.txtJumlahKeluar.ReadOnly = true;
            this.txtJumlahKeluar.Size = new System.Drawing.Size(41, 20);
            this.txtJumlahKeluar.TabIndex = 79;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 124);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 13);
            this.label10.TabIndex = 80;
            this.label10.Text = "JumlahKeluar";
            // 
            // txtNamaBB
            // 
            this.txtNamaBB.Enabled = false;
            this.txtNamaBB.Location = new System.Drawing.Point(199, 69);
            this.txtNamaBB.Name = "txtNamaBB";
            this.txtNamaBB.ReadOnly = true;
            this.txtNamaBB.Size = new System.Drawing.Size(153, 20);
            this.txtNamaBB.TabIndex = 71;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 13);
            this.label6.TabIndex = 73;
            this.label6.Text = "Nama Bahan Baku";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(185, 13);
            this.label2.TabIndex = 72;
            this.label2.Text = "Kode Surat Pengeluaran Bahan Baku";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.groupBox1.Controls.Add(this.dataGridPermintaan);
            this.groupBox1.Location = new System.Drawing.Point(389, 338);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(353, 204);
            this.groupBox1.TabIndex = 158;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "List Penerimaan Produk";
            // 
            // dataGridPermintaan
            // 
            this.dataGridPermintaan.AllowUserToAddRows = false;
            this.dataGridPermintaan.AllowUserToDeleteRows = false;
            this.dataGridPermintaan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridPermintaan.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.KodeProduk,
            this.NamaProduk,
            this.Harga,
            this.JumlahDiterima});
            this.dataGridPermintaan.Location = new System.Drawing.Point(15, 22);
            this.dataGridPermintaan.Name = "dataGridPermintaan";
            this.dataGridPermintaan.ReadOnly = true;
            this.dataGridPermintaan.RowTemplate.Height = 24;
            this.dataGridPermintaan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridPermintaan.Size = new System.Drawing.Size(317, 127);
            this.dataGridPermintaan.TabIndex = 111;
            this.dataGridPermintaan.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridPermintaan_CellContentClick);
            // 
            // KodeProduk
            // 
            this.KodeProduk.HeaderText = "KodeProduk";
            this.KodeProduk.Name = "KodeProduk";
            this.KodeProduk.ReadOnly = true;
            // 
            // NamaProduk
            // 
            this.NamaProduk.HeaderText = "NamaProduk";
            this.NamaProduk.Name = "NamaProduk";
            this.NamaProduk.ReadOnly = true;
            // 
            // Harga
            // 
            this.Harga.HeaderText = "Harga";
            this.Harga.Name = "Harga";
            this.Harga.ReadOnly = true;
            // 
            // JumlahDiterima
            // 
            this.JumlahDiterima.HeaderText = "JumlahDiterima";
            this.JumlahDiterima.Name = "JumlahDiterima";
            this.JumlahDiterima.ReadOnly = true;
            // 
            // btnReset
            // 
            this.btnReset.Image = global::PT_Union_Ceramics_Utama.Properties.Resources.undod;
            this.btnReset.Location = new System.Drawing.Point(673, 554);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(69, 28);
            this.btnReset.TabIndex = 161;
            this.btnReset.Text = "Reset";
            this.btnReset.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReset.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Visible = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // FormPenerimaanProduk
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(754, 605);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.panelButtonExecute);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "FormPenerimaanProduk";
            this.Text = "FormPenerimaanProduk";
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.groupBox2, 0);
            this.Controls.SetChildIndex(this.groupBox3, 0);
            this.Controls.SetChildIndex(this.panelButtonExecute, 0);
            this.Controls.SetChildIndex(this.btnReset, 0);
            ((System.ComponentModel.ISupportInitialize)(this._bs)).EndInit();
            this.panelButtonExecute.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridProduk)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPermintaan)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Panel panelButtonExecute;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnEmpty;
        private System.Windows.Forms.Button btnAddToCart;
        private System.Windows.Forms.TextBox txtKodePenerimaanProduk;
        private System.Windows.Forms.TextBox txtKodeProduk;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtJumlahDiterima;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnSearchPemasok;
        public System.Windows.Forms.TextBox txtKodeSrtPengeluaranBahanBaku;
        private System.Windows.Forms.GroupBox groupBox4;
        public System.Windows.Forms.DataGridView dataGridProduk;
        public System.Windows.Forms.TextBox txtJumlahKeluar;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.TextBox txtNamaBB;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridPermintaan;
        private System.Windows.Forms.TextBox txtNamaProduk;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtKodePegawai;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtTanggalKeluar;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtHarga;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridViewTextBoxColumn KodeProduk;
        private System.Windows.Forms.DataGridViewTextBoxColumn NamaProduk;
        private System.Windows.Forms.DataGridViewTextBoxColumn Harga;
        private System.Windows.Forms.DataGridViewTextBoxColumn JumlahDiterima;
    }
}