﻿namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    partial class FormPemesananProduk
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.kdProduk = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.namaProduk = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.harga = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jumlahDipesan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtNamaProduk = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtJumlah = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtKodePemesanan = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNamaPelanggan = new System.Windows.Forms.TextBox();
            this.panelAgen = new System.Windows.Forms.GroupBox();
            this.btnSearchPelanggan = new System.Windows.Forms.Button();
            this.txtKodePelanggan = new System.Windows.Forms.TextBox();
            this.txtTelepon = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtAlamat = new System.Windows.Forms.RichTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.panelProduk = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSearchProduk = new System.Windows.Forms.Button();
            this.txtKodeProduk = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtStok = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtHarga = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnEmpty = new System.Windows.Forms.Button();
            this.btnAddToCart = new System.Windows.Forms.Button();
            this.panelButtonExecute = new System.Windows.Forms.Panel();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtKodePegawai = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this._bs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panelAgen.SuspendLayout();
            this.panelProduk.SuspendLayout();
            this.panelButtonExecute.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.kdProduk,
            this.namaProduk,
            this.harga,
            this.jumlahDipesan,
            this.subTotal});
            this.dataGridView1.Location = new System.Drawing.Point(15, 24);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(577, 151);
            this.dataGridView1.TabIndex = 111;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // kdProduk
            // 
            this.kdProduk.HeaderText = "Kode Produk";
            this.kdProduk.Name = "kdProduk";
            this.kdProduk.ReadOnly = true;
            // 
            // namaProduk
            // 
            this.namaProduk.HeaderText = "Nama Produk";
            this.namaProduk.Name = "namaProduk";
            this.namaProduk.ReadOnly = true;
            // 
            // harga
            // 
            this.harga.HeaderText = "Harga";
            this.harga.Name = "harga";
            this.harga.ReadOnly = true;
            // 
            // jumlahDipesan
            // 
            this.jumlahDipesan.HeaderText = "Jumlah Dipesan";
            this.jumlahDipesan.Name = "jumlahDipesan";
            this.jumlahDipesan.ReadOnly = true;
            // 
            // subTotal
            // 
            this.subTotal.HeaderText = "Sub Total";
            this.subTotal.Name = "subTotal";
            this.subTotal.ReadOnly = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 52;
            this.label3.Text = "Nama";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.groupBox1.Controls.Add(this.dataGridView1);
            this.groupBox1.Location = new System.Drawing.Point(11, 371);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(608, 190);
            this.groupBox1.TabIndex = 120;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "List Pemesanan Produk";
            // 
            // txtNamaProduk
            // 
            this.txtNamaProduk.Enabled = false;
            this.txtNamaProduk.Location = new System.Drawing.Point(139, 55);
            this.txtNamaProduk.Name = "txtNamaProduk";
            this.txtNamaProduk.ReadOnly = true;
            this.txtNamaProduk.Size = new System.Drawing.Size(146, 20);
            this.txtNamaProduk.TabIndex = 67;
            this.txtNamaProduk.Tag = "Produk.namaProduk";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 136);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 13);
            this.label7.TabIndex = 52;
            this.label7.Text = "Jumlah Dipesan";
            // 
            // txtJumlah
            // 
            this.txtJumlah.Location = new System.Drawing.Point(139, 133);
            this.txtJumlah.Name = "txtJumlah";
            this.txtJumlah.Size = new System.Drawing.Size(45, 20);
            this.txtJumlah.TabIndex = 26;
            this.txtJumlah.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtJumlah_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 13);
            this.label5.TabIndex = 51;
            this.label5.Text = "Kode Produk";
            // 
            // txtKodePemesanan
            // 
            this.txtKodePemesanan.Enabled = false;
            this.txtKodePemesanan.Location = new System.Drawing.Point(161, 110);
            this.txtKodePemesanan.Name = "txtKodePemesanan";
            this.txtKodePemesanan.ReadOnly = true;
            this.txtKodePemesanan.Size = new System.Drawing.Size(200, 20);
            this.txtKodePemesanan.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 113);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 63;
            this.label1.Text = "Kode Pemesanan";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 59);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 73;
            this.label6.Text = "Nama";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 72;
            this.label2.Text = "Kode Agen";
            // 
            // txtNamaPelanggan
            // 
            this.txtNamaPelanggan.Enabled = false;
            this.txtNamaPelanggan.Location = new System.Drawing.Point(119, 56);
            this.txtNamaPelanggan.Name = "txtNamaPelanggan";
            this.txtNamaPelanggan.ReadOnly = true;
            this.txtNamaPelanggan.Size = new System.Drawing.Size(146, 20);
            this.txtNamaPelanggan.TabIndex = 71;
            this.txtNamaPelanggan.Tag = "Pelanggan.nama";
            // 
            // panelAgen
            // 
            this.panelAgen.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panelAgen.Controls.Add(this.btnSearchPelanggan);
            this.panelAgen.Controls.Add(this.txtKodePelanggan);
            this.panelAgen.Controls.Add(this.txtTelepon);
            this.panelAgen.Controls.Add(this.label10);
            this.panelAgen.Controls.Add(this.txtAlamat);
            this.panelAgen.Controls.Add(this.label9);
            this.panelAgen.Controls.Add(this.txtNamaPelanggan);
            this.panelAgen.Controls.Add(this.label2);
            this.panelAgen.Controls.Add(this.label6);
            this.panelAgen.Location = new System.Drawing.Point(15, 163);
            this.panelAgen.Name = "panelAgen";
            this.panelAgen.Size = new System.Drawing.Size(280, 198);
            this.panelAgen.TabIndex = 121;
            this.panelAgen.TabStop = false;
            this.panelAgen.Text = "Agen";
            // 
            // btnSearchPelanggan
            // 
            this.btnSearchPelanggan.BackgroundImage = global::PT_Union_Ceramics_Utama.Properties.Resources.search;
            this.btnSearchPelanggan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSearchPelanggan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearchPelanggan.Location = new System.Drawing.Point(244, 29);
            this.btnSearchPelanggan.Name = "btnSearchPelanggan";
            this.btnSearchPelanggan.Size = new System.Drawing.Size(21, 21);
            this.btnSearchPelanggan.TabIndex = 123;
            this.btnSearchPelanggan.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnSearchPelanggan.UseVisualStyleBackColor = true;
            this.btnSearchPelanggan.Click += new System.EventHandler(this.btnSearchPelanggan_Click);
            // 
            // txtKodePelanggan
            // 
            this.txtKodePelanggan.Enabled = false;
            this.txtKodePelanggan.Location = new System.Drawing.Point(119, 30);
            this.txtKodePelanggan.Name = "txtKodePelanggan";
            this.txtKodePelanggan.Size = new System.Drawing.Size(119, 20);
            this.txtKodePelanggan.TabIndex = 81;
            this.txtKodePelanggan.Tag = "Pelanggan.kdPelanggan";
            // 
            // txtTelepon
            // 
            this.txtTelepon.Enabled = false;
            this.txtTelepon.Location = new System.Drawing.Point(119, 164);
            this.txtTelepon.Name = "txtTelepon";
            this.txtTelepon.ReadOnly = true;
            this.txtTelepon.Size = new System.Drawing.Size(146, 20);
            this.txtTelepon.TabIndex = 79;
            this.txtTelepon.Tag = "Pelanggan.noTelepon";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(14, 167);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 13);
            this.label10.TabIndex = 80;
            this.label10.Text = "No. Telp";
            // 
            // txtAlamat
            // 
            this.txtAlamat.Enabled = false;
            this.txtAlamat.Location = new System.Drawing.Point(119, 82);
            this.txtAlamat.Name = "txtAlamat";
            this.txtAlamat.ReadOnly = true;
            this.txtAlamat.Size = new System.Drawing.Size(146, 76);
            this.txtAlamat.TabIndex = 78;
            this.txtAlamat.Tag = "Pelanggan.alamat";
            this.txtAlamat.Text = "";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 85);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(39, 13);
            this.label9.TabIndex = 77;
            this.label9.Text = "Alamat";
            // 
            // panelProduk
            // 
            this.panelProduk.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panelProduk.Controls.Add(this.panel1);
            this.panelProduk.Controls.Add(this.btnSearchProduk);
            this.panelProduk.Controls.Add(this.txtKodeProduk);
            this.panelProduk.Controls.Add(this.label13);
            this.panelProduk.Controls.Add(this.txtStok);
            this.panelProduk.Controls.Add(this.label12);
            this.panelProduk.Controls.Add(this.txtHarga);
            this.panelProduk.Controls.Add(this.label11);
            this.panelProduk.Controls.Add(this.txtNamaProduk);
            this.panelProduk.Controls.Add(this.label5);
            this.panelProduk.Controls.Add(this.label3);
            this.panelProduk.Controls.Add(this.label7);
            this.panelProduk.Controls.Add(this.txtJumlah);
            this.panelProduk.Location = new System.Drawing.Point(309, 163);
            this.panelProduk.Name = "panelProduk";
            this.panelProduk.Size = new System.Drawing.Size(310, 169);
            this.panelProduk.TabIndex = 124;
            this.panelProduk.TabStop = false;
            this.panelProduk.Text = "Produk";
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 201);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(310, 40);
            this.panel1.TabIndex = 156;
            // 
            // btnSearchProduk
            // 
            this.btnSearchProduk.BackgroundImage = global::PT_Union_Ceramics_Utama.Properties.Resources.search;
            this.btnSearchProduk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSearchProduk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearchProduk.Location = new System.Drawing.Point(264, 28);
            this.btnSearchProduk.Name = "btnSearchProduk";
            this.btnSearchProduk.Size = new System.Drawing.Size(21, 21);
            this.btnSearchProduk.TabIndex = 127;
            this.btnSearchProduk.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnSearchProduk.UseVisualStyleBackColor = true;
            this.btnSearchProduk.Click += new System.EventHandler(this.btnSearchProduk_Click);
            // 
            // txtKodeProduk
            // 
            this.txtKodeProduk.Enabled = false;
            this.txtKodeProduk.Location = new System.Drawing.Point(139, 29);
            this.txtKodeProduk.Name = "txtKodeProduk";
            this.txtKodeProduk.Size = new System.Drawing.Size(119, 20);
            this.txtKodeProduk.TabIndex = 126;
            this.txtKodeProduk.Tag = "Produk.idProduk";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 110);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 13);
            this.label13.TabIndex = 124;
            this.label13.Text = "Stok";
            // 
            // txtStok
            // 
            this.txtStok.Enabled = false;
            this.txtStok.Location = new System.Drawing.Point(139, 107);
            this.txtStok.Name = "txtStok";
            this.txtStok.ReadOnly = true;
            this.txtStok.Size = new System.Drawing.Size(45, 20);
            this.txtStok.TabIndex = 123;
            this.txtStok.Tag = "Produk.jumlah";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(136, 84);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(27, 13);
            this.label12.TabIndex = 122;
            this.label12.Text = "Rp. ";
            // 
            // txtHarga
            // 
            this.txtHarga.Enabled = false;
            this.txtHarga.Location = new System.Drawing.Point(169, 81);
            this.txtHarga.Name = "txtHarga";
            this.txtHarga.ReadOnly = true;
            this.txtHarga.Size = new System.Drawing.Size(116, 20);
            this.txtHarga.TabIndex = 121;
            this.txtHarga.Tag = "Produk.harga";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 84);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(36, 13);
            this.label11.TabIndex = 120;
            this.label11.Text = "Harga";
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(440, 338);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(61, 23);
            this.btnDelete.TabIndex = 155;
            this.btnDelete.Text = "Hapus";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEmpty
            // 
            this.btnEmpty.Location = new System.Drawing.Point(509, 338);
            this.btnEmpty.Name = "btnEmpty";
            this.btnEmpty.Size = new System.Drawing.Size(94, 23);
            this.btnEmpty.TabIndex = 154;
            this.btnEmpty.Text = "Kosongkan List";
            this.btnEmpty.UseVisualStyleBackColor = true;
            this.btnEmpty.Click += new System.EventHandler(this.btnEmpty_Click);
            // 
            // btnAddToCart
            // 
            this.btnAddToCart.Location = new System.Drawing.Point(325, 338);
            this.btnAddToCart.Name = "btnAddToCart";
            this.btnAddToCart.Size = new System.Drawing.Size(109, 23);
            this.btnAddToCart.TabIndex = 153;
            this.btnAddToCart.Text = "Tambahkan ke List";
            this.btnAddToCart.UseVisualStyleBackColor = true;
            this.btnAddToCart.Click += new System.EventHandler(this.btnAddToCart_Click);
            // 
            // panelButtonExecute
            // 
            this.panelButtonExecute.Controls.Add(this.btnPrint);
            this.panelButtonExecute.Controls.Add(this.btnSave);
            this.panelButtonExecute.Location = new System.Drawing.Point(227, 562);
            this.panelButtonExecute.Name = "panelButtonExecute";
            this.panelButtonExecute.Size = new System.Drawing.Size(184, 37);
            this.panelButtonExecute.TabIndex = 156;
            // 
            // btnPrint
            // 
            this.btnPrint.Image = global::PT_Union_Ceramics_Utama.Properties.Resources.agt_print;
            this.btnPrint.Location = new System.Drawing.Point(3, 6);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 28);
            this.btnPrint.TabIndex = 155;
            this.btnPrint.Text = "Cetak";
            this.btnPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Visible = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnSave
            // 
            this.btnSave.Image = global::PT_Union_Ceramics_Utama.Properties.Resources.save;
            this.btnSave.Location = new System.Drawing.Point(100, 6);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 28);
            this.btnSave.TabIndex = 154;
            this.btnSave.Text = "Simpan";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnReset
            // 
            this.btnReset.Image = global::PT_Union_Ceramics_Utama.Properties.Resources.undod;
            this.btnReset.Location = new System.Drawing.Point(550, 568);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(69, 28);
            this.btnReset.TabIndex = 153;
            this.btnReset.Text = "Reset";
            this.btnReset.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReset.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Visible = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 136);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(123, 13);
            this.label4.TabIndex = 160;
            this.label4.Text = "Kode Pegawai Pemesan";
            // 
            // txtKodePegawai
            // 
            this.txtKodePegawai.Enabled = false;
            this.txtKodePegawai.Location = new System.Drawing.Point(161, 136);
            this.txtKodePegawai.Name = "txtKodePegawai";
            this.txtKodePegawai.ReadOnly = true;
            this.txtKodePegawai.Size = new System.Drawing.Size(200, 20);
            this.txtKodePegawai.TabIndex = 159;
            // 
            // FormPemesananProduk
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(635, 600);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtKodePegawai);
            this.Controls.Add(this.panelButtonExecute);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnEmpty);
            this.Controls.Add(this.btnAddToCart);
            this.Controls.Add(this.panelProduk);
            this.Controls.Add(this.panelAgen);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtKodePemesanan);
            this.Controls.Add(this.label1);
            this.Name = "FormPemesananProduk";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form Pemesanan Produk";
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.txtKodePemesanan, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.panelAgen, 0);
            this.Controls.SetChildIndex(this.panelProduk, 0);
            this.Controls.SetChildIndex(this.btnAddToCart, 0);
            this.Controls.SetChildIndex(this.btnEmpty, 0);
            this.Controls.SetChildIndex(this.btnDelete, 0);
            this.Controls.SetChildIndex(this.panelButtonExecute, 0);
            this.Controls.SetChildIndex(this.txtKodePegawai, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.btnReset, 0);
            ((System.ComponentModel.ISupportInitialize)(this._bs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.panelAgen.ResumeLayout(false);
            this.panelAgen.PerformLayout();
            this.panelProduk.ResumeLayout(false);
            this.panelProduk.PerformLayout();
            this.panelButtonExecute.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtKodePemesanan;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox panelAgen;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox panelProduk;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnSearchPelanggan;
        private System.Windows.Forms.Button btnSearchProduk;
        public System.Windows.Forms.TextBox txtNamaProduk;
        public System.Windows.Forms.TextBox txtNamaPelanggan;
        public System.Windows.Forms.TextBox txtTelepon;
        public System.Windows.Forms.RichTextBox txtAlamat;
        public System.Windows.Forms.TextBox txtHarga;
        public System.Windows.Forms.TextBox txtStok;
        public System.Windows.Forms.TextBox txtKodePelanggan;
        public System.Windows.Forms.TextBox txtKodeProduk;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnEmpty;
        private System.Windows.Forms.Button btnAddToCart;
        private System.Windows.Forms.Panel panelButtonExecute;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.DataGridViewTextBoxColumn kdProduk;
        private System.Windows.Forms.DataGridViewTextBoxColumn namaProduk;
        private System.Windows.Forms.DataGridViewTextBoxColumn harga;
        private System.Windows.Forms.DataGridViewTextBoxColumn jumlahDipesan;
        private System.Windows.Forms.DataGridViewTextBoxColumn subTotal;
        public System.Windows.Forms.TextBox txtJumlah;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtKodePegawai;

    }
}