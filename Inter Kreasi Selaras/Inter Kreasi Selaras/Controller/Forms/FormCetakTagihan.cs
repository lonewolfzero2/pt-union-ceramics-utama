﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using PT_Union_Ceramics_Utama.Controller.Helpers;
using PT_Union_Ceramics_Utama.Controller.Report;

namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    public partial class FormCetakTagihan : Form
    {
        private readonly String _kode;
        private readonly String _pajak;
        private readonly String _gt = "";

        public FormCetakTagihan(String kode,String pajak,String grandtotal)
        {
            InitializeComponent();
            _kode = kode;
            _pajak = pajak;
            _gt = grandtotal;

            var conn = DatabaseHandler.Instance.SqlConnection;

            string tanggal = DateTime.Today.Date.ToShortDateString();
            String p = "SELECT kodetagihan='" + _kode + "',pajak ='" + _pajak + "',tanggalcetak='" + tanggal + "',grandtotal = '" + _gt + "',l.kdPelanggan AS kodepelanggan,l.nama AS namapelanggan,l.alamat AS alamatpelanggan,l.noTelepon AS notelponpelanggan,p.idProduk AS kodeproduk,p.namaProduk AS namaproduk,d.jumlahDipesan AS jumlahpesan,p.harga AS harga,d.subtotal AS subtotal FROM Tagihan t JOIN SuratJalan s ON t.idSuratJalan = s.idSuratJalan JOIN SuratPemesananProduk sp ON s.idOrder = sp.idOrder JOIN DetailSuratPemesananProduk d ON d.idOrder = sp.idOrder JOIN Agen l ON sp.idAgen = l.kdPelanggan JOIN Produk p ON p.idProduk = d.idProduk WHERE t.idTagihan ='" + _kode + "'";
            SqlDataAdapter data1 = new SqlDataAdapter(p, conn);

            DataSet1 ds = new DataSet1();
            data1.Fill(ds, "Tagihan");

            ReportTagihan cr = new ReportTagihan();
            cr.SetDataSource(ds);
            crystalReportViewer1.ReportSource = cr;
            conn.Close();
        }
    }
}
