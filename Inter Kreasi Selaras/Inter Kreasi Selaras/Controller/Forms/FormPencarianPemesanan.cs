﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    public partial class FormPencarianPemesanan : GridForm
    {
        private FormSuratJalan _fsj;
        private string _kodePemesanan = "", _tanggal = "", _kdPegawai = "", _kdPelanggan = "";
        public FormSuratJalan MyFormSuratJalan { get { return _fsj; } set { _fsj = value; } }

        public FormPencarianPemesanan(FormSuratJalan formSuratJalan, Form parent) : base(parent)
        {
            InitializeComponent();
            cmbSearch.SelectedIndex = 0;

            MyFormSuratJalan = formSuratJalan;
        }

        public override void Initiate()
        {
            _defaultDataGridView = dataGridView1;
            base.Initiate();
        }

        protected override object _data
        {
            get
            {
                return (from q in _dataContext.SuratPemesananProduks
                        select new { q.idOrder, q.tanggalOrder, q.idPegawai, q.idAgen }).ToList();
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtKataKunci.Text))
            {
                MessageBox.Show(@"Isi kata kuncinya dulu", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                try
                {
                    if (cmbSearch.SelectedText.Equals("Kode Surat Pemesanan Produk"))
                    {
                        var dataKodePemesanan = (from q in _dataContext.SuratPemesananProduks
                                                 where q.idOrder == txtKataKunci.Text
                                                 select q).First();
                        UpdateGridView(dataKodePemesanan);
                    }
                    else
                    {
                        var dataNamaPelanggan = (from q in _dataContext.SuratPemesananProduks
                                                 where q.idOrder == txtKataKunci.Text
                                                 select q).First();
                        UpdateGridView(dataNamaPelanggan);
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show(@"Data tidak ditemukan", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void btnPilih_Click(object sender, EventArgs e)
        {
            if (_kodePemesanan.Equals(""))
            {
                MessageBox.Show(@"Pilih dulu datanya", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                var dataPelanggan = (from q in _dataContext.Agens
                                     where q.kdPelanggan == _kdPelanggan
                                     select new { q.nama, q.alamat }).First();

                var dataDetailPemesanan = (from q in _dataContext.DetailSuratPemesananProduks
                                           where q.idOrder == _kodePemesanan
                                           join p in _dataContext.Produks
                                           on q.idProduk equals p.idProduk
                                           select new { q.idProduk, p.namaProduk, p.harga, q.jumlahDipesan, q.subtotal }).ToList();

                _fsj.txtKodePemesananProduk.Text = _kodePemesanan;
                _fsj.txtKodePegawai.Text = _kdPegawai;
                _fsj.txtTanggalPemesanan.Text = _tanggal;
                _fsj.txtKodePelanggan.Text = _kdPelanggan;
                _fsj.txtNamaPelanggan.Text = dataPelanggan.nama;
                _fsj.txtAlamatPelanggan.Text = dataPelanggan.alamat;

                var dataPemesanan = (from q in _dataContext.SuratPemesananProduks
                                     where q.idOrder == _kodePemesanan
                                     select q.tanggalOrder).First();

                _fsj.TanggalPemesanan = dataPemesanan.ToString();
                _fsj.dataGridViewPemesanan.DataSource = dataDetailPemesanan;
                Dispose();
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            UpdateData(e.RowIndex);
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            UpdateData(e.RowIndex);
        }
        private void UpdateData(int rowIndex)
        {
            _kodePemesanan = dataGridView1.Rows[rowIndex].Cells[0].Value.ToString();
            _tanggal = dataGridView1.Rows[rowIndex].Cells[1].Value.ToString();
            _kdPegawai = dataGridView1.Rows[rowIndex].Cells[2].Value.ToString();
            _kdPelanggan = dataGridView1.Rows[rowIndex].Cells[3].Value.ToString();
        }

    }
}
