﻿namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    partial class FormKwitansi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.txtKodeKwitansi = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtTanggalJatuhTempo = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtKodeTagihan = new System.Windows.Forms.TextBox();
            this.txtNamaPelanggan = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtKodePelanggan = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtTotalTagihan = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtBunga = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtGrandTotal = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtKodePegawai = new System.Windows.Forms.TextBox();
            this.txtNamaPegawai = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panelButtonExecute = new System.Windows.Forms.Panel();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this._bs)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.panelButtonExecute.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtKodeKwitansi
            // 
            this.txtKodeKwitansi.Enabled = false;
            this.txtKodeKwitansi.Location = new System.Drawing.Point(160, 132);
            this.txtKodeKwitansi.Margin = new System.Windows.Forms.Padding(4);
            this.txtKodeKwitansi.Name = "txtKodeKwitansi";
            this.txtKodeKwitansi.ReadOnly = true;
            this.txtKodeKwitansi.Size = new System.Drawing.Size(265, 22);
            this.txtKodeKwitansi.TabIndex = 122;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 135);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 17);
            this.label1.TabIndex = 123;
            this.label1.Text = "Kode Kwitansi";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.groupBox1.Controls.Add(this.txtTanggalJatuhTempo);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.btnSearch);
            this.groupBox1.Controls.Add(this.txtKodeTagihan);
            this.groupBox1.Controls.Add(this.txtNamaPelanggan);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.txtKodePelanggan);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(16, 169);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(949, 242);
            this.groupBox1.TabIndex = 126;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tagihan";
            // 
            // txtTanggalJatuhTempo
            // 
            this.txtTanggalJatuhTempo.Enabled = false;
            this.txtTanggalJatuhTempo.Location = new System.Drawing.Point(168, 149);
            this.txtTanggalJatuhTempo.Margin = new System.Windows.Forms.Padding(4);
            this.txtTanggalJatuhTempo.Name = "txtTanggalJatuhTempo";
            this.txtTanggalJatuhTempo.ReadOnly = true;
            this.txtTanggalJatuhTempo.Size = new System.Drawing.Size(217, 22);
            this.txtTanggalJatuhTempo.TabIndex = 163;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 153);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(147, 17);
            this.label6.TabIndex = 162;
            this.label6.Text = "Tanggal Jatuh Tempo";
            // 
            // btnSearch
            // 
            this.btnSearch.BackgroundImage = global::PT_Union_Ceramics_Utama.Properties.Resources.search;
            this.btnSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSearch.Location = new System.Drawing.Point(359, 47);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(4);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(28, 26);
            this.btnSearch.TabIndex = 123;
            this.btnSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtKodeTagihan
            // 
            this.txtKodeTagihan.Enabled = false;
            this.txtKodeTagihan.Location = new System.Drawing.Point(168, 48);
            this.txtKodeTagihan.Margin = new System.Windows.Forms.Padding(4);
            this.txtKodeTagihan.Name = "txtKodeTagihan";
            this.txtKodeTagihan.Size = new System.Drawing.Size(181, 22);
            this.txtKodeTagihan.TabIndex = 16;
            // 
            // txtNamaPelanggan
            // 
            this.txtNamaPelanggan.Enabled = false;
            this.txtNamaPelanggan.Location = new System.Drawing.Point(168, 114);
            this.txtNamaPelanggan.Margin = new System.Windows.Forms.Padding(4);
            this.txtNamaPelanggan.Name = "txtNamaPelanggan";
            this.txtNamaPelanggan.ReadOnly = true;
            this.txtNamaPelanggan.Size = new System.Drawing.Size(217, 22);
            this.txtNamaPelanggan.TabIndex = 13;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 118);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(117, 17);
            this.label11.TabIndex = 12;
            this.label11.Text = "Nama Pelanggan";
            // 
            // txtKodePelanggan
            // 
            this.txtKodePelanggan.Enabled = false;
            this.txtKodePelanggan.Location = new System.Drawing.Point(168, 81);
            this.txtKodePelanggan.Margin = new System.Windows.Forms.Padding(4);
            this.txtKodePelanggan.Name = "txtKodePelanggan";
            this.txtKodePelanggan.ReadOnly = true;
            this.txtKodePelanggan.Size = new System.Drawing.Size(217, 22);
            this.txtKodePelanggan.TabIndex = 11;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(13, 85);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(113, 17);
            this.label10.TabIndex = 10;
            this.label10.Text = "Kode Pelanggan";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtTotalTagihan);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.dataGridView1);
            this.groupBox2.Location = new System.Drawing.Point(401, 20);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(529, 203);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Detail Tagihan";
            // 
            // txtTotalTagihan
            // 
            this.txtTotalTagihan.Enabled = false;
            this.txtTotalTagihan.Location = new System.Drawing.Point(324, 171);
            this.txtTotalTagihan.Margin = new System.Windows.Forms.Padding(4);
            this.txtTotalTagihan.Name = "txtTotalTagihan";
            this.txtTotalTagihan.ReadOnly = true;
            this.txtTotalTagihan.Size = new System.Drawing.Size(183, 22);
            this.txtTotalTagihan.TabIndex = 162;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(159, 175);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(96, 17);
            this.label14.TabIndex = 161;
            this.label14.Text = "Total Tagihan";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(284, 175);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 17);
            this.label2.TabIndex = 160;
            this.label2.Text = "Rp.";
            // 
            // dataGridView1
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Location = new System.Drawing.Point(20, 23);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(488, 127);
            this.dataGridView1.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 52);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Kode Tagihan";
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.SystemColors.ControlLight;
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.txtBunga);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Controls.Add(this.txtGrandTotal);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.txtKodePegawai);
            this.groupBox5.Controls.Add(this.txtNamaPegawai);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Location = new System.Drawing.Point(20, 420);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox5.Size = new System.Drawing.Size(837, 112);
            this.groupBox5.TabIndex = 169;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Informasi Pembayaran";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(793, 34);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(20, 17);
            this.label13.TabIndex = 176;
            this.label13.Text = "%";
            // 
            // txtBunga
            // 
            this.txtBunga.Enabled = false;
            this.txtBunga.Location = new System.Drawing.Point(596, 31);
            this.txtBunga.Margin = new System.Windows.Forms.Padding(4);
            this.txtBunga.Name = "txtBunga";
            this.txtBunga.Size = new System.Drawing.Size(188, 22);
            this.txtBunga.TabIndex = 175;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(465, 34);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 17);
            this.label4.TabIndex = 174;
            this.label4.Text = "Bunga Telat";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(592, 66);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(30, 17);
            this.label12.TabIndex = 173;
            this.label12.Text = "Rp.";
            // 
            // txtGrandTotal
            // 
            this.txtGrandTotal.Enabled = false;
            this.txtGrandTotal.Location = new System.Drawing.Point(629, 63);
            this.txtGrandTotal.Margin = new System.Windows.Forms.Padding(4);
            this.txtGrandTotal.Name = "txtGrandTotal";
            this.txtGrandTotal.ReadOnly = true;
            this.txtGrandTotal.Size = new System.Drawing.Size(183, 22);
            this.txtGrandTotal.TabIndex = 172;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(465, 68);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(84, 17);
            this.label15.TabIndex = 171;
            this.label15.Text = "Grand Total";
            // 
            // txtKodePegawai
            // 
            this.txtKodePegawai.Enabled = false;
            this.txtKodePegawai.Location = new System.Drawing.Point(165, 30);
            this.txtKodePegawai.Margin = new System.Windows.Forms.Padding(4);
            this.txtKodePegawai.Name = "txtKodePegawai";
            this.txtKodePegawai.Size = new System.Drawing.Size(213, 22);
            this.txtKodePegawai.TabIndex = 169;
            // 
            // txtNamaPegawai
            // 
            this.txtNamaPegawai.Enabled = false;
            this.txtNamaPegawai.Location = new System.Drawing.Point(165, 63);
            this.txtNamaPegawai.Margin = new System.Windows.Forms.Padding(4);
            this.txtNamaPegawai.Name = "txtNamaPegawai";
            this.txtNamaPegawai.ReadOnly = true;
            this.txtNamaPegawai.Size = new System.Drawing.Size(213, 22);
            this.txtNamaPegawai.TabIndex = 166;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 33);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 17);
            this.label5.TabIndex = 167;
            this.label5.Text = "Kode Pegawai";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 65);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(102, 17);
            this.label7.TabIndex = 168;
            this.label7.Text = "Nama Pegawai";
            // 
            // panelButtonExecute
            // 
            this.panelButtonExecute.Controls.Add(this.btnPrint);
            this.panelButtonExecute.Controls.Add(this.btnSave);
            this.panelButtonExecute.Controls.Add(this.btnReset);
            this.panelButtonExecute.Enabled = false;
            this.panelButtonExecute.Location = new System.Drawing.Point(335, 539);
            this.panelButtonExecute.Margin = new System.Windows.Forms.Padding(4);
            this.panelButtonExecute.Name = "panelButtonExecute";
            this.panelButtonExecute.Size = new System.Drawing.Size(309, 46);
            this.panelButtonExecute.TabIndex = 174;
            // 
            // btnPrint
            // 
            this.btnPrint.Image = global::PT_Union_Ceramics_Utama.Properties.Resources.agt_print;
            this.btnPrint.Location = new System.Drawing.Point(45, 4);
            this.btnPrint.Margin = new System.Windows.Forms.Padding(4);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(100, 34);
            this.btnPrint.TabIndex = 155;
            this.btnPrint.Text = "Cetak";
            this.btnPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click_1);
            // 
            // btnSave
            // 
            this.btnSave.Image = global::PT_Union_Ceramics_Utama.Properties.Resources.save;
            this.btnSave.Location = new System.Drawing.Point(168, 4);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 34);
            this.btnSave.TabIndex = 154;
            this.btnSave.Text = "Simpan";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnReset
            // 
            this.btnReset.Image = global::PT_Union_Ceramics_Utama.Properties.Resources.undod;
            this.btnReset.Location = new System.Drawing.Point(317, 4);
            this.btnReset.Margin = new System.Windows.Forms.Padding(4);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(92, 34);
            this.btnReset.TabIndex = 153;
            this.btnReset.Text = "Reset";
            this.btnReset.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReset.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Visible = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // FormKwitansi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 586);
            this.Controls.Add(this.panelButtonExecute);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtKodeKwitansi);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormKwitansi";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form Kwitansi";
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.txtKodeKwitansi, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.groupBox5, 0);
            this.Controls.SetChildIndex(this.panelButtonExecute, 0);
            ((System.ComponentModel.ISupportInitialize)(this._bs)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.panelButtonExecute.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtKodeKwitansi;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.GroupBox groupBox5;
        public System.Windows.Forms.TextBox txtKodePegawai;
        public System.Windows.Forms.TextBox txtNamaPegawai;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox txtKodeTagihan;
        public System.Windows.Forms.TextBox txtNamaPelanggan;
        public System.Windows.Forms.TextBox txtKodePelanggan;
        public System.Windows.Forms.DataGridView dataGridView1;
        public System.Windows.Forms.TextBox txtTotalTagihan;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox txtTanggalJatuhTempo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnReset;
        public System.Windows.Forms.TextBox txtBunga;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtGrandTotal;
        private System.Windows.Forms.Label label13;
        public System.Windows.Forms.Panel panelButtonExecute;
    }
}