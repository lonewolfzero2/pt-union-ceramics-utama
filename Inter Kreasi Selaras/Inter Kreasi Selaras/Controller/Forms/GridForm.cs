﻿using System.Windows.Forms;
using PT_Union_Ceramics_Utama.Controller.Core;
using PT_Union_Ceramics_Utama.Controller.Helpers;

namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    public class GridForm : BaseLogoForm
    {
        protected bool _isDataGridClicked;
        protected TextBoxCollection _textBoxCollection;

        public GridForm() : this(null) { }

        protected GridForm(Form parent) : base(parent)
        {
        }

        private int _reloadMode = 0;
        protected virtual object _data
        {
            get { return null; }
        }
        protected override void RefreshForm()
        {
            base.RefreshForm();

            if (_reloadMode == 0)
                UpdateGridView(_data);
        }

        public override void Initiate()
        {
            base.Initiate();
        }

        protected virtual void OnDataGridClicked(int rowIndex)
        {
            _textBoxCollection.UpdateTextBoxValue(rowIndex);
            _isDataGridClicked = true;
        }
    }
}
