﻿using System;
using System.Linq;
using System.Windows.Forms;
using PT_Union_Ceramics_Utama.Controller.Helpers;
using PT_Union_Ceramics_Utama.Controller.Helpers.Enums;

namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    public partial class FormBahanBakuGudang : GridForm
    {
        private DmlModes _dmlMode;
        
        public FormBahanBakuGudang(Form parent) : base(parent)
        {
            InitializeComponent();
        }

        public override void Initiate()
        {
            _defaultDataGridView = dataGridView1;
            _textBoxCollection = new TextBoxCollection(dataGridView1, panelBahanBaku);
            cmbSearch.SelectedIndex = 1;
            base.Initiate();
        }

        protected override object _data
        {
            get { return from a in _dataContext.BahanBakus select new {a.idBahanBaku, a.namaBahanBaku, a.Berat}; }
        }

        protected override void EnablePanel(bool status)
        {
            dataGridView1.Enabled = status;
            panelBahanBaku.Enabled = !status;
            panelPencarian.Enabled = status;
            btnRefresh.Enabled = status;
            txtKodeBahanBaku.Enabled = _dmlMode == DmlModes.Memasukan;
        }
        
        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtKataKunci.Text))
            {
                MessageBox.Show(@"Isi kata kuncinya dulu", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (cmbSearch.SelectedItem.ToString().Equals("Kode Bahan Baku"))
                {
                    try
                    {
                        var dataKodeProduk = (from q in _dataContext.BahanBakus
                                              where q.idBahanBaku.Contains(txtKataKunci.Text)
                                              select q).First();
                        UpdateGridView(dataKodeProduk);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show(@"Data tidak ditemukan", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else if (cmbSearch.SelectedItem.ToString().Equals("Nama Bahan Baku"))
                {
                    try
                    {
                        var dataNamaProduk = (from q in _dataContext.BahanBakus
                                              where q.namaBahanBaku.Contains(txtKataKunci.Text)
                                              select q).ToList();
                        UpdateGridView(dataNamaProduk);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show(@"Data tidak ditemukan", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    txtKataKunci.ReadOnly = true;
                    try
                    {
                        var dataKodeProduk = (from q in _dataContext.BahanBakus
                                              where q.Berat < 100
                                              select q).First();
                        UpdateGridView(dataKodeProduk);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show(@"Data tidak ditemukan", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            RefreshForm();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            OnDataGridClicked(e.RowIndex);
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            OnDataGridClicked(e.RowIndex);
        }
    }
}
