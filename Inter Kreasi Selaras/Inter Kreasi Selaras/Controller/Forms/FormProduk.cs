﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using PT_Union_Ceramics_Utama.Controller.Helpers;
using PT_Union_Ceramics_Utama.Controller.Helpers.Enums;

namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    public partial class FormProduk : GridForm
    {
        private DmlModes _dmlMode;

        public FormProduk(Form parent) : base(parent)
        {
            InitializeComponent();
            cmbSearch.SelectedIndex = 1;
        }

        protected override object _data
        {
            get
            {
                return from a in _dataContext.Produks
                       select new {a.idProduk, a.namaProduk, a.harga, a.jumlah};
            }
        }

        public override void Initiate()
        {
            _defaultDataGridView = dataGridView1;
            _textBoxCollection = new TextBoxCollection(dataGridView1, panelProduk);
            base.Initiate();
        }

        protected override void EnablePanel(bool status)
        {
            dataGridView1.Enabled = status;
            panelButtonDML.Enabled = status;
            panelButtonExecute.Enabled = !status;
            panelProduk.Enabled = !status;
            panelPencarian.Enabled = status;
            btnRefresh.Enabled = status;

            //if (dmlMode == 1) txtKodeProduk.Enabled = true;
        }

        public bool CekKodeKategori(string newKodeKategori)
        {
            var result = false;
            if (newKodeKategori.Length == 8)
            {
                int resultInt;
                if (newKodeKategori.Substring(0, 5).Equals("DUKAT") && int.TryParse(newKodeKategori.Substring(5, 3), out resultInt))
                {
                    result = true;
                }
            }
            return result;
        }

        private void btnUbah_Click(object sender, EventArgs e)
        {
            if (_isDataGridClicked)
            {
                _dmlMode = DmlModes.Mengubah;
                EnablePanel(false);
                _isDataGridClicked = false;
            }
            else
            {
                MessageBox.Show(@"Pilih dulu datanya", @"Pengubahan Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnTambah_Click(object sender, EventArgs e)
        {
            _dmlMode = DmlModes.Memasukan;
            _textBoxCollection.ClearValues(DmlModes.Memasukan);
            EnablePanel(false);
            List<string> data = (from prod in _dataContext.Produks
                                 orderby prod.idProduk descending
                                 select prod.idProduk).Take(1).ToList();
            txtKodeProduk.Text = data.ToCode();
            txtKodeProduk.Enabled = false;
        }

        private void btnHapus_Click(object sender, EventArgs e)
        {
            if (_isDataGridClicked)
            {
                _dmlMode = DmlModes.Menghapus;
                _isDataGridClicked = false;

                Produk data = (from q in _dataContext.Produks
                               where q.idProduk == txtKodeProduk.Text
                               select q).First();
                DialogResult intReturnValue = MessageBox.Show(@"Apa Anda yakin ingin menghapus data ini?", @"Penghapusan Data", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (intReturnValue == DialogResult.Yes)
                {
                    _dataContext.Produks.DeleteOnSubmit(data);
                    _dataContext.SubmitChanges();
                    _textBoxCollection.Clear();
                    RefreshForm();
                    EnablePanel(true);
                    MessageBox.Show(@"Data berhasil di hapus", @"Penghapusan Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show(@"Pilih dulu datanya", @"Penghapusan Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            txtKataKunci.Text = "";
            txtKodeProduk.Text = "";
            txtNamaProduk.Text = "";
            txtHarga.Text = "";
            txtJumlah.Text = "";
            EnablePanel(false);
        }

        private void btnYa_Click(object sender, EventArgs e)
        {
            if (!_textBoxCollection.IsEmpty)
            {
                var produk = new Produk();
                if (_dmlMode != DmlModes.Menghapus)
                {
                    switch (_dmlMode)
                    {
                        case DmlModes.Memasukan:
                            {
                                produk.idProduk = txtKodeProduk.Text;
                                produk.namaProduk = txtNamaProduk.Text;
                                produk.jumlah = txtJumlah.Text.ParseToIntOrDefault(0);
                                produk.harga = txtHarga.Text.ParseToIntOrDefault(0);
                                _dataContext.Produks.InsertOnSubmit(produk);
                            }
                            break;
                        case DmlModes.Mengubah:
                            {
                                Produk data = (from q in _dataContext.Produks
                                               where q.idProduk == txtKodeProduk.Text
                                               select q).First();
                                data.namaProduk = txtNamaProduk.Text;
                                data.jumlah = txtJumlah.Text.ParseToIntOrDefault(0);
                                data.harga = txtHarga.Text.ParseToIntOrDefault(0);
                            }
                            break;
                    }

                    _dataContext.SubmitChanges();
                    _textBoxCollection.ClearValues(_dmlMode);
                    EnablePanel(true);
                }
                RefreshForm();
                var s =  (_dmlMode == DmlModes.Mengubah) ? "mengubah" : "memasukkan";
                MessageBox.Show(@"Berhasil " + s + @" data", @"Penyimpanan Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(@"Data tidak boleh kosong");
            }
        }

        private void txtHarga_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }

        private void txtJumlah_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }
        
        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtKataKunci.Text.Equals(""))
            {
                MessageBox.Show(@"Isi kata kuncinya dulu", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (cmbSearch.SelectedItem.ToString().Equals("Kode Produk"))
                {
                    try
                    {
                        List<Produk> dataKodeProduk = (from q in _dataContext.Produks
                                                 where q.idProduk.Contains(txtKataKunci.Text)
                                                 select q).ToList();
                        UpdateGridView(dataKodeProduk);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show(@"Data tidak ditemukan", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                else if (cmbSearch.SelectedItem.ToString().Equals("Nama Produk"))
                {
                    try
                    {
                        List<Produk> dataNamaProduk = (from q in _dataContext.Produks
                                                       where q.namaProduk.Contains(txtKataKunci.Text)
                                                       select q).ToList();
                        UpdateGridView(dataNamaProduk);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show(@"Data tidak ditemukan", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            RefreshForm();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            OnDataGridClicked(e.RowIndex);
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            OnDataGridClicked(e.RowIndex);
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void cmbSearch_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}