﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    public partial class FormPencarianPemasok : GridForm
    {
        private string _kode = "";
        private string _namaPerusahaan = "";
        private string _namaPic = "";
        private string _alamat = "";
        private readonly FormPermintaanBahanBaku _myFormPermintaanBahanBaku;

        public FormPencarianPemasok(FormPermintaanBahanBaku parent) : base(null)
        {
            InitializeComponent();
            _myFormPermintaanBahanBaku = parent;
            cmbSearch.SelectedIndex = 0;
        }

        public override void Initiate()
        {
            _defaultDataGridView = dataGridView1;
            base.Initiate();
        }

        protected override object _data
        {
            get
            {
                return from a in _dataContext.Suppliers
                       select new {a.idSupplier, a.namaPerusahaan, a.pic, a.noTelepon, a.noFax, a.email, a.kota, a.alamat};
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtKataKunci.Text))
            {
                MessageBox.Show(@"Isi kata kuncinya dulu", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (cmbSearch.SelectedIndex == 0)
                {
                    try
                    {
                        var dataKodePemasok = (from q in _dataContext.Suppliers
                                                 where q.idSupplier== txtKataKunci.Text
                                                 select q).First();
                        UpdateGridView(dataKodePemasok);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show(@"Data tidak ditemukan", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            RefreshForm();
        }

        private void btnPilih_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(_kode))
            {
                MessageBox.Show(@"Pilih dulu datanya", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                _myFormPermintaanBahanBaku.txtKodePemasok.Text = _kode;
                _myFormPermintaanBahanBaku.txtNamaPIC.Text = _namaPic;
                _myFormPermintaanBahanBaku.txtNamaPerusahaan.Text = _namaPerusahaan;
                _myFormPermintaanBahanBaku.txtAlamat.Text = _alamat;

                var produkPemasok = (from q in _dataContext.BahanBakus
                                     where q.idsupplier == _kode
                                     select q).ToList();
                UpdateGridView(produkPemasok, _myFormPermintaanBahanBaku.dataGridProduk);

                Dispose();
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView1_CellClick(sender, e);
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.CurrentRow != null)
            {
                _kode = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                _namaPerusahaan = dataGridView1.CurrentRow.Cells[1].Value.ToString();
                _namaPic = dataGridView1.CurrentRow.Cells[2].Value.ToString();
                _alamat = dataGridView1.CurrentRow.Cells[7].Value.ToString();
            }
            //_merek = dataGridView1.CurrentRow.Cells[9].Value.ToString();
        }
    }
}
