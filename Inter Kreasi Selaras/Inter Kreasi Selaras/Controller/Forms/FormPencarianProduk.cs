﻿using System;
using System.Linq;
using System.Windows.Forms;
using PT_Union_Ceramics_Utama.Controller.Core;

namespace PT_Union_Ceramics_Utama.Controller.Forms
{
    public partial class FormPencarianProduk : GridForm
    {
        private string _kode = "", _nama = "", _harga = "", _stok = "";
        private readonly FormPemesananProduk _myFormPemesananProduk;

        public FormPencarianProduk(FormPemesananProduk parent) : base(null) 
        {
            InitializeComponent();

            _myFormPemesananProduk = parent;
            cmbSearch.SelectedIndex = 0;
        }

        public override void Initiate()
        {
            _defaultDataGridView = dataGridView1;
            base.Initiate();
        }

        protected override object _data
        {
            get
            {
                return from a in _dataContext.Produks
                       select new { a.idProduk, a.namaProduk, a.harga, a.jumlah };
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            RefreshForm();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtKataKunci.Text))
            {
                MessageBox.Show(@"Isi kata kuncinya dulu", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (cmbSearch.SelectedItem.ToString().Equals("Kode Produk"))
                {
                    try
                    {
                        var dataKodeProduk = (from q in _dataContext.Produks
                                              where q.idProduk == txtKataKunci.Text
                                              select q).First();
                        UpdateGridView(dataKodeProduk);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show(@"Data tidak ditemukan", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                else if (cmbSearch.SelectedItem.ToString().Equals("Nama Produk"))
                {
                    try
                    {
                        var dataNamaProduk = (from q in _dataContext.Produks
                                              where q.namaProduk.Contains(txtKataKunci.Text)
                                              select q).ToList();
                        UpdateGridView(dataNamaProduk);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show(@"Data tidak ditemukan", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }

            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            _kode = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            _nama = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            _harga = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            _stok = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
        }

        private void btnPilih_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(_kode))
            {
                MessageBox.Show(@"Pilih dulu datanya", @"Pencarian Data", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                _myFormPemesananProduk.txtKodeProduk.Text = _kode;
                _myFormPemesananProduk.txtNamaProduk.Text = _nama;
                _myFormPemesananProduk.txtHarga.Text = _harga;
                _myFormPemesananProduk.txtStok.Text = _stok;
                _myFormPemesananProduk.txtJumlah.Text = "";
                Dispose();
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            _kode = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            _nama = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            _harga = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            _stok = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
        }
    }
}
