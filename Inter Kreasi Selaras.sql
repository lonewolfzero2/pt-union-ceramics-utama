--create database [Inter Kreasi Selaras]

use [Inter Kreasi Selaras]

--exec sp_MSforeachtable "DROP TABLE ? PRINT '? dropped' "

create table BahanBaku(
	kdBahanBaku nvarchar(10) primary key not null,
	namaBahanBaku nvarchar(50) not null,
	harga bigint,
	jumlah int,
	constraint cek10 check (kdBahanBaku like 'BHNKU[0-9][0-9][0-9]')
)

create table ProdukKategori(
	kdKategori nvarchar(10) primary key not null,
	namaKategori nvarchar(50) not null,
	constraint cek1 check([kdKategori] like 'DUKAT[0-9][0-9][0-9]'),
)

create table Produk(
	kdProduk nvarchar(10) primary key not null,
	namaProduk nvarchar(50) not null,
	kdKategori nvarchar(10) not null,
	harga bigint,
	jumlah int,
	foreign key (kdKategori) references ProdukKategori on update cascade on delete no action,
	constraint cek2 check (kdProduk like 'PRODU[0-9][0-9][0-9]')
)
create table Pemasok(
	kdPemasok nvarchar(10) primary key not null,
	tanggalPendaftaran nvarchar(10) not null,
	namaPerusahaan nvarchar (50) not null,
	pic nvarchar(50) not null,
	noTelepon nvarchar(50) not null,
	noFax nvarchar(50),
	email nvarchar(50),
	kota nvarchar(50) not null,
	alamat nvarchar(50) not null,
	constraint cek3 check ([kdPemasok] like 'PASOK[0-9][0-9][0-9]')
)
create table Pegawai(
	kdPegawai nvarchar(10) primary key not null,
	jabatan nvarchar(50) not null,
	tanggalPendaftaran nvarchar(10) not null,
	nama nvarchar(50) not null,
	tanggalLahir nvarchar(10) not null,
	noTelepon nvarchar(50) not null,
	noFax nvarchar(50),
	jenisKelamin nvarchar(50) not null,
	email nvarchar(50),
	kota nvarchar(50) not null,
	alamat nvarchar(50) not null,
	foreign key (kdPegawai) references Pegawai on update no action on delete no action,
	constraint cek4 check ([kdPegawai] like 'GAWAI[0-9][0-9][0-9]')
)
create table Pelanggan(
	kdPelanggan nvarchar(10) primary key not null,
	tanggalPendaftaran nvarchar(10) not null,
	nama nvarchar(50) not null,
	tanggalLahir nvarchar(10) not null,
	noTelepon nvarchar(50) not null,
	noFax nvarchar(50),
	jenisKelamin nvarchar(50) not null,
	email nvarchar(50),
	kota nvarchar(50) not null,
	alamat nvarchar(50) not null,
	constraint [kdPelanggan] check ([kdPelanggan] like 'NGGAN[0-9][0-9][0-9]')
)

create table SuratPermintaanPemasokan(
	kdSuratPermintaanPemasokan nvarchar(10) primary key not null,
	tanggalPermintaanPemasokan nvarchar(10) not null,
	kdPemasok nvarchar(10) not null,
	kdPegawai nvarchar(10) not null,
	foreign key(kdPemasok) references Pemasok on update cascade on delete no action,
	constraint cek5 check(kdSuratPermintaanPemasokan like 'MINTA[0-9][0-9][0-9]')
)
create table DetailSuratPermintaanPemasokan(
	kdSuratPermintaanPemasokan nvarchar(10) not null,
	kdBahanBaku nvarchar(10) not null,
	jumlahDiminta int not null,
	subtotal bigint not null,
	foreign key(kdSuratPermintaanPemasokan) references SuratPermintaanPemasokan on update cascade on delete no action,
	foreign key(kdBahanBaku) references BahanBaku on update cascade on delete no action
)
create table SuratPenerimaanPemasokan(
	kdSuratPenerimaanPemasokan nvarchar(10) primary key not null,
	kdSuratPermintaanPemasokan nvarchar(10) not null,
	tanggalSuratPenerimaanPemasokan nvarchar(10) not null,
	kdPegawai nvarchar(10) not null,
	deskripsi nvarchar(100) not null,
	[status] nvarchar(50) not null
	foreign key (kdSuratPermintaanPemasokan) references SuratPermintaanPemasokan on update cascade on delete no action,
	foreign key (kdPegawai) references Pegawai on update cascade on delete no action,
	constraint cek6 check( kdSuratPenerimaanPemasokan like 'TRIMA[0-9][0-9][0-9]')
)
create table DetailSuratPenerimaanPemasokan(
	kdSuratPenerimaanPemasokan nvarchar(10) not null,
	kdBahanBaku nvarchar(10) not null,
	jumlahDiterima int not null,
	foreign key(kdSuratPenerimaanPemasokan) references SuratPenerimaanPemasokan on update cascade on delete no action,
	foreign key(kdBahanBaku) references BahanBaku on update cascade on delete no action,
)

create table SuratPemesananProduk(
	kdSuratPemesananProduk nvarchar(10) primary key not null,
	tanggalSuratPemesananProduk nvarchar(10) not null,
	kdPelanggan nvarchar(10) not null,
	kdPegawai nvarchar(10) not null,
	foreign key (kdPelanggan) references Pelanggan on update cascade on delete no action,
	foreign key (kdPegawai) references Pegawai on update cascade on delete no action,
	constraint cek7 check(kdSuratPemesananProduk like 'PESAN[0-9][0-9][0-9]')
)
create table DetailSuratPemesananProduk(
	kdSuratPemesananProduk nvarchar(10) not null,
	kdProduk nvarchar(10) not null,
	jumlahDipesan int not null,
	subtotal bigint not null,
	foreign key(kdSuratPemesananProduk) references SuratPemesananProduk on update cascade on delete no action,
	foreign key(kdProduk) references Produk on update cascade on delete no action
)
--create table SuratPengeluaranProduk(
--	kdSuratPengeluaranProduk nvarchar(10)primary key not null,
--	tanggalSuratPengeluaranProduk nvarchar(10) not null,
--	kdSuratPemesananProduk nvarchar(10) not null,
--	kdPegawai nvarchar(10) not null,
--	foreign key(kdSuratPemesananProduk) references SuratPemesananProduk on update cascade on delete no action,
--	foreign key (kdPegawai) references Pegawai on update cascade on delete no action,
--	constraint cek8 check(kdSuratPengeluaranProduk like 'KLUAR[0-9][0-9][0-9]')
--)
--create table DetailSuratPengeluaranProduk(
--	kdSuratPengeluaranProduk nvarchar(10) not null,
--	kdProduk nvarchar(10) not null,
--	jumlahDikeluarkan int not null,
--	foreign key(kdSuratPengeluaranProduk) references SuratPengeluaranProduk on update cascade on delete no action,
--	foreign key(kdProduk) references Produk on update cascade on delete no action
--)
create table SuratJalan(
	kdSuratJalan nvarchar(10)primary key not null,
	kdSuratPemesananProduk nvarchar(10) not null,
	kdPegawai nvarchar(10) not null,
	tanggalJalan nvarchar(10) not null,
	deskripsi nvarchar(100) not null,
	foreign key(kdSuratPemesananProduk) references SuratPemesananProduk on update cascade on delete no action,
	foreign key (kdPegawai) references Pegawai on update no action on delete no action,
	constraint cek8 check(kdSuratJalan like 'JALAN[0-9][0-9][0-9]')
)

create table DetailSuratJalan(
	kdSuratJalan nvarchar(10) not null,
	kdProduk nvarchar(10) not null,
	jumlahDikirim int not null,
	foreign key(kdSuratJalan) references SuratJalan on update cascade on delete no action,
	foreign key(kdProduk) references Produk on update cascade on delete no action
)

create table Tagihan(
	kdTagihan nvarchar(10)primary key not null,
	kdSuratPemesananProduk nvarchar(10) not null,
	kdPegawai nvarchar(10) not null,
	grandTotal bigint not null,
	deskripsi nvarchar(100) not null,
	foreign key(kdSuratPemesananProduk) references SuratPemesananProduk on update cascade on delete no action,
	foreign key (kdPegawai) references Pegawai on update no action on delete no action,
	constraint cek9 check(kdTagihan like 'TAGIH[0-9][0-9][0-9]')
)

create table PembayaranPenjualanProduk(
	kdPembayaran nvarchar(10) primary key not null,
	kdTagihan nvarchar(10) not null,
	kdPegawai nvarchar(10) not null,
	tanggalPembayaran nvarchar(10) not null,
	jumlahBayar bigint not null,
	foreign key (kdPegawai) references Pegawai on update no action on delete no action,
	foreign key (kdTagihan) references Tagihan on update cascade on delete no action,
)

INSERT INTO BahanBaku  VALUES ('BHNKU001','Aenean eget','59792','6');
INSERT INTO BahanBaku  VALUES ('BHNKU002','sed consequat','185307','6');
INSERT INTO BahanBaku  VALUES ('BHNKU003','eu lacus.','67530','2');
INSERT INTO BahanBaku  VALUES ('BHNKU004','ornare placerat,','142026','8');
INSERT INTO BahanBaku  VALUES ('BHNKU005','dolor, tempus','70632','1');
INSERT INTO BahanBaku  VALUES ('BHNKU006','Sed id','176554','5');
INSERT INTO BahanBaku  VALUES ('BHNKU007','mattis ornare,','134696','9');
INSERT INTO BahanBaku  VALUES ('BHNKU008','lacus. Nulla','61584','1');
INSERT INTO BahanBaku  VALUES ('BHNKU009','mauris sagittis','128492','8');
INSERT INTO BahanBaku  VALUES ('BHNKU010','luctus. Curabitur','83120','2');

insert into ProdukKategori values ('DUKAT001', 'BRAVO')
insert into ProdukKategori values ('DUKAT002', 'CITY')
insert into ProdukKategori values ('DUKAT003', 'HOLIDAY')
insert into ProdukKategori values ('DUKAT004', 'ITEM')
insert into ProdukKategori values ('DUKAT005', 'LACASA')
insert into ProdukKategori values ('DUKAT006', 'SMART')


INSERT INTO Produk VALUES ('PRODU001','mi. Aliquam','DUKAT001','19327','29');
INSERT INTO Produk VALUES ('PRODU002','vel arcu.','DUKAT002','18522','28');
INSERT INTO Produk VALUES ('PRODU003','pede nec','DUKAT003','18504','10');
INSERT INTO Produk VALUES ('PRODU004','euismod urna.','DUKAT004','19552','26');
INSERT INTO Produk VALUES ('PRODU005','gravida sit','DUKAT005','13288','20');
INSERT INTO Produk VALUES ('PRODU006','neque pellentesque','DUKAT006','11566','14');
INSERT INTO Produk VALUES ('PRODU007','Curabitur massa.','DUKAT001','18679','27');
INSERT INTO Produk VALUES ('PRODU008','Cras sed','DUKAT002','15511','15');
INSERT INTO Produk VALUES ('PRODU009','ligula elit,','DUKAT003','19394','18');
INSERT INTO Produk VALUES ('PRODU010','odio a','DUKAT004','11090','18');

INSERT INTO Pemasok VALUES ('PASOK001','21/01/2011','Yahoo','Ali Cotton','984 0930','643 9682','sit.amet@ipsum.ca','Schenectady','365-7155 At, Rd.');
INSERT INTO Pemasok VALUES ('PASOK002','01/07/2011','Cakewalk','Wade Diaz','960 1292','170 6249','accumsan.neque.et@estac.ca','Cape Coral','Ap #489-7007 Lobortis. Av.');
INSERT INTO Pemasok VALUES ('PASOK003','13/11/2010','Altavista','Kuame Herring','488 3778','369 4459','enim@DonectinciduntDonec.org','Saginaw','534-7331 Ligula Rd.');
INSERT INTO Pemasok VALUES ('PASOK004','13/01/2011','Adobe','Nasim Hays','219 1250','917 5700','sodales.Mauris@sedturpis.ca','Erie','P.O. Box 468, 7842 Magna St.');
INSERT INTO Pemasok VALUES ('PASOK005','14/12/2010','Altavista','Cullen Graves','194 1464','967 0874','Proin.nisl@atpedeCras.com','Branson','987-4060 Integer Av.');
INSERT INTO Pemasok VALUES ('PASOK006','23/07/2011','Adobe','Aladdin Terrell','786 1480','331 7156','Etiam.gravida@maurisblanditmattis.ca','Calabasas','700-2147 Ultricies Rd.');
INSERT INTO Pemasok VALUES ('PASOK007','13/10/2011','Microsoft','Brent Small','434 1999','590 3440','justo.sit.amet@libero.com','Cody','P.O. Box 277, 5346 Eu Avenue');
INSERT INTO Pemasok VALUES ('PASOK008','11/03/2011','Lavasoft','Gregory Snow','293 1692','733 9251','consequat.dolor.vitae@auctor.com','Eugene','Ap #608-792 Senectus St.');
INSERT INTO Pemasok VALUES ('PASOK009','09/05/2011','Lavasoft','Ezekiel Pena','652 7339','481 4999','vel@commodo.ca','Kahului','7439 Cum Ave');
INSERT INTO Pemasok VALUES ('PASOK010','04/08/2011','Chami','Gavin Gray','883 7746','834 2020','ipsum.leo.elementum@magnisdisparturient.com','Burlington','P.O. Box 365, 4575 Bibendum Street');

INSERT INTO Pegawai VALUES ('GAWAI001',' Pengiriman ','28/03/2011','Ramona Avery','18/02/2011','7097683','7011735',' Perempuan','nulla.magna@justoPraesent.org','Evanston','P.O. Box 500, 1642 Metus. Rd.');
INSERT INTO Pegawai VALUES ('GAWAI002',' Pengiriman ','03/12/2010','Sade Holcomb','11/03/2011','1191587','1309813',' Perempuan','mollis@sedhendrerita.com','Morgantown','3262 Pede. Av.');
INSERT INTO Pegawai VALUES ('GAWAI003',' Marketing ','10/09/2011','Aurora Estes','02/05/2011','2945054','9585364','Laki-Laki ','dictum.sapien.Aenean@Etiam.org','Davenport','P.O. Box 861, 3213 Urna. St.');
INSERT INTO Pegawai VALUES ('GAWAI004','Pembelian ','11/01/2011','Lael Jarvis','14/08/2012','2298901','9632841',' Perempuan','ipsum.primis@estMauriseu.org','Huntington Beach','Ap #402-6934 Urna. Road');
INSERT INTO Pegawai VALUES ('GAWAI005',' Marketing ','02/12/2010','Britanney Dean','07/09/2011','4635052','4167531',' Perempuan','Mauris.nulla.Integer@egetnisidictum.edu','Minneapolis','P.O. Box 674, 6003 Quis, Av.');
INSERT INTO Pegawai VALUES ('GAWAI006',' Operasional ','18/04/2011','Wynter Watson','18/01/2011','5207945','5501338','Laki-Laki ','Vivamus@dui.edu','La Habra','983-1287 Eu Road');
INSERT INTO Pegawai VALUES ('GAWAI007',' Pengiriman ','04/10/2011','Shelby Moreno','12/02/2012','5144484','9991331',' Perempuan','sit@milacinia.com','Bowie','770-1776 Tristique Rd.');
INSERT INTO Pegawai VALUES ('GAWAI008',' Kasir','30/04/2011','Leslie Potts','17/07/2011','5989014','8556193',' Perempuan','orci@dolorvitae.com','Huntington Park','Ap #936-8970 Adipiscing. Ave');
INSERT INTO Pegawai VALUES ('GAWAI009','Pembelian ','10/03/2011','Kaye Fernandez','30/12/2010','1463529','7739499',' Perempuan','leo@iaculisodio.org','Texas City','708-696 Nulla Av.');
INSERT INTO Pegawai VALUES ('GAWAI010',' Kasir','30/05/2011','Lani Harvey','10/02/2011','7520774','8336163',' Perempuan','consectetuer.euismod.est@montes.com','Avalon','606-1132 Sed Rd.');--create table DetailSuratJalan(



INSERT INTO Pelanggan VALUES ('NGGAN001','18/01/2011','Heidi Good','20/09/2006','4104693','4481841','Laki-Laki ','Maecenas@ultricesaauctor.edu','Moscow','P.O. Box 912, 8639 Volutpat. Ave');
INSERT INTO Pelanggan VALUES ('NGGAN002','08/09/2011','Linda Mccoy','08/06/1993','2900095','2775606',' Perempuan','ornare.egestas.ligula@Suspendisse.edu','Plattsburgh','700-5135 Vivamus St.');
INSERT INTO Pelanggan VALUES ('NGGAN003','30/01/2011','Xandra Scott','18/05/2008','2159610','3306689',' Perempuan','egestas.Aliquam.nec@Etiambibendumfermentum.org','Darlington','P.O. Box 833, 4213 Non Avenue');
INSERT INTO Pelanggan VALUES ('NGGAN004','04/08/2011','Tamara Day','21/10/2003','2151820','8509475','Laki-Laki ','orci.adipiscing@nisimagnased.ca','Vergennes','7492 Ipsum. Av.');
INSERT INTO Pelanggan VALUES ('NGGAN005','28/10/2011','Jayme Castro','28/10/2002','8494588','3459833','Laki-Laki ','Donec@Donecegestas.com','Needham','Ap #575-2136 A St.');
INSERT INTO Pelanggan VALUES ('NGGAN006','28/10/2011','Kaitlin Shaffer','24/07/2007','1726610','1835459',' Perempuan','habitant@Duisdignissimtempor.com','Salinas','P.O. Box 390, 8394 Lorem, St.');
INSERT INTO Pelanggan VALUES ('NGGAN007','24/11/2010','Yvonne Johnson','17/01/1997','7030676','5865628',' Perempuan','Proin.mi@miac.ca','Rolla','348-9513 Nulla Av.');
INSERT INTO Pelanggan VALUES ('NGGAN008','10/01/2011','Gay Delaney','09/04/2000','2808470','3162750','Laki-Laki ','ultrices.Duis.volutpat@accumsansedfacilisis.ca','New Britain','805-9709 Nascetur Rd.');
INSERT INTO Pelanggan VALUES ('NGGAN009','12/05/2011','Lareina Estrada','02/04/2005','5841017','1759667',' Perempuan','egestas@acturpis.edu','Midland','7576 Sit Ave');
INSERT INTO Pelanggan VALUES ('NGGAN010','01/03/2011','Kai Head','07/09/1992','2249745','4186437','Laki-Laki ','dolor.quam.elementum@utipsum.ca','San Angelo','Ap #452-6741 Metus. St.');

INSERT INTO SuratPermintaanPemasokan  VALUES ('MINTA001','11/08/2011','PASOK001','GAWAI008');
INSERT INTO SuratPermintaanPemasokan  VALUES ('MINTA002','02/11/2011','PASOK003','GAWAI010');
INSERT INTO SuratPermintaanPemasokan  VALUES ('MINTA003','19/07/2011','PASOK009','GAWAI003');
INSERT INTO SuratPermintaanPemasokan  VALUES ('MINTA004','16/08/2011','PASOK003','GAWAI007');
INSERT INTO SuratPermintaanPemasokan  VALUES ('MINTA005','20/12/2010','PASOK004','GAWAI007');
INSERT INTO SuratPermintaanPemasokan  VALUES ('MINTA006','02/05/2011','PASOK004','GAWAI001');
INSERT INTO SuratPermintaanPemasokan  VALUES ('MINTA007','09/06/2011','PASOK003','GAWAI009');
INSERT INTO SuratPermintaanPemasokan  VALUES ('MINTA008','21/03/2011','PASOK007','GAWAI005');
INSERT INTO SuratPermintaanPemasokan  VALUES ('MINTA009','27/10/2011','PASOK002','GAWAI009');
INSERT INTO SuratPermintaanPemasokan  VALUES ('MINTA010','11/05/2011','PASOK008','GAWAI007');

INSERT INTO DetailSuratPermintaanPemasokan VALUES ('MINTA001','BHNKU002','7','13229');
INSERT INTO DetailSuratPermintaanPemasokan VALUES ('MINTA002','BHNKU009','5','18672');
INSERT INTO DetailSuratPermintaanPemasokan VALUES ('MINTA003','BHNKU002','8','18522');
INSERT INTO DetailSuratPermintaanPemasokan VALUES ('MINTA004','BHNKU003','9','17568');
INSERT INTO DetailSuratPermintaanPemasokan VALUES ('MINTA005','BHNKU008','13','17765');
INSERT INTO DetailSuratPermintaanPemasokan VALUES ('MINTA006','BHNKU003','11','13446');
INSERT INTO DetailSuratPermintaanPemasokan VALUES ('MINTA007','BHNKU010','13','18856');
INSERT INTO DetailSuratPermintaanPemasokan VALUES ('MINTA008','BHNKU007','11','12754');
INSERT INTO DetailSuratPermintaanPemasokan VALUES ('MINTA009','BHNKU010','10','16444');
INSERT INTO DetailSuratPermintaanPemasokan VALUES ('MINTA010','BHNKU007','8','15040');

INSERT INTO SuratPenerimaanPemasokan VALUES ('TRIMA001','MINTA009','29/05/2011','GAWAI002','Lorem ipsum dolor sit amet,','Tidak Penuh');
INSERT INTO SuratPenerimaanPemasokan VALUES ('TRIMA002','MINTA008','22/06/2011','GAWAI006','Lorem ipsum dolor','Tidak Penuh');
INSERT INTO SuratPenerimaanPemasokan VALUES ('TRIMA003','MINTA010','26/06/2011','GAWAI010','Lorem ipsum dolor sit amet,','Tidak Penuh');
INSERT INTO SuratPenerimaanPemasokan VALUES ('TRIMA004','MINTA002','31/07/2011','GAWAI006','Lorem ipsum dolor','Penuh');
INSERT INTO SuratPenerimaanPemasokan VALUES ('TRIMA005','MINTA001','02/10/2011','GAWAI001','Lorem ipsum','Penuh');
INSERT INTO SuratPenerimaanPemasokan VALUES ('TRIMA006','MINTA010','27/04/2011','GAWAI005','Lorem ipsum dolor sit amet,','Penuh');
INSERT INTO SuratPenerimaanPemasokan VALUES ('TRIMA007','MINTA002','19/11/2010','GAWAI009','Lorem ipsum','Kosong');
INSERT INTO SuratPenerimaanPemasokan VALUES ('TRIMA008','MINTA008','23/09/2011','GAWAI004','Lorem','Tidak Penuh');
INSERT INTO SuratPenerimaanPemasokan VALUES ('TRIMA009','MINTA003','24/11/2010','GAWAI004','Lorem ipsum dolor sit','Penuh');
INSERT INTO SuratPenerimaanPemasokan VALUES ('TRIMA010','MINTA009','29/04/2011','GAWAI006','Lorem ipsum','Kosong');

INSERT INTO DetailSuratPenerimaanPemasokan VALUES ('TRIMA005','BHNKU008','10');
INSERT INTO DetailSuratPenerimaanPemasokan VALUES ('TRIMA001','BHNKU005','10');
INSERT INTO DetailSuratPenerimaanPemasokan VALUES ('TRIMA008','BHNKU001','7');
INSERT INTO DetailSuratPenerimaanPemasokan VALUES ('TRIMA010','BHNKU006','14');
INSERT INTO DetailSuratPenerimaanPemasokan VALUES ('TRIMA002','BHNKU007','11');
INSERT INTO DetailSuratPenerimaanPemasokan VALUES ('TRIMA006','BHNKU008','6');
INSERT INTO DetailSuratPenerimaanPemasokan VALUES ('TRIMA007','BHNKU007','10');
INSERT INTO DetailSuratPenerimaanPemasokan VALUES ('TRIMA007','BHNKU010','6');
INSERT INTO DetailSuratPenerimaanPemasokan VALUES ('TRIMA003','BHNKU009','12');
INSERT INTO DetailSuratPenerimaanPemasokan VALUES ('TRIMA007','BHNKU008','10');

INSERT INTO SuratPemesananProduk VALUES ('PESAN001','28/04/2011','NGGAN003','GAWAI006');
INSERT INTO SuratPemesananProduk VALUES ('PESAN002','18/01/2012','NGGAN007','GAWAI003');
INSERT INTO SuratPemesananProduk VALUES ('PESAN003','26/07/2011','NGGAN007','GAWAI001');
INSERT INTO SuratPemesananProduk VALUES ('PESAN004','19/03/2012','NGGAN010','GAWAI006');
INSERT INTO SuratPemesananProduk VALUES ('PESAN005','12/11/2011','NGGAN008','GAWAI004');
INSERT INTO SuratPemesananProduk VALUES ('PESAN006','30/12/2010','NGGAN002','GAWAI010');
INSERT INTO SuratPemesananProduk VALUES ('PESAN007','12/12/2010','NGGAN005','GAWAI006');
INSERT INTO SuratPemesananProduk VALUES ('PESAN008','07/04/2012','NGGAN001','GAWAI007');
INSERT INTO SuratPemesananProduk VALUES ('PESAN009','12/06/2012','NGGAN008','GAWAI003');
INSERT INTO SuratPemesananProduk VALUES ('PESAN010','11/04/2011','NGGAN005','GAWAI007');


INSERT INTO DetailSuratPemesananProduk VALUES ('PESAN001','PRODU004','4','19649');
INSERT INTO DetailSuratPemesananProduk VALUES ('PESAN002','PRODU001','7','13916');
INSERT INTO DetailSuratPemesananProduk VALUES ('PESAN003','PRODU006','10','17928');
INSERT INTO DetailSuratPemesananProduk VALUES ('PESAN004','PRODU005','4','15238');
INSERT INTO DetailSuratPemesananProduk VALUES ('PESAN005','PRODU001','5','16480');
INSERT INTO DetailSuratPemesananProduk VALUES ('PESAN006','PRODU004','5','18552');
INSERT INTO DetailSuratPemesananProduk VALUES ('PESAN007','PRODU001','8','18943');
INSERT INTO DetailSuratPemesananProduk VALUES ('PESAN008','PRODU004','5','19462');
INSERT INTO DetailSuratPemesananProduk VALUES ('PESAN009','PRODU009','2','10850');
INSERT INTO DetailSuratPemesananProduk VALUES ('PESAN010','PRODU008','9','15181');

INSERT INTO SuratJalan VALUES ('JALAN001','PESAN003','GAWAI003','27/02/2012','Pesanan siap diantar');
INSERT INTO SuratJalan VALUES ('JALAN002','PESAN004','GAWAI009','09/04/2012','Belum siap antar');
INSERT INTO SuratJalan VALUES ('JALAN003','PESAN003','GAWAI010','09/03/2011','Pesanan siap diantar');
INSERT INTO SuratJalan VALUES ('JALAN004','PESAN005','GAWAI004','05/05/2012','Belum siap antar');
INSERT INTO SuratJalan VALUES ('JALAN005','PESAN008','GAWAI009','30/11/2011','Belum siap antar');
INSERT INTO SuratJalan VALUES ('JALAN006','PESAN005','GAWAI008','12/01/2012','Pesanan siap diantar');
INSERT INTO SuratJalan VALUES ('JALAN007','PESAN010','GAWAI009','14/05/2011','Belum siap antar');
INSERT INTO SuratJalan VALUES ('JALAN008','PESAN009','GAWAI008','22/03/2012','Pesanan siap diantar');
INSERT INTO SuratJalan VALUES ('JALAN009','PESAN006','GAWAI002','17/12/2010','Pesanan siap diantar');
INSERT INTO SuratJalan VALUES ('JALAN010','PESAN001','GAWAI003','01/09/2011','Belum siap antar');

INSERT INTO DetailSuratJalan VALUES ('JALAN001','PRODU004','4');
INSERT INTO DetailSuratJalan VALUES ('JALAN002','PRODU001','7');
INSERT INTO DetailSuratJalan VALUES ('JALAN003','PRODU006','10');
INSERT INTO DetailSuratJalan VALUES ('JALAN004','PRODU005','4');
INSERT INTO DetailSuratJalan VALUES ('JALAN005','PRODU001','5');
INSERT INTO DetailSuratJalan VALUES ('JALAN006','PRODU004','5');
INSERT INTO DetailSuratJalan VALUES ('JALAN007','PRODU001','8');
INSERT INTO DetailSuratJalan VALUES ('JALAN008','PRODU004','5');
INSERT INTO DetailSuratJalan VALUES ('JALAN009','PRODU009','2');
INSERT INTO DetailSuratJalan VALUES ('JALAN010','PRODU008','9');

INSERT INTO Tagihan  VALUES ('TAGIH001','PESAN004','GAWAI003','16455','Lunas');
INSERT INTO Tagihan  VALUES ('TAGIH002','PESAN003','GAWAI001','19817','Masih dicicil');
INSERT INTO Tagihan  VALUES ('TAGIH003','PESAN006','GAWAI009','11611','Lunas');
INSERT INTO Tagihan  VALUES ('TAGIH004','PESAN001','GAWAI009','12402','Masih dicicil');
INSERT INTO Tagihan  VALUES ('TAGIH005','PESAN005','GAWAI006','11678','Lunas');
INSERT INTO Tagihan  VALUES ('TAGIH006','PESAN002','GAWAI003','12634','Lunas');
INSERT INTO Tagihan  VALUES ('TAGIH007','PESAN007','GAWAI003','16353','Masih dicicil');
INSERT INTO Tagihan  VALUES ('TAGIH008','PESAN003','GAWAI004','16194','Lunas');
INSERT INTO Tagihan  VALUES ('TAGIH009','PESAN006','GAWAI010','11484','Masih dicicil');
INSERT INTO Tagihan  VALUES ('TAGIH010','PESAN007','GAWAI002','10445','Masih dicicil');


INSERT INTO PembayaranPenjualanProduk VALUES ('BAYAR001','TAGIH005','GAWAI003','10/03/2011','17404');
INSERT INTO PembayaranPenjualanProduk VALUES ('BAYAR002','TAGIH004','GAWAI007','08/06/2011','11634');
INSERT INTO PembayaranPenjualanProduk VALUES ('BAYAR003','TAGIH002','GAWAI006','25/02/2011','17694');
INSERT INTO PembayaranPenjualanProduk VALUES ('BAYAR004','TAGIH004','GAWAI004','22/04/2011','13192');
INSERT INTO PembayaranPenjualanProduk VALUES ('BAYAR005','TAGIH008','GAWAI010','10/09/2011','13560');
INSERT INTO PembayaranPenjualanProduk VALUES ('BAYAR006','TAGIH002','GAWAI008','04/11/2011','10771');
INSERT INTO PembayaranPenjualanProduk VALUES ('BAYAR007','TAGIH007','GAWAI007','22/02/2011','17145');
INSERT INTO PembayaranPenjualanProduk VALUES ('BAYAR008','TAGIH003','GAWAI008','09/12/2010','13761');
INSERT INTO PembayaranPenjualanProduk VALUES ('BAYAR009','TAGIH007','GAWAI007','30/01/2011','12201');
INSERT INTO PembayaranPenjualanProduk VALUES ('BAYAR010','TAGIH004','GAWAI008','11/11/2010','13913');

--	kdSuratJalan nvarchar(10) primary key not null,
--	kdProduk nvarchar(10) not null,
--	jumlahDiterima int not null,
--	foreign key(kdSuratJalan) references SuratJalan on update cascade on delete no action,
--	foreign key(kdProduk) references Produk on update cascade on delete no action,
--)

--create table suratPembayaran(
--	kdPembayaran nvarchar(10) primary key not null,
--	kdPesananPemasokan nvarchar(10),
--	kdPesananPenjualan nvarchar(10),
--	foreign key(kdPesananPemasokan) references PesananPemasokan on update cascade on delete no action,
--	foreign key(kdPesananPenjualan) references PesananPenjualan on update cascade on delete no action,
--)
	
--insert into ProdukKategori values ('DUKAT001', 'BRAVO')
--insert into ProdukKategori values ('DUKAT002', 'CITY')
--insert into ProdukKategori values ('DUKAT003', 'HOLIDAY')
--insert into ProdukKategori values ('DUKAT004', 'ITEM')
--insert into ProdukKategori values ('DUKAT005', 'LACASA')
--insert into ProdukKategori values ('DUKAT006', 'SMART')




--INSERT INTO Produk VALUES ('PRODU001','Christian Pennington','DUKAT001',1000,5);
--INSERT INTO Produk VALUES ('PRODU002','Thomas Arnold','DUKAT002',2000,6);
--INSERT INTO Produk VALUES ('PRODU003','Hedley Hughes','DUKAT003',3000,7);
--INSERT INTO Produk VALUES ('PRODU004','Yasir Marquez','DUKAT004',4000,8);
--INSERT INTO Produk VALUES ('PRODU005','Randall Whitaker','DUKAT005',1000,9);
--INSERT INTO Produk VALUES ('PRODU006','Sylvester Meyers','DUKAT006',2000,10);
--INSERT INTO Produk VALUES ('PRODU007','Ulysses Richards','DUKAT001',3000,11);
--INSERT INTO Produk VALUES ('PRODU008','Axel Hester','DUKAT002',4000,12);
--INSERT INTO Produk VALUES ('PRODU009','Macaulay Mack','DUKAT003',1000,13);
--INSERT INTO Produk VALUES ('PRODU010','Camden Kirk','DUKAT004',2000,14);

--INSERT INTO Pelanggan  VALUES ('NGGAN001','Raymond Carr','Ap #360-594 Interdum. Rd.','381 2266-2551');
--INSERT INTO Pelanggan  VALUES ('NGGAN002','Lucian Hayden','3849 Parturient Street','103 7767-0491');
--INSERT INTO Pelanggan  VALUES ('NGGAN003','Forrest Bradford','1750 Vel Ave','765 3946-0497');
--INSERT INTO Pelanggan  VALUES ('NGGAN004','Rajah Wilkerson','P.O. Box 493, 212 Vivamus Av.','557 3599-1410');
--INSERT INTO Pelanggan  VALUES ('NGGAN005','Dolan Ferguson','Ap #210-1173 Imperdiet, Street','629 7692-2647');
--INSERT INTO Pelanggan  VALUES ('NGGAN006','Galvin Anthony','494 Risus St.','418 7798-4271');
--INSERT INTO Pelanggan  VALUES ('NGGAN007','Alec Cooke','6713 Luctus St.','216 6256-6644');
--INSERT INTO Pelanggan  VALUES ('NGGAN008','Joshua Roach','P.O. Box 246, 5193 At, Rd.','332 4194-2505');
--INSERT INTO Pelanggan  VALUES ('NGGAN009','Kane Whitaker','P.O. Box 874, 7393 Commodo St.','823 9392-7158');
--INSERT INTO Pelanggan  VALUES ('NGGAN010','Rahim Wiggins','Ap #615-4851 Sagittis Avenue','646 6344-5849');

--INSERT INTO Pemasok  VALUES ('PASOK001','Nathan Nixon','1127 Pellentesque St.','1 55 216 5953-8351','D8B 5G6');
--INSERT INTO Pemasok  VALUES ('PASOK002','Sean Koch','P.O. Box 567, 7346 Et Street','1 43 889 1965-9188','40858');
--INSERT INTO Pemasok  VALUES ('PASOK003','Blake Bray','729 Nec Road','1 22 379 6084-9845','J8F 2I6');
--INSERT INTO Pemasok  VALUES ('PASOK004','Brendan Coleman','245-5486 Tristique Avenue','1 61 683 8535-5195','I2J 7Y5');
--INSERT INTO Pemasok  VALUES ('PASOK005','Dale Daniel','250-3732 Nunc Road','1 73 934 5184-6192','V8J 2Y7');
--INSERT INTO Pemasok  VALUES ('PASOK006','Sylvester Garza','Ap #916-5074 Quisque St.','1 90 864 8082-1678','61149');
--INSERT INTO Pemasok  VALUES ('PASOK007','Lucas Stanley','Ap #643-3470 In Rd.','1 81 331 3277-0753','20806');
--INSERT INTO Pemasok  VALUES ('PASOK008','Carlos Buchanan','6996 Elit, Ave','1 92 456 3786-8196','94423');
--INSERT INTO Pemasok  VALUES ('PASOK009','Elmo Chandler','P.O. Box 904, 956 Tincidunt Avenue','1 95 855 2017-3860','89379');
--INSERT INTO Pemasok  VALUES ('PASOK010','Dalton Duke','Ap #374-6741 Cursus Avenue','1 53 662 2487-6993','U1I 5V3');


--INSERT INTO Pegawai  VALUES ('GAWAI001','Ronan Vega','1184 Egestas Avenue','1 30 542 4432-3549','88880','Northern Mariana Islands');
--INSERT INTO Pegawai  VALUES ('GAWAI002','Bevis Gentry','P.O. Box 654, 1572 Neque Rd.','1 24 345 8872-2155','05584','Italy');
--INSERT INTO Pegawai  VALUES ('GAWAI003','Ivor Boyle','296-2098 Euismod Rd.','1 68 463 6184-9561','17313','Turkey');
--INSERT INTO Pegawai  VALUES ('GAWAI004','Ralph Gilliam','249-8881 Quis, Street','1 83 142 4150-2341','03521','Turkey');
--INSERT INTO Pegawai  VALUES ('GAWAI005','Josiah Sargent','P.O. Box 977, 6305 Tristique Road','1 61 545 6870-0202','54468','Antarctica');
--INSERT INTO Pegawai  VALUES ('GAWAI006','Abel Warner','5703 Ultrices Rd.','1 99 555 7092-6896','27183','Micronesia');
--INSERT INTO Pegawai  VALUES ('GAWAI007','Walter Hebert','369-543 Euismod Av.','1 90 689 2348-4400','07124','French Guiana');
--INSERT INTO Pegawai  VALUES ('GAWAI008','Rashad Stokes','767-5756 Nam Ave','1 70 661 3503-8522','23320','Malawi');
--INSERT INTO Pegawai  VALUES ('GAWAI009','John Serrano','Ap #943-1014 Turpis Road','1 96 667 3385-8987','00533','Tonga');
--INSERT INTO Pegawai  VALUES ('GAWAI010','Griffin Hill','689-2894 Sed Avenue','1 95 492 1693-9888','75286','San Marino');

--INSERT INTO SuratPermintaanPemasokan values('MINTA001', GETDATE(), 'PASOK001', 'GAWAI001')
--go

--INSERT INTO DetailSuratPermintaanPemasokan values('MINTA001', 'PRODU001', 1)
--go

--INSERT INTO SuratPenerimaanPemasokan values('TRIMA001', 'MINTA001', GETDATE(), 'GAWAI002')
--go

--INSERT INTO DetailSuratPenerimaanPemasokan values('TRIMA001','PRODU001', 1 )
--go

--INSERT INTO SuratPemesananProduk values('PESAN001', GETDATE(), 'NGGAN001', 'GAWAI001')
--go

--INSERT INTO DetailSuratPemesananProduk values('PESAN001', 'PRODU001', 1)
--go

--INSERT INTO SuratPengeluaranProduk values('KLUAR001', GETDATE(), 'PESAN001','GAWAI002')
--go

--INSERT INTO DetailSuratPengeluaranProduk values('KLUAR001','PRODU001', 1 )
--go



/*
select * from BahanBaku
select * from ProdukKategori
select * from Produk
select * from Pemasok
select * from Pelanggan
select * from Pegawai
select * from SuratPermintaanPemasokan
select * from DetailSuratPermintaanPemasokan
select * from SuratPenerimaanPemasokan
select * from DetailSuratPenerimaanPemasokan
select * from SuratPemesananProduk
select * from DetailSuratPemesananProduk
--select * from SuratPengeluaranProduk
--select * from DetailSuratPengeluaranProduk
select * from SuratJalan
select * from DetailSuratJalan
select * from Tagihan
select * from PembayaranPenjualanProduk
*/
/*
drop table DetailSuratPenerimaanPemasokan
drop table SuratPenerimaanPemasokan
drop table DetailSuratPermintaanPemasokan
drop table SuratPermintaanPemasokan
drop table DetailSuratPengeluaranProduk
drop table SuratPengeluaranProduk
drop table DetailSuratPemesananProduk
drop table SuratPemesananProduk
drop table Pegawai
drop table Pelanggan
drop table Pemasok
drop table Produk
drop table ProdukKategori
*/