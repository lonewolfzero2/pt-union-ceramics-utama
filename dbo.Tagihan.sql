﻿CREATE TABLE [dbo].[Tagihan] (
    [kdTagihan]              NVARCHAR (10)  NOT NULL,
    [kdSuratJalan]			 NVARCHAR (10)  NOT NULL,
    [kdPegawai]              NVARCHAR (10)  NOT NULL,
    [grandTotal]             BIGINT         NOT NULL,
    [deskripsi]              NVARCHAR (100) NOT NULL,
    PRIMARY KEY CLUSTERED ([kdTagihan] ASC),
    FOREIGN KEY ([kdSuratJalan]) REFERENCES [dbo].[SuratJalan] ([kdSuratJalan]) ON UPDATE CASCADE,
    FOREIGN KEY ([kdPegawai]) REFERENCES [dbo].[Pegawai] ([kdPegawai]),
    CONSTRAINT [cek9] CHECK ([kdTagihan] like 'TAGIH[0-9][0-9][0-9]')
);

